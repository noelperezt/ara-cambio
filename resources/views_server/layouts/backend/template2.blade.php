<!DOCTYPE html>
<html>
  <head>
    @include('layouts.backend.partial.head')
  </head>
  <body class="hold-transition skin-blue sidebar-mini" id="app">
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a class="logo" href="index2.html">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>RA</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>ARACâmb</b>io</span>
        </a>
        @include('layouts.backend.partial.navbar')
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        @include('layouts.backend.partial.aside')
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div id="app" class="content-wrapper" style="min-height: 100px !important">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>@yield('title')<small>@yield('description')</small></h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-circle-o "></i>@yield('title')</a></li>
            <li class="active">@yield('description')</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="container-fluid">
          @yield('content')
        </section>
          @yield('elements')
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      </div>
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b>
        </div>
        <strong>
          Desenvolvido por
          <a href="https://www.result.inf.br">RESULT SOLUTIONS</a>
        </strong>, (92) 3675-3244 | 
        <a href="https://www.result.inf.br">www.result.inf.br</a>
      </footer>
      @include('layouts.backend.partial.script')
    
  </body>
</html>

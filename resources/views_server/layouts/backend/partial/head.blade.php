<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<title>
  Ara Câmbio - @yield('title')
</title>
    <!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta id="token" name="token" value="{{ csrf_token() }}">
<!-- Bootstrap 3.3.7 -->
<link href="{!! asset('public/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
<!-- Font Awesome -->
<link href="{!! asset('public/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet">
<!-- Ionicons -->
<link href="{!! asset('public/Ionicons/css/ionicons.min.css') !!}" rel="stylesheet">
<!-- Theme style -->
<link href="{!! asset('public/dist/css/AdminLTE.min.css') !!}" rel="stylesheet">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<link href="{!! asset('public/dist/css/skins/_all-skins.min.css')!!}" rel="stylesheet">
<link href="{!! asset('public/select2/css/select2.min.css')!!}" rel="stylesheet">
<link href="{!! asset('public/select2/css/select2-bootstrap.min.css')!!}" rel="stylesheet">
<!-- bootstrap wysihtml5 - text editor -->
<link href="{!! asset('public/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')!!}" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet">
<link rel="shortcut icon" type="image/png" href="{!! asset('public/dist/img/logoara.png')!!}"/>
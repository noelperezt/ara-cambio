<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="text-center image">
      <img alt="User Image" class="img-circle" src="public/dist/img/logoara.png">
      </img>
    </div>
  </div>
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">
      Navegação Principal
    </li>
    <li class="active">
      <a href="{{ url('/') }}">
        <i class="fa fa-home ">
        </i>
        <span>
          Início
        </span>
      </a>
    </li>
  </ul>
  @if(Auth::user()->rol == "Administrador" || Auth::user()->rol == "Operador")
  <ul class="sidebar-menu" data-widget="tree">
    <li class="treeview">
      <a href="#">
        <i class="fa fa-cog ">
        </i>
        <span>
          Configuração Básica
        </span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right">
          </i>
        </span>
      </a>
      <ul class="treeview-menu">
        @if(Auth::user()->rol == "Administrador")
        <li>
          <a href="{{ url('banco') }}">
            <i class="fa fa-circle-o">
            </i>
            Bancos
          </a>
        </li>
        @endif
        <li>
          <a href="{{ url('beneficiario') }}">
            <i class="fa fa-circle-o">
            </i>
            Beneficiário
          </a>
        </li>
        <li>
          <a href="{{ url('cliente') }}">
            <i class="fa fa-circle-o">
            </i>
            Cliente
          </a>
        </li>
        <li>
          <a href="{{ url('cuentas') }}">
            <i class="fa fa-circle-o">
            </i>
            Contas Bancárias
          </a>
        </li>
        @if(Auth::user()->rol == "Administrador")
        <li>
          <a href="{{ url('moneda') }}">
            <i class="fa fa-circle-o">
            </i>
            Moedas
          </a>
        </li>
        <li>
          <a href="{{ url('paises') }}">
            <i class="fa fa-circle-o">
            </i>
            Paises
          </a>
        </li>
        <li>
          <a href="{{ url('penalidades') }}">
            <i class="fa fa-circle-o">
            </i>
            Penalidades
          </a>
        </li>
        <li>
          <a href="{{ url('tipo_pago') }}">
            <i class="fa fa-circle-o">
            </i>
            Tipos de Pagamento
          </a>
        </li>
        <li>
          <a href="{{ url('unidades') }}">
            <i class="fa fa-circle-o">
            </i>
            Unidades
          </a>
        </li>
        @endif
      </ul>
    </li>
  </ul>
  @endif
  @if(Auth::user()->rol == "Administrador")
  <ul class="sidebar-menu" data-widget="tree">
    <li>
      <a href="{{ url('usuario') }}">
        <i class="fa fa-user ">
        </i>
        <span>
          Usuários
        </span>
      </a>
    </li>
  </ul>
  @endif
  <ul class="sidebar-menu" data-widget="tree">
    <li>
      <a href="{{ url('caxa') }}">
        <i class="fa fa-briefcase ">
        </i>
        <span>
          Caixa
        </span>
      </a>
    </li>
  </ul>
  <ul class="sidebar-menu" data-widget="tree">
    <li>
      <a href="{{ url('solicitud') }}">
        <i class="fa fa-file">
        </i>
        <span>
          Pedido
        </span>
      </a>
    </li>
  </ul>
  <ul class="sidebar-menu" data-widget="tree">
    <li class="treeview">
      <a href="#">
        <i class="fa fa-folder-open">
        </i>
        <span>
          Informes
        </span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right">
          </i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li>
          <a href="{{ url('observacion') }}">
            <i class="fa fa-circle-o">
            </i>
            Histórico do Observações
          </a>
        </li>
        <li>
          <a href="{{ url('historico_tasa') }}">
            <i class="fa fa-circle-o">
            </i>
            Histórico do Taxas
          </a>
        </li>
      </ul>
    </li>
  </ul>
</section>

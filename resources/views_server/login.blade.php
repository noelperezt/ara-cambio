<!DOCTYPE doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>
      Ara Cambio - Iniciar Sessão
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link href="{!! asset('public/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{!! asset('public/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="{!! asset('public/Ionicons/css/ionicons.min.css') !!}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{!! asset('public/dist/css/AdminLTE.min.css') !!}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{!! asset('public/plugins/iCheck/square/blue.css') !!}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link rel="shortcut icon" type="image/png" href="{!! asset('public/dist/img/logoara.png')!!}"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet">
    <style>
      html,body {
        height:100%;
        width:100%;
        position:fixed;
      }
      #background-carousel{
        position:fixed;
        width:100%;
        height:100%;
        z-index:-1;
      }
      .carousel,
      .carousel-inner {
        width:100%;
        height:100%;
        z-index:0;
        overflow:hidden;
      }
      .item {
        width:100%;
        height:100%;
        background-position:center center;
        background-size:cover;
        z-index:0;
      }
       
      #content-wrapper {
        position:absolute;
        z-index:1 !important;
        min-width:100%;
        min-height:100%;
      }
      .well {
          opacity:0.85
      }
    </style>

  </head>
  <body class="hold-transition login-page">
    @include('alerts.requestlogin')
    <div id="background-carousel">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active" style="background-image:url({!! asset('public/dist/img/fondo.jpg')!!})">
          <div class="row">
            <div class="col-lg-12">
              
            </div>
          </div>
        </div>

        <div class="item" style="background-image:url({!! asset('public/dist/img/fondo2.jpg')!!})">
          <div class="row">
            <div class="col-lg-12">
              
            </div>
            <!-- ./col -->
          </div>
          <!-- /.row -->
        </div>
        <div class="item" style="background-image:url({!! asset('public/dist/img/fondo4.jpg')!!})">
          <div class="row">
            <div class="col-lg-12">
              
            </div>
            <!-- ./col -->
          </div>
          <!-- /.row -->
        </div>

      </div>
    </div> 
    </div>


    <div class="login-box">
      <div class="login-logo">
        <a href="{{ url('/') }}">
          <b>
            ARA
          </b>
          Câmbio
        </a>
      </div>
      <!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">
          <!--Faça login para iniciar sua sessão-->
        </p>
        {!!Form::open(['method'=>'POST','autocomplete'=>'off', 'route'=>'login.login'])!!}
          <div class="form-group has-feedback">
            {!!Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Username', 'required'=>'required'])!!}
            <span class="glyphicon glyphicon-user form-control-feedback">
            </span>
          </div>
          <div class="form-group has-feedback">
            {!!Form::password('password',['class'=>'form-control', 'placeholder'=>'Senha', 'required'=>'required'])!!}
            <span class="glyphicon glyphicon-lock form-control-feedback">
            </span>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <!--<div class="checkbox icheck">
                <label>
                  <input type="checkbox">
                    Lembre de mim
                  </input>
                </label>
              </div>-->
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
              <button class="btn btn-primary btn-block btn-flat" type="submit">
                Iniciar sessão
              </button>
            </div>
            <!-- /.col -->
          </div>
        {!!Form::close()!!}
        <!--<div class="social-auth-links text-center">
          <p style="height: 20px">
          </p>
          <a class="btn btn-block btn-social btn-facebook btn-flat" href="#">
            <i class="fa fa-question-circle">
            </i>
            Esqueci a minha senha
          </a>
          <a class="btn btn-block btn-social btn-google btn-flat" href="#">
            <i class="fa fa-user-circle">
            </i>
            Registre uma nova associação
          </a>
          <a class="text-center;btn btn-block btn-social btn-facebook btn-flat" href="register.html">
          </a>
        </div>-->
        <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
      <!-- jQuery 3 -->
      <script src="{!! asset('public/jquery/dist/jquery.min.js') !!}">
      </script>
      <!-- Bootstrap 3.3.7 -->
      <script src="{!! asset('public/bootstrap/js/bootstrap.min.js') !!}">
      </script>
    </div>
  </body>
</html>
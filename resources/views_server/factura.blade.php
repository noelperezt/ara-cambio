@extends('layouts.backend.facture')
@section('content')
<section><br/>
<div class="row" style="font-size: 10px;">
  <div class="col-md-4" align="center"></div>
  <div class="col-md-3" align="center"><br><br>
    <div class="row">
      <div class="col-md-12" align="center">
        ==============================================
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ARA CORRETORA DE CAMBIOS E INVESTIMENTOS<br>
        DIGITAIS LTDA
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        CNPJ: 20.419.043/0001-13
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        www.aracambio.com.br
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ==============================================
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        REMETENTE
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ------------------------------------------------------------------------
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        <table>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">N. DA TRANSACAO:</strong></td>
            <td width="110px">ARA.0000{{$solicitud['id']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">NOME:</strong></td>
            <td width="110px">{{$cliente['nombre']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">SOBRENOME:</strong></td>
            <td width="110px">{{$cliente['apellido']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">IDENTIDADE:</strong></td>
            <td width="110px">{{$cliente['cep']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">TELEFONE:</strong></td>
            <td width="110px">{{$cliente['celular']}}</td>
          </tr>
        </table>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12" align="center">
        ==============================================
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        BENEFICIARIO
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ------------------------------------------------------------------------
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
         <table>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">NOME:</strong></td>
            <td width="110px">{{$beneficiario['nombre']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">SOBRENOME:</strong></td>
            <td width="110px">{{$beneficiario['apellido']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">IDENTIDADE:</strong></td>
            <td width="110px">{{$beneficiario['cpf']}}</td>
          </tr>
        </table>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12" align="center">
        ==============================================
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        DADOS BANCARIOS
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ------------------------------------------------------------------------
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
         <table>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">BANCO:</strong></td>
            <td width="110px">{{$banco['nombre']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">N. DA CONTA:</strong></td>
            <td width="110px">{{$cuenta['cuenta']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">TIPO DE CONTA:</strong></td>
            <td width="110px">{{$cuenta['tipo_cuenta']}}</td>
          </tr>
        </table>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12" align="center">
        ==============================================
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        VALORES
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ------------------------------------------------------------------------
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
         <table>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">MOEDA:</strong></td>
            <td width="110px">{{$moneda['moneda']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">VALOR:</strong></td>
            <td width="110px">{{$solicitud['monto']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">TAXA:</strong></td>
            <td width="110px">{{$solicitud['taza']}}</td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">BOLIVAR:</strong></td>
            <td width="110px">{{$solicitud['taza']*$solicitud['monto']}}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ==============================================
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        TERMO DE ACEITE
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ------------------------------------------------------------------------
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        <table>
          <tr>
            <td width="220px" align="justify">Todas as infomacoes fornecidas por mim em contidas em qualquer documentacao emitida em conexao com a minha compra de tais servicos sao verdadeiras. Eu reconheco ainda que esta operacao esta sujeta a os termos e condicoes previstos na ARA CAMBIO, e estou de acordo com este termos.</td>
          </tr>
        </table>
        <br><br>
        <table>
          <tr>
            <td width="220px" align="center">______________________</td>
          </tr>
          <tr>
             <td width="220px" align="center">ASSINATURA</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ==============================================
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        <table>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">HORA E DATA:</strong></td>
            <td width="110px"><?php $date = date_create($solicitud['created_at']); echo date_format($date, 'd/m/Y H:i:s');?></td>
          </tr>
          <tr>
            <td width="110px"><strong style="padding-right: 20px;">OPERADOR:</strong></td>
            <td width="110px">{{$usuario['nombre']}} {{$usuario['apellido']}}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        ------------------------------------------------------------------------
      </div>
    </div>

  </div>
</div>
</section>
@endsection
@section('js')
{!!Html::script('public/js/facturas.js')!!}
@endsection
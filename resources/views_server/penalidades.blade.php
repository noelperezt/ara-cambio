@if(Auth::user()->rol != "Administrador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@extends('layouts.backend.template')
@section('title', 'Penalidades')
@section('description', 'Lista da Penalidades')
@section('content')
<section class="content" style="min-height: 100px">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
            Descrição Penalidades
          </h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" v-on:submit.prevent="createItem">
          <div class="box-body">
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.descricao}">
                <label for="exampleInputEmail1">
                  Descrição
                </label>
              <input class="form-control" id="exampleInputEmail1" v-model="newItem.descricao" placeholder="Escreva aqui" type="text">
              <span class="help-block text-danger" v-if="formErrors.errors.descricao">@{{formErrors.errors.descricao}}</span>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.montante}">
                <label for="exampleInputEmail1">
                  Montante o Penalidades
                </label>
              
             <div class="input-group" >
              <input class="form-control" id="exampleInputEmail1"  v-model="newItem.montante" placeholder="Escreva aqui" type="text">
              
              <span class="input-group-addon" id="basic-addon2">$R</span>
              </div> 
             <span class="help-block text-danger" v-if="formErrors.errors.montante">@{{formErrors.errors.montante}}</span>
            <div class="clearfix"></div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3">
                <a href="#" v-on:click.prevent="createItem" type="submit"><i class=" glyphicon glyphicon-plus" ; style="padding-top: 30px;font-size: 25px"></a></i>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
      <h2 class="box-title">
        Lista da Penalidades
      </h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="width: 25%;font-size: 22px">
                Descricao
              </th>
              <th style="width: 25%;font-size: 22px">
                Montante
              </th>
              <th style="width: 25%;font-size: 22px">
                Estado
              </th>
              <th style="width: 25%;font-size: 22px">
                Opções
              </th>
            </tr>
          </thead>
          <tbody>
          <template v-for="item in items">
            <tr>
              <td><p>@{{item.descripcion}}</p></td>
              <td><p>@{{item.monto}}</p></td>
              <td>
                <template v-if="item.status == 1">
                  <span class="label label-success">
                    Ativo
                  </span>
                </template>
                <template v-if="item.status == 0">
                  <span class="label label-danger">
                    Inativo
                  </span>
                </template>
              </td>
              <td>
                <div class="icon">
                  <a href="#" class="text-warning" v-on:click="editItem(item.id)"><i class="glyphicon glyphicon-pencil" v-on:click="editItem(item.id)" style="width: 20%;font-size: 20px"></i></a>
                    
                  <a href="#" class="text-danger" v-on:click="destroyItem(item.id)"><i  class="glyphicon glyphicon-trash"  v-on:click="destroyItem(item.id)" style="font-size: 20px"></i></a>                 
                           
                </div>
              </td>
            </tr>
            </template>
          </tbody>
        </table>
      </div>
    </div>
    <div class="box-footer">
      <div class="row">
        <div class="col-sm-7 col-md-offset-5">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            <ul class="pagination pull-right">
              <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                  <span aria-hidden="true">«</span>
                </a>
              </li>
              <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#" @click.prevent="changePage(page)">
                  @{{ page }}
                </a>
              </li>
              <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                  <span aria-hidden="true">»</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('elements')
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Penalidad</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Descrição</label>
                <input type="text" v-model="fillItem.descripcion" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Montante o Penalidades</label>
                <input type="text" v-model="fillItem.monto" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Estado</label>
                <select v-model="fillItem.status" class="form-control">
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>
<!-- /.table-responsive -->
@endsection
@section('js')
{!!Html::script('public/js/vue/penalidad.js')!!}
@endsection

@section('title', 'Históricos de Solicitação')
@section('description', 'Históricos de Observações')
@extends('layouts.backend.template2')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <div class="col-md-10">
            <h2 class="box-title">Histórico do Observações</h2>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
              <tr>
                <th style="width: 10%;font-size: 12px">Data</th>
                <th style="width: 5%;font-size: 12px">DOC. IDENT</th>
                <th style="width: 10%;font-size: 12px">Cliente</th>
                <th style="width: 10%;font-size: 12px">Beneficiario</th>
                <th style="width: 10%;font-size: 12px">Banco</th>
                <th style="width: 10%;font-size: 12px">BRL Enviado</th>
                <th style="width: 10%;font-size: 12px">BSF Taxa</th>
                <th style="width: 10%;font-size: 12px">BSF Total</th>
                <th style="width: 10%;font-size: 12px">Status</th>
                <th style="width: 15%;font-size: 12px">Opções</th>
              </tr>
              </thead>
              <tbody>
                <template v-for="item in items">
                  <tr>
                    <td><p>@{{dateFormat(item.fecha)}}</p></td>
                    <td><p> @{{item.cliente.cep}}</p></td>
                    <td><p>@{{item.cliente.nombre}} @{{item.cliente.apellido}}</p></td>
                    <td><p>@{{item.beneficiario.nombre}} @{{item.beneficiario.apellido}}</p></td>
                    <td><p>@{{item.banco.nombre}}</p></td>
                    <td><p> @{{format(item.taza,2)}}</p></td>
                    <td><p> @{{format(item.monto,2)}}</p></td>
                    <td><p> @{{format(item.total,2)}}</p></td>
                    <td align="center">
                      <template v-if="item.status == 1">
                        <span class="label label-info">
                          PEDENTE
                        </span>
                      </template>
                      <template v-if="item.status == 2">
                        <span class="label label-warning">
                          EM PROCESSO
                        </span>
                      </template>
                      <template v-if="item.status == 3">
                        <span class="label label-success">
                          PAGO
                        </span>
                      </template>
                      <template v-if="item.status == 4">
                        <span class="label label-danger">
                          REJEITADO
                        </span>
                      </template>
                    </td>
                    <td class="ico_estado">
                      <div class="container-fluid">
                        <div class="col-md-4">
                          <a href="#" style="font-size: 1.5em" class="text-info"  v-on:click.prevent="reporte(item.comentario)" title="Observações"><span class="glyphicon glyphicon-list-alt"></span></a>
                        </div>
                      </div>
                    </td>
                  </tr>
                </template>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-7 col-md-offset-5">
              <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                <ul class="pagination pull-right">
                  <li v-if="pagination.current_page > 1">
                    <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                      <span aria-hidden="true">«</span>
                    </a>
                  </li>
                  <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                    <a href="#" @click.prevent="changePage(page)">
                      @{{ page }}
                    </a>
                  </li>
                  <li v-if="pagination.current_page < pagination.last_page">
                    <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                      <span aria-hidden="true">»</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- /.content -->
  </div>
</section>
@endsection
@section('elements')

<div class="modal fade" id="reporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Observações</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="font-size: 22px">Observação</th>
              <th style="font-size: 22px">Usuário</th>
              <th style="font-size: 22px">STATUS</th>
              <th style="font-size: 22px">Data</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in reports">
              <tr>
                <td><p> @{{item.descripcion}}</p></td>
                <td><p> @{{item.usuario.nombre}} @{{item.usuario.apellido}}</p></td>
                <td align="center">
                  <template v-if="item.status == 1">
                    <span class="label label-info">
                      PENDENTE
                    </span>
                  </template>
                  <template v-if="item.status == 2">
                    <span class="label label-warning">
                      EM PROCESSO
                    </span>
                  </template>
                  <template v-if="item.status == 3">
                    <span class="label label-success">
                      PAGO
                    </span>
                  </template>
                  <template v-if="item.status == 4">
                    <span class="label label-danger">
                      REJEITADO
                    </span>
                  </template>

                </td>
                <td><p> @{{item.created_at}}</p></td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      </div>
      <div class="modal-footer">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>-->
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
{!!Html::script('js/moment.js')!!}
{!!Html::script('js/moment-with-locales.js')!!}
{!!Html::script('js/vue/observacion.js')!!}
{!!Html::script('js/solicitudes.js')!!}
@endsection

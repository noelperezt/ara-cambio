@extends('layouts.backend.template')

@section('title', 'Painel de Controle')
@section('description', '    Inicio')

@section('content')
<section class="content">
@if (Session::has('status'))
<?php
echo "
<script language='JavaScript'>
alert('Senha Alterada com Sucess!.');
</script>";
?>
@endif
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <p>Registrar</p>
          <h4><strong>Solicitação</strong></h4>
          <h5>&nbsp;</h5>
        </div>
        <div class="icon">
          <i class="ion ion-android-document"></i>
        </div>
        <a href="{{ url('solicitud') }}" class="small-box-footer"><div align="right">Ir&nbsp;&nbsp;&nbsp;<i class="ion ion-chevron-right"></i><i class="ion ion-chevron-right"></i>&nbsp;&nbsp;</div></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <p>Listar</p>
          <h4><strong>Moedas</strong></h4>
          <h5>&nbsp;</h5>
        </div>
        <div class="icon">
          <i class="ion ion-cash"></i>
        </div>
        <a href="{{ url('moneda') }}" class="small-box-footer"><div align="right">Ir&nbsp;&nbsp;&nbsp;<i class="ion ion-chevron-right"></i><i class="ion ion-chevron-right"></i>&nbsp;&nbsp;</div></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <p>Registrar</p>
          <h4><strong>Clientes</strong></h4>
          <h5>&nbsp;</h5>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="{{ url('cliente') }}" class="small-box-footer"><div align="right">Ir&nbsp;&nbsp;&nbsp;<i class="ion ion-chevron-right"></i><i class="ion ion-chevron-right"></i>&nbsp;&nbsp;</div></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <p>Registrar</p>
          <h4><strong>Beneficiários</strong></h4>
          <h5>&nbsp;</h5>
        </div>
        <div class="icon">
          <i class="ion ion-briefcase"></i>
        </div>
        <a href="{{ url('beneficiario') }}" class="small-box-footer"><div align="right">Ir&nbsp;&nbsp;&nbsp;<i class="ion ion-chevron-right"></i><i class="ion ion-chevron-right"></i>&nbsp;&nbsp;</div></a>
      </div>
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->


  

  <div class="row">
    <section class="col-lg-12 connectedSortable">
      <div class="box box-info">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-lg-6">
              <form name="formulario" id="formulario">
                <span class="label bg-yellow">Valor </span>
                <input class="form-control" type="text" name="cantidad" id="cantidad" onkeyup="calcular()">
                <br>

                <span class="label bg-yellow">Enviar DE/PARA</span>
                <select class="form-control" name="opciones" id="opciones" onChange="calcular()">
                  @foreach($items as $item)
                   <option value="brl_{{$item['moneda']}}">Reais a {{$item['moneda']}}</option>
                   <option value="{{$item['moneda']}}_brl">{{$item['moneda']}} a Reais</option>
                  @endforeach
                </select>
                <br>
 
                <span class="label label-success">Total </span>
                <div class="input-group">
                  <input class="form-control" type="text" name="resultado" readonly="readonly">
                  <div class="input-group-btn">
                    <button class="btn btn-default" type="button"  onclick="limpiar()">
                      <i class="glyphicon glyphicon-trash"></i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-lg-6">
              <div class="tradingview-widget-container">
                <div class="tradingview-widget-container__widget"></div>
                <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-forex-cross-rates.js" async>
                {
                "currencies": [
                  "BRL",
                  "EUR",
                  "USD",
                  "MXN",
                  "AUD"
                ],
                "width": "100%",
                "height": "100%",
                "locale": "br"
              }
                </script>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <div class="row">
        <div class="col-lg-12">
          <!-- TradingView Widget BEGIN -->

          <div class="tradingview-widget-container">
            <div class="tradingview-widget-container__widget"></div>
            <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>
            {
            "symbols": [
              {
                "proName": "BITFINEX:BTCUSD",
                "title": "BTC/USD"
              },
              {
                "proName": "BITFINEX:ETHUSD",
                "title": "ETH/USD"
              },
              {
                "description": "BTC/BRL",
                "proName": "MERCADO:BTCBRL"
              },
              {
                "description": "ETH/EUR",
                "proName": "BITSTAMP:ETHEUR"
              },
              {
                "description": "LTC/BRL",
                "proName": "MERCADO:LTCBRL"
              }
            ],
            "locale": "br"
          }
            </script>
          </div>
        </div>
      </div>
    </div>

    <div class="item">
      <div class="row">
        <div class="col-lg-12">
          <!-- TradingView Widget BEGIN -->
          <div class="tradingview-widget-container">
            <div class="tradingview-widget-container__widget"></div>
            <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>
            {
            "symbols": [
              {
                "proName": "FX_IDC:EURUSD",
                "title": "EUR/USD"
              },
              {
                "description": "BRL/EUR",
                "proName": "FX_IDC:BRLEUR"
              },
              {
                "description": "BRL/USD",
                "proName": "FX_IDC:BRLUSD"
              },
              {
                "description": "BRL/VEF",
                "proName": "FX_IDC:BRLVEF"
              },
              {
                "description": "BRL/COP",
                "proName": "FX_IDC:BRLCOP"
              }
            ],
            "locale": "br"
          }
            </script>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </div>

  </div>
</div> 
</section>

<script id="jsbin-javascript">
function limpiar(){
  document.getElementById("cantidad").value="";
  document.formulario.resultado.value ="";
} 

function calcular (){
  var tipo = document.getElementById("opciones").value;
  var cantidad = document.getElementById("cantidad").value;
  var resultado;
  const noTruncarDecimales = {maximumFractionDigits: 20};
  
  if( (cantidad - 0) != cantidad && (''+cantidad).replace(/^\s+|\s+$/g, "").length > 0)
    {
      alert("Informar numeros");
      return false;
    }
  else
    {
      @foreach($items as $item)
       if(tipo === "brl_{{$item['moneda']}}")
        {
        resultado = cantidad * {{$item['tasa']}};
        }
        if(tipo === "{{$item['moneda']}}_brl")
        {
        resultado = cantidad / {{$item['tasa']}};
        }
      @endforeach

      return document.formulario.resultado.value = resultado.toLocaleString('es', noTruncarDecimales);
    }
}


</script>
@endsection

<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="text-center image">
      <img alt="User Image" class="img-circle" src="dist/img/logoara.png">
      </img>
    </div>
  </div>
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">
      Menu de Navegação
    </li>
    <li class="active">
      <a href="{{ url('/') }}">
        <i class="fa fa-home ">
        </i>
        <span>
          Principal
        </span>
      </a>
    </li>
  </ul>
  @if(Auth::user()->rol == "Administrador" || Auth::user()->rol == "Operador")
  <ul class="sidebar-menu" data-widget="tree">
    <li class="treeview">
      <a href="#">
        <i class="fa fa-cog ">
        </i>
        <span>
          Cadastros Principais        </span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right">
          </i>
        </span>
      </a>
      <ul class="treeview-menu">
        @if(Auth::user()->rol == "Administrador")
        <li>
          <a href="{{ url('banco') }}">
            <i class="fa fa-circle-o">
            </i>
            Bancos
          </a>
        </li>
        @endif
        <li>
          <a href="{{ url('beneficiario') }}">
            <i class="fa fa-circle-o">
            </i>
            Beneficiários
          </a>
        </li>
        <li>
          <a href="{{ url('cliente') }}">
            <i class="fa fa-circle-o">
            </i>
            Clientes
          </a>
        </li>
        <li>
          <a href="{{ url('cuentas') }}">
            <i class="fa fa-circle-o">
            </i>
            Contas Bancárias
          </a>
        </li>
        @if(Auth::user()->rol == "Administrador")
        <li>
          <a href="{{ url('moneda') }}">
            <i class="fa fa-circle-o">
            </i>
            Moedas
          </a>
        </li>
        <li>
          <a href="{{ url('paises') }}">
            <i class="fa fa-circle-o">
            </i>
            Paises
          </a>
        </li>
        <li>
          <a href="{{ url('penalidades') }}">
            <i class="fa fa-circle-o">
            </i>
            Multas e Penalidades
          </a>
        </li>
        <li>
          <a href="{{ url('tipo_pago') }}">
            <i class="fa fa-circle-o">
            </i>
            Tipo de Pagamento
          </a>
        </li>
        <li>
          <a href="{{ url('unidades') }}">
            <i class="fa fa-circle-o">
            </i>
            Cadastro de Filiais
          </a>
        </li>
        @endif
      </ul>
    </li>
  </ul>
  @endif
  @if(Auth::user()->rol == "Administrador")
  <ul class="sidebar-menu" data-widget="tree">
    <li>
      <a href="{{ url('usuario') }}">
        <i class="fa fa-user ">
        </i>
        <span>
          Cadastro de Usuários
        </span>
      </a>
    </li>
  </ul>
  @endif
  <ul class="sidebar-menu" data-widget="tree">
    <li>
      <a href="{{ url('caxa') }}">
        <i class="fa fa-briefcase ">
        </i>
        <span>
          Controle de Caixa
        </span>
      </a>
    </li>
  </ul>
  <ul class="sidebar-menu" data-widget="tree">
    <li>
      <a href="{{ url('solicitud') }}">
        <i class="fa fa-file">
        </i>
        <span>
          Registrar Solicitação 
        </span>
      </a>
    </li>
  </ul>
  <ul class="sidebar-menu" data-widget="tree">
    <li class="treeview">
      <a href="#">
        <i class="fa fa-folder-open">
        </i>
        <span>
          Relatórios
        </span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right">
          </i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li>
          <a href="{{ url('observacion') }}">
            <i class="fa fa-circle-o">
            </i>
            Históricos de Observações
          </a>
        </li>
        <li>
          <a href="{{ url('historico_tasa') }}">
            <i class="fa fa-circle-o">
            </i>
            Históricos de Taxas
          </a>
        </li>
        <li>
          <a href="{{ url('estado_cuenta') }}">
            <i class="fa fa-circle-o">
            </i>
            Relatório de Movimentação
          </a>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-bar-chart"></i>  Gráficos e Analises
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="{{ url('relatoriovendas') }}">
                <i class="fa fa-circle-o"></i>Gráficos de Vendas 
              
              </a>
            
            </li>
        
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</section>

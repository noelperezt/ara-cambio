<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a class="sidebar-toggle" data-toggle="push-menu" href="#" role="button">
    <span class="sr-only">
      Toggle navigation
    </span>
  </a>
  <div id="" style="
  display: inline;
  margin-left: 10px;
  font-size: 14px;
  font-family: 'Trebuchet MS', Tahoma, Arial, sans-serif !important;
  position: relative;
  top: 15px;
  color: white;
  ">

   <span class="hidden-xs"> <strong> Taxa do Dia = <?php $taxa=session()->get('taxa'); echo number_format($taxa,2,',','.') ?></strong></span>

    </div>
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <li class="dropdown user user-menu">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          Bem vindo,
            <span class="hidden-xs">
               &nbsp;<strong>{{Auth::user()->nombre}} {{Auth::user()->apellido}}</strong>
            </span>
          </img>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <img alt="User Image" class="img-circle" src="dist/img/user.png">
              <p>
                {{Auth::user()->nombre}} {{Auth::user()->apellido}}
                <small>
                  {{Auth::user()->rol}}
                </small>
              </p>
            </img>
          </li>
          <li class="user-footer">
            <div class="pull-left">
              <a class="btn btn-default btn-flat" href="{{url('resetpassword')}}">
               Alterar Senha
              </a>
            </div>
            <div class="pull-right">
              <a class="btn btn-danger btn-flat" href="{{ url('login/logout') }}">
              Sair
              </a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
@section('js')
<script type="text/javascript">

  
  $(function () {
    jQuery.ajax({
      url: '/monto/index',
      dataType: 'json',
      success: function (response) {
          var json = jQuery.parseJSON(JSON.stringify(response));
          $("#nome").html(json[0]['monto']);
         
          // alert(json[0]['monto']);
        }
      
   });
   
  })
</script>
@endsection
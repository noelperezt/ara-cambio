@if(Auth::user()->rol != "Administrador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@section('title', 'Usuario')
@section('description', '')
@extends('layouts.backend.template2')
@section('content')
<section class="content" style="min-height: 100px">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <div class="form-group">
            <div class="col-md-10"><h3 class="box-title">CADASTRO DE USUÁRIOS </h3>
            </div>
           <!-- <div class="col-md-2">
              <i class="glyphicon glyphicon-search" data-toggle="modal" data-target="#buscar" style="padding-top: 7px;font-size: 25px ;cursor: pointer"></i>
            </div>-->
          </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
          <form role="form">
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.nome}">
                <label for="exampleInputEmail1">NOME</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItem.nome" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.nome">@{{formErrors.errors.nome}}</span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.nome2}">
                <label for="exampleInputEmail1">SOBRENOME</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItem.nome2" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.nome2">@{{formErrors.errors.nome2}}</span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.username}">
                <label for="exampleInputEmail1">NOME DO USUÁRIO</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItem.username" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.username">@{{formErrors.errors.username}}</span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.senha}">
                <label for="exampleInputEmail1">SENHA</label>
                <input type="password" class="form-control" id="exampleInputEmail1" v-model="newItem.senha" placeholder="Escreva aqui"">
                <span class="help-block text-danger" v-if="formErrors.errors.senha">@{{formErrors.errors.senha}}</span>
              </div>
            </div> 
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':pass}">
                <label for="exampleInputEmail1">CONFIRMAR SENHA</label>
                <input type="password" class="form-control" id="exampleInputEmail1" v-model="newItem.senha1" placeholder="Escreva aqui" v-on:keyup="passConfirm">
                <span class="help-block text-danger" v-if="pass">Essas senhas não coincidem. Tentar Novamente</span>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-2">
                <a href="#" v-on:click.prevent="createItem" type="submit"><i class=" glyphicon glyphicon-plus" ; style="padding-top: 30px;font-size: 25px"></i></a>
              </div>
            </div>
            <div class="clearfix"></div>
              <div class="col-md-3">
                <div class="form-group" v-bind:class="{'has-error':formErrors.errors.rol}">
                   <label for="exampleInputEmail1">SELECIONE O USUÁRIO</label>
                    <select class="form-control select2" v-model="newItem.rol">
                      <option selected="selected" value="Administrador">Administrador</option>
                      <option value="Operador">Operador</option>
                      <option value="Receptor">Receptor</option>
                    </select>
                  <span class="help-block text-danger" v-if="formErrors.errors.rol">@{{formErrors.errors.rol}}</span>
                  </div>
              </div>
              <div class="col-md-3">
                <div class="form-group" v-bind:class="{'has-error':formErrors.errors.unidad}">
                   <label for="exampleInputEmail1">FILIAL</label>
                   <select v-model="newItem.unidad" class="form-control" id="">
                      <option value="" disabled="">SELECIONE</option>
                      <template v-for="unidad in unidades">
                        <option value="@{{unidad.id}}">@{{unidad.descripcion}}</option>
                      </template>
                    </select>
                  <span class="help-block text-danger" v-if="formErrors.errors.unidad">@{{formErrors.errors.unidad}}</span>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
<section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
      <h2 class="box-title">Lista de Usuários Cadastrados</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
          <tr>
            <th style="width: 20%;font-size: 20px">NOME DO FUNCIONÁRIO</th>
            <th style="width: 15%;font-size: 20px">NOME DE USUÁRIO</th>
            <th style="width: 15%;font-size: 20px">FUNÇÃO</th>
            <th style="width: 10%; font-size: 20px">STATUS</th>
            <th style="width: 10%;font-size: 22px">OPÇÕES</th>
          </tr>
          </thead>
          <tbody>
            <template v-for="item in items">
              <tr>
                <td><a href="pages/examples/invoice.html"> @{{item.nombre}} @{{item.apellido}}</a></td>
                <td><a href="pages/examples/invoice.html"> @{{item.username}}</a></td>
                <td><a href="pages/examples/invoice.html"> @{{item.rol}}</a></td>
                <td>
                  <template v-if="item.status == 1">
                    <span class="label label-success">
                      Ativo
                    </span>
                  </template>
                  <template v-if="item.status == 0">
                      <span class="label label-danger">
                        Inativo
                      </span>
                    </template>
                </td>
                <td>
                  <div class="icon">
                    <a href="#" class="text-warning" v-on:click.prevent="editItem(item.id)"><i class="glyphicon glyphicon-pencil" v-on:click="editItem(item.id)" style="width: 20%;font-size: 20px"></i></a>
                      
                    <a href="#" class="text-danger" v-on:click="destroyItem(item.id)"><i  class="glyphicon glyphicon-trash"  v-on:click="destroyItem(item.id)" style="font-size: 20px"></i></a>
                  </div>
                </td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
    </div>
    <div class="box-footer">
      <div class="row">
        <div class="col-sm-7 col-md-offset-5">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            <ul class="pagination pull-right">
              <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                  <span aria-hidden="true">«</span>
                </a>
              </li>
              <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#" @click.prevent="changePage(page)">
                  @{{ page }}
                </a>
              </li>
              <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                  <span aria-hidden="true">»</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('elements')
<div class="modal fade" id="agregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Pesquisa</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -15px">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="row">
      
      <div class="col-md-12">
                        
          <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Digite o Usuário</label>
                          <select class="form-control select2">

                            <option selected="selected">Administrador</option>
                            <option>Operador</option>
                            <option>Receptor</option>
                          </select>
                          </div>
                        </div>

                  
                        <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">NOME </label>
                          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Escreva aqui">
                           </div>
                        </div>

                        <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1"> SOBRENOME </label>
                          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Escreva aqui">
                           </div>
                        </div>                      
                        <div class="col-md-3">
                        <div class="form-group">
                           <label for="exampleInputEmail1">STATUS    </label>
                          <select class="form-control select2">

                            <option selected="selected">Activo</option>
                            <option>Inactivo</option>
                            <option>Procesando</option>
                          </select>
                          </div>
                        </div>
                </div>
           </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar </button>
        <button type="button" class="btn btn-primary">Salvar </button>
      </div>
    </div>
  </div>
</div>


  <!-- Editar Elementos -->
 <div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ALTERAR USUÁRIO.</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
          <div class="row">
            <div class="col-md-4">
            
                        <div class="form-group">
                          <label for="exampleInputEmail1">NOME</label>
                          <input type="text" class="form-control" id="exampleInputEmail1" v-model="fillItem.nome" placeholder="Escreva aqui">
                           </div>
                        </div>

                        <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">SOBRENOME</label>
                          <input type="text" class="form-control" id="exampleInputEmail1" v-model="fillItem.nome2" placeholder="Escreva aqui">
                           </div>
                        </div>

                        <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">NOME DE USUÁRIO</label>
                          <input type="text" class="form-control" id="exampleInputEmail1" v-model="fillItem.username" placeholder="Escreva aqui">
                           </div>
                        </div>

                        

                   <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">SENHA</label>
                          <input type="password" class="form-control" id="exampleInputEmail1" v-model="fillItem.senha" placeholder="Escreva aqui"">
                           </div>
                        </div>
                      <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">CONFIRMAR SENHA</label>
                          <select class="form-control select2" v-model="fillItem.rol">

                            <option>Administrador</option>
                            <option>Operador</option>
                            <option>Receptor</option>
                          </select>
                          </div>
                        </div>

                        <div class="col-md-5">
                        <div class="form-group">
                           <label for="exampleInputEmail1">FILIAL</label>
                          <select v-model="fillItem.unidad" class="form-control" id="">
                              <option value="" disabled="">SELECIONE</option>
                              <template v-for="unidad in unidades">
                                <option value="@{{unidad.id}}">@{{unidad.descripcion}}</option>
                              </template>
                            </select>
                          </div>
                        </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Salvar </button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('js/vue/usuario.js')!!}
@endsection
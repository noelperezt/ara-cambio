@section('title', 'Alterar minha senha')
@section('description', 'Senha')
@extends('layouts.backend.template')
@section('content')
<section class="content" style="min-height: 100px">
<div class="box-header with-border">
          <h2 class="box-title">
           Alterar minha senha
          </h2>
        </div>
@if (Session::has('message'))
 <div class="text-danger">
 {{Session::get('message')}}
 </div>
@endif
<hr />
<form method="post" action="{{url('user/updatepassword')}}">
 {{csrf_field()}}
 <div class="form-group">
 <div class="col-md-3">
  <label for="mypassword">Digite sua senha atual:</label>
  <input type="password" name="mypassword" class="form-control" placeholder="Escreva aqui">
  <div class="text-danger">{{$errors->first('mypassword')}}</div>
 </div>
 </div>
 <div class="form-group">
<div class="col-md-3">
  <label for="password">Digite sua nova senha:</label>
  <input type="password" name="password" class="form-control" placeholder="Escreva aqui">
  <div class="text-danger">{{$errors->first('password')}}</div>
 </div>
 </div>
 <div class="form-group">
 <div class="col-md-3">
  <label for="mypassword">Confirme sua nova senha:</label>
  <input style="padding-top: 10px"  type="password" name="password_confirmation" class="form-control" placeholder="Escreva aqui">
 </div>
 </div>
 <br>
 <div class="form-group pull-rigth">
 <div class="col-md-12">
 <button  type="submit" class="btn btn-primary">Alterar minha senha</button>
  </div>
 </div>
 </section>
</form>
@stop

@if(Auth::user()->rol != "Administrador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@extends('layouts.backend.template')
@section('title', 'Moedas')
@section('description', '')
@section('content')
<section class="content" style="min-height: 100px">

  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Cadastrar Taxa de Moedas</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
          <form role="form" v-on:submit.prevent="createItem">
          <div class="col-md-3">
            <div class="form-group" v-bind:class= "{'has-error':formErrors.errors.pais}">
              <label for="exampleInputEmail1">PAÍS</label>
              <select class="form-control select2" v-model="newItem.pais">
                <option selected="selected" value="">SELECIONE</option>
                <template v-for="pais in paises">
                  <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                </template>
              </select>
              <span class="help-block text-danger" v-if="formErrors.errors.pais">@{{formErrors.errors.pais}}</span>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group"  v-bind:class="{'has-error':formErrors.errors.moeda}">
              <label for="exampleInputEmail1">MOEDA</label>
                <input type="text" class="form-control" v-model="newItem.moeda" id="exampleInputEmail1" placeholder="Moeda">
            <span class="help-block text-danger" v-if="formErrors.errors.moeda">@{{formErrors.errors.moeda}}</span>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group" v-bind:class= "{'has-error':formErrors.errors.copo}">
              <label for="exampleInputEmail1">TAXA DE CÂMBIO</label>
              <div class="input-group">
                <input type="number" v-model="newItem.copo" class="form-control" id="exampleInputEmail1" placeholder="Taxa">
                <span class="input-group-addon" id="basic-addon2">BRL</span>
              </div>
              <span class="help-block text-danger" v-if="formErrors.errors.copo">@{{formErrors.errors.copo}}</span>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">
              <a href="#" v-on:click="createItem"><i class="glyphicon glyphicon-plus"; style="padding-top: 30px;font-size: 25px" ></i></a>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
      <h2 class="box-title">Lista de Moedas Cadastradas</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="width: 20%;font-size: 22px">PAÍS</th>
              <th style="width: 20%;font-size: 22px">MOEDA</th>
              <th style="width: 20%;font-size: 22px">TAXA</th>
              <th style="width: 20%;font-size: 22px">STATUS</th>
              <th style="width: 20%;font-size: 22px">OPÇÕES</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in items">
              <tr>
                <td><p> @{{item.pais.descripcion}}</p></td>
                <td><p> @{{item.moneda}}</p></td>
                <td><p> @{{format(item.tasa,2)}}</p></td>
                <td>
                  <template v-if="item.status == 1">
                    <span class="label label-success">
                      Ativo
                    </span>
                  </template>
				  <template v-if="item.status == 0">
                    <span class="label label-danger">
                      Inativo
                    </span>
                  </template>
                </td>
                <td>
                  <div class="icon">
                    <a href="#" class="text-warning" v-on:click="editItem(item.id)"><i class="glyphicon glyphicon-pencil"; style="width: 20%;font-size: 20px"></i></a>
                  </div>
                </td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <div class="box-footer">
      <div class="row">
        <div class="col-sm-7 col-md-offset-5">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            <ul class="pagination pull-right">
              <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                  <span aria-hidden="true">«</span>
                </a>
              </li>
              <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#" @click.prevent="changePage(page)">
                  @{{ page }}
                </a>
              </li>
              <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                  <span aria-hidden="true">»</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('elements')
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ALTERAR MOEDA</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="">MOEDA</label>
                <input type="text" v-model="fillItem.moeda" class="form-control">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">TAXA</label>
                <div class="input-group">
                  <input type="number" v-model="fillItem.copo" class="form-control" id="exampleInputEmail1" placeholder="Taxa">
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">PAÍS</label>
                <select v-model="fillItem.pais" class="form-control">
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">STATUS</label>
                <select v-model="fillItem.status" class="form-control">
                  <option value="1">Ativo</option>
                  <option value="0">Inativo</option>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Salvar </button>
      </div>
    </div>
  </div>
</div> 
@endsection
@section('js')
{!!Html::script('js/vue/moneda.js')!!} 
@endsection
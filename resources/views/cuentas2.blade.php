@if(Auth::user()->rol != "Administrador" && Auth::user()->rol != "Operador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@extends('layouts.backend.template')
@section('title', 'Conta Bancárias')
@section('description', 'Lista da Contas')
@section('content')
<section class="content" style="min-height: 100px">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title" style="margin-top: 5px">Descrição da Conta Bancárias </h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <form role="form" v-on:submit="createItem">
              <div class="col-md-3">
                <div class="form-group" :class="{'has-error': formErrors.errors.beneficario}">
                  <label for="exampleInputEmail1">Beneficiario</label>
                  <select v-model="newItem.beneficario" v-selecttwo="newItem.beneficario" class="form-control beneficiario_select" name="beneficario" :disabled="!beneficiario">
                    <option value="" selected="selected" disabled="">Seleccione</option>
                    <template v-for="item in beneficiario">
                      <option value="" v-bind:value="item.id">@{{item.nombre}} @{{item.apellido}}</option>
                    </template>
                  </select>
                  <span class="help-block text-danger" v-if="formErrors.errors.beneficario">@{{formErrors.errors.beneficario}}</span>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" :class="{'has-error': formErrors.errors.banco}">
                  <label for="exampleInputEmail1">Banco</label>
                  <select class="form-control banco_select" v-selecttwo="newItem.banco" v-model="newItem.banco" :disabled="!bancos">
                    <option selected="selected" value="" disabled>Seleccione</option>
                    <template v-for="banco in bancos">
                      <option v-bind:value="banco.id">@{{banco.nombre}}</option>
                    </template>
                  </select>
                  <span class="help-block text-danger" v-if="formErrors.errors.banco">@{{formErrors.errors.banco}}</span>
                </div>
              </div>
           <div class="col-md-2">

              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.tipo_da_conta}">
                <label for="exampleInputEmail1">Tipo da conta</label>
                <select v-model="newItem.tipo_da_conta" class="form-control" id="">
                  <option value="" disabled="">Seleccione</option>
                  <option value="Corriente">Corriente</option>
                  <option value="Ahorro">Ahorro</option>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.tipo_da_conta">@{{formErrors.errors.tipo_da_conta}}</span>
              </div>
            </div>
              <div class="col-md-3">
                <div class="form-group" :class="{'has-error': formErrors.errors.numero_da_conta}">
                  <label for="exampleInputEmail1">Conta Bancária</label>
                  <input type="text" maxlength="20" v-model="newItem.numero_da_conta" class="form-control" id="exampleInputEmail1" placeholder="Escreva aqui">
                  <span class="help-block text-danger" v-if="formErrors.errors.numero_da_conta">@{{formErrors.errors.numero_da_conta}}</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-2">
                  <a href="#" v-on:click.prevent="createItem"><i class="glyphicon glyphicon-plus"; style="padding-top: 30px;font-size: 25px";></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>

 <section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
    <div class="col-md-10">
            <h2 class="box-title">Lista da Conta Bancárias</h2>
          </div>
          <div class="col-md-2" style="text-align: right;">
            <i class="glyphicon glyphicon-search" data-toggle="collapse" href="#busqueda" aria-expanded="false" aria-controls="busqueda" style="padding-top: 7px;font-size: 25px ;cursor: pointer"></i>
          </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
       <div class="collapse" id="busqueda">
            <div class="row">                       
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Beneficario</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" v-model="filter.beneficiario" placeholder="Escreva aqui" v-on:keyUp="filterItem">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Banco </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" v-model="filter.banco" placeholder="Escreva aqui" v-on:keyUp="filterItem">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tipo da conta</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" v-model="filter.tipo_da_conta" placeholder="Escreva aqui" v-on:keyUp="filterItem">
                </div>
            </div>
          </div>
        </div>
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
          <tr>
            <th style="width: 20%;font-size: 22px">Beneficiario</th>
            <th style="width: 15%;font-size: 22px">Banco</th>
            <th style="width: 20%;font-size: 22px">Tipo da conta</th>
            <th style="width: 20%;font-size: 22px">Conta Bancaria</th>
            <th style="width: 15%;font-size: 22px">Estado</th>
            <th style="width: 20%;font-size: 22px">Opções</th>
          </tr>
          </thead>
          <tbody>
            <template v-for="item in items">
              <tr>
                <td><p>@{{item.beneficiario.nombre}} @{{item.beneficiario.apellido}}</p></td>
                <td><p>@{{item.banco.nombre}}</p></td>
                <td><p>@{{item.tipo_cuenta}}</p></td>
                <td><p>@{{item.cuenta}}</p></td>
                <td>
                  <template v-if="item.status == 1">
                    <span class="label label-success">
                      Ativo
                    </span>
                  </template>
                  <template v-if="item.status == 0">
                    <span class="label label-danger">
                      Inativo
                    </span>
                  </template>
                </td>
                <td>
                  <div class="icon">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-md-6">
                          <a href="#" class="text-warning" v-on:click="editItem(item.id)"><i class="glyphicon glyphicon-pencil"; style="width: 30%;font-size: 20px"></i></a>
                        </div>
                      <template v-if="item.status == 1">
                        <div class="col-md-6">
                          <a href="#" style="font-size: 1.5em" class="text-danger" v-on:click.prevent="destroyItem(item.id)"><i class="glyphicon glyphicon-trash"></i></a>
                        </div>
                      </template>
                      <template v-if="item.status == 0">
                        <div class="col-md-6">
                          <a href="#" style="font-size: 1.5em" class="text-success" v-on:click.prevent="destroyItem(item.id)"><i class="glyphicon glyphicon-check"></i></a>
                        </div>
                      </template>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <div class="box-footer">
      <div class="row">
        <div class="col-sm-7 col-md-offset-5">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            <ul class="pagination pull-right">
              <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                  <span aria-hidden="true">«</span>
                </a>
              </li>
              <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#" @click.prevent="changePage(page)">
                  @{{ page }}
                </a>
              </li>
              <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                  <span aria-hidden="true">»</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('elements')
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar País.</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="exampleInputEmail1">Beneficiario</label>
                 <select v-model="fillItem.beneficario" class="form-control" name="beneficario" :disabled="!beneficiario">
                    <template v-for="item in beneficiario">
                      <option value="" v-bind:value="item.id">@{{item.nombre}} @{{item.apellido}}</option>
                    </template>
                  </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="exampleInputEmail1">Banco</label>
                <select class="form-control select2" v-model="fillItem.banco">
                  <option selected="selected" disabled>Seleccione</option>
                  <template v-for="banco in bancos">
                    <option v-bind:value="banco.id">@{{banco.nombre}}</option>
                  </template>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="exampleInputEmail1">Tipo de conta</label>
                <input type="text" v-model="fillItem.tipo_da_conta" class="form-control" id="exampleInputEmail1" placeholder="Escreva aqui">
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <label for="exampleInputEmail1">Conta Bancária</label>
                <input type="text" maxlength="20" v-model="fillItem.numero_da_conta" class="form-control" id="exampleInputEmail1" placeholder="Conta Bancária">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('js/vue/cuenta.js')!!}
<script>
  $('.beneficiario_select').select2({
    theme: "bootstrap"
  });
  $('.banco_select').select2({
    theme: "bootstrap"
  });
</script>
@endsection
@section('title', 'Relatórios de Vendas')
@section('description', 'Relatórios de Vendas por Meses')
@extends('layouts.backend.template2')
@section('content')
<section class="content">
  <div class="row">
      
        
            <div class="col-md-14">
                <!-- Relatorio de Vendas Filias -->
                <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Gráfico de Relatório de Vendas por Filias </h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="body">
                        <canvas id="canvas" height="150"></canvas>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
   
    
</section>
@endsection
@section('elements')

@endsection

@section('js')
{!! Html::script('js/chart.js/Chart.bundle.js') !!}
{!!Html::script('js/moment.js')!!}
{!!Html::script('js/moment-with-locales.js')!!}
<!-- page script -->
<script type="text/javascript">

  
  $(function () {
    jQuery.ajax({
      url: '/relatoriovendas/index',
      dataType: 'json',
      success: function (response) {
          var json = jQuery.parseJSON(JSON.stringify(response));
          var data1=[];
          var data2=[];
          var data3=[];
          var data4=[];
          
          for (i=0;i<12;i++){

            data1.push(json[0].Valores[i].Total);
            data2.push(json[1].Valores[i].Total);
            data3.push(json[2].Valores[i].Total);
            data4.push(json[3].Valores[i].Total);
         }

         var data = [{
          label: json[0].Filial.toUpperCase(),
          backgroundColor: "rgba(233, 30, 99,1)",
          borderColor: "rgba(233, 30, 99,1)",
          data: data1,
          fill: false,
      },{
          label: json[2].Filial.toUpperCase(),
          fill: false,
          backgroundColor: "rgba(139, 195, 74,1)",
          borderColor: "rgba(139, 195, 74,1)",
          data: data3,
      },
      {
          label: json[3].Filial.toUpperCase(),
          fill: false,
          backgroundColor: "rgba(255, 152, 0,1)",
          borderColor: "rgba(255, 152, 0,1)",
          data: data4,
      }];

      CargaGrafico(data); 

         
      }

      
   });
  
   function CargaGrafico(data){
    var MONTHS = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
                var config = {
                    type: 'line',
                    data: {
                        labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                        datasets: data
                    },
                    options: {
                        responsive: true,
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Mes'
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Valor (R$)'
                                }
                            }]
                        }
                    }
                };

                var ctx = document.getElementById('canvas').getContext('2d');
                if (window.myLine) {
                  window.myLine.clear();
                  window.myLine.destroy();
                }
                  window.myLine = new Chart(ctx, config);
                  window.myLine.update();
                  var ahora = moment();
                            var mes = ahora.format('MM');
                            var resta = 12 - mes;
                            var neg = -(resta);

                            console.log(ahora.format('MM'));
                            console.log(resta);

                            config.data.labels.splice(neg, resta); // remove the label first

			                config.data.datasets.forEach(function(dataset) {
				            dataset.data.pop();
			                });

			                window.myLine.update();
      }                

  })
</script>
@endsection
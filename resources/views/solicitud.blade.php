@section('title', 'REGISTRO DE SOLICITAÇÃO')
@section('description', '')
@extends('layouts.backend.template2')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <div class="col-md-10">
            <h2 class="box-title">Lista de Solicitação</h2>
          </div>
          <div class="col-md-2" style="text-align: right;">
            @if(Auth::user()->rol != "Receptor")
            <a href="#"><i class="glyphicon glyphicon-plus" v-on:click.prevent="getCodeItem" style="padding-top: 7px;font-size: 25px; width: 15%; padding-right:50px;"></i></a>
            @endif
            <i class="glyphicon glyphicon-search" data-toggle="collapse" href="#busqueda" aria-expanded="false" aria-controls="busqueda" style="padding-top: 7px;font-size: 25px ;cursor: pointer"></i>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="collapse" id="busqueda">
            <div class="row">                       
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">DATA INICIO</label>
                  <input type="date" class="form-control" id="exampleInputEmail1" v-model="filter.data_de_pedido" v-on:change="filterItem" placeholder=" Escreva aqui">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">DATA FINAL</label>
                  <input type="date" class="form-control" id="exampleInputEmail1" v-model="filter.data_de_pedido2" placeholder="Escreva aqui" v-on:change="filterItem">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">PRIORIDADE</label>
                    <select class="form-control prioridadbuscar" v-model="filter.prioridad"  v-selecttwo="filter.prioridad"  v-on:change="filterItem" style="width:100% ">
                      <option value="">Seleccione</option>
                      <option v-bind:value="1" value="1">Normal</option>
                      <option v-bind:value="2" value="1">Urgente</option>
                    </select>

                   <input type="text" class="form-control prioridad" id="exampleInputEmail1" v-model="filter.prioridad" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">CLIENTE</label>
                  <select class="form-control clientebuscar" v-model="filter.cliente"  v-selecttwo="filter.cliente" :disabled="!clientes" v-on:change="filterItem" style="width:100% ">
                      <option value="">SELECIONE</option>
                      <template v-for="cliente in clientes">
                        <option data-nombre="@{{cliente.nombre}} @{{cliente.apellido}}" data-id="@{{cliente.id}}"  data-cep="@{{cliente.cep}}" data-celular="@{{cliente.celular}}" v-bind:value="@{{cliente.id}}" value="@{{cliente.id}}">@{{cliente.nombre}} @{{cliente.apellido}}</option>
                      </template>
                    </select>
                   <input type="text" class="form-control cliente" id="exampleInputEmail1" v-model="filter.cliente" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">TIPO DE PAGAMENTO</label>
                  <select class="form-control pagobuscar" v-model="filter.tipo_pagamento"  v-selecttwo="filter.tipo_pagamento" :disabled="!pagos" v-on:change="filterItem" style="width:100% ">
                        <option value="">SELECIONE</option>
                        <template v-for="pago in pagos">
                          <option v-bind:value="@{{pago.id}}" value="@{{cliente.id}}">@{{pago.descripcion}}</option>
                        </template>
                      </select>
                   <input type="text" class="form-control pago" id="exampleInputEmail1" v-model="filter.tipo_pagamento" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1" style="margin-left: 5px">BANCO</label>
                    <select class="form-control bancobuscar" v-model="filter.banco"  v-selecttwo="filter.banco" :disabled="!bancos" v-on:change="filterItem" style="width:100% ">
                        <option value="">SELECIONE</option>
                        <template v-for="banco in bancos">
                          <option v-bind:value="@{{banco.id}}" value="@{{banco.id}}">@{{banco.nombre}}</option>
                        </template>
                      </select>
                   <input type="text" class="form-control banco" id="exampleInputEmail1" v-model="filter.banco" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                  </div> 
                </div> 
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1" style="margin-left: 5px">FILIAL</label>
                    <select class="form-control unidadbuscar" v-model="filter.unidad"  v-selecttwo="filter.unidad" :disabled="!unidades" v-on:change="filterItem" style="width:100% ">
                        <option value="">SELECIONE</option>
                        <template v-for="unidad in unidades">
                          <option v-bind:value="@{{unidad.id}}" value="@{{unidad.id}}">@{{unidad.descripcion}}</option>
                        </template>
                      </select>
                   <input type="text" class="form-control unidad" id="exampleInputEmail1" v-model="filter.unidad" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                  </div> 
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1" style="margin-left: 5px">USUÁRIO</label>
                    <select class="form-control usuariobuscar" v-model="filter.usuario"  v-selecttwo="filter.usuario" :disabled="!usuarios" v-on:change="filterItem" style="width:100% ">
                        <option value="">SELECIONE</option>
                        <template v-for="usuario in usuarios">
                          <option v-bind:value="@{{usuario.id}}" value="@{{usuario.id}}">@{{usuario.nombre}} @{{usuario.apellido}}</option>
                        </template>
                      </select>
                   <input type="text" class="form-control usuario" id="exampleInputEmail1" v-model="filter.usuario" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                  </div> 
                </div> 
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="exampleInputEmail1">STATUS</label>
                    <select class="form-control statusfbuscar" v-model="filter.statusf"  v-selecttwo="filter.statusf"  v-on:change="filterItem" style="width:100% ">
                      <option value="">TODOS</option>
                      <option v-bind:value="1" value="1">Pendientes</option>
                      <option v-bind:value="2" value="2">Em processo</option>
                      <option v-bind:value="3" value="3">Pago</option>
                      <option v-bind:value="4" value="4">Rejeitado</option>
                      <option v-bind:value="5" value="5">Reembolso</option>
                      <option v-bind:value="6" value="6">Revisão</option>
                    </select>

                   <input type="text" class="form-control statusf" id="exampleInputEmail1" v-model="filter.statusf" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                  </div> 
                </div> 
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table no-margin" style="font-size: 12px">
              <thead>
              <tr>
                <th style="font-size: 12px">ID</th>
                <th style="width: 7%;font-size: 12px">DATA</th>
                <!--<th style="width: 7%;font-size: 16px">CPF</th>-->
                <th style="font-size: 12px">CLIENTE</th>
                <th style="font-size: 12px">BENEFICIÁRIO</th>
                <th style="font-size: 12px">BANCO</th>
                <th style="font-size: 12px">BRL ENVIADO</th>
                <th style="font-size: 12px">BSF TAXA</th>
                <th style="font-size: 12px">BSF TOTAL</th>
                <th style="font-size: 12px">STATUS</th>
                <th style="font-size: 12px">USUÁRIO</th>
                <th style="font-size: 12px">OBSERVAÇÕES</th>
                <th style="font-size: 12px">ALTERAR STATUS</th>
                <th style="font-size: 12px">OPÇÕES</th>
              </tr>
              </thead>
              <tbody>
                <template v-for="item in items">
                  <tr v-if="item.prioriad == 2" style="color:red;">
                    <td><p>ARA.000@{{item.id}}</p></td>
                    <td><p>@{{dateFormat(item.fecha)}}</p></td>
                    <!--<td><p> @{{item.cliente.cep}}</p></td>-->
                    <td><p>@{{item.cliente.nombre}} @{{item.cliente.apellido}}</p></td>
                    <td><p>@{{item.beneficiario.nombre}} @{{item.beneficiario.apellido}}</p></td>
                    <td><p>@{{item.banco.nombre}}</p></td>
					<td><p> @{{format(item.monto,2)}}</p></td>
                    <td><p> @{{format(item.taza,2)}}</p></td>
                    <td><p> @{{format(item.total,2)}}</p></td>
                    <td align="center">
                      <template v-if="item.status == 1">
                        <span class="label label-info">
                          PENDENTE
                        </span>
                      </template>
                      <template v-if="item.status == 2">
                        <span class="label label-warning">
                          EM PROCESSO
                        </span>
                      </template>
                      <template v-if="item.status ==3">
                        <span class="label label-success">
                          PAGO
                        </span>
                      </template>
                      <template v-if="item.status == 4">
                        <span class="label label-danger">
                          REJEITADO
                        </span>
                      </template>
                      <template v-if="item.status == 5">
                        <span class="label" style="background-color: #7F8C8D">
                          REEMBOLSADO
                        </span>
                      </template>
                      <template v-if="item.status == 6">
                        <span class="label" style="background-color: #D35400">
                          EM REVISÃO
                        </span>
                      </template>
                    </td>
                    <td><p>@{{item.usuario.username}}</p></td>
                    <td class="ico_estado" align="center">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-6">
                            <a href="#" style="font-size: 1.5em" v-on:click.prevent="describir(item.id)" title="Adicionar Observações"><span class="glyphicon glyphicon-comment"></span></a>
                          </div>
                          <div class="col-md-6">
                            <a href="#" style="font-size: 1.5em"  v-on:click.prevent="reporte(item.comentario)" title="Historico Observações"><span class="glyphicon glyphicon-list-alt"></span></a>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td class="ico_estado">
                      <div class="container-fluid">
                        <div class="row">
                          <template v-if="item.status == 1">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-info" v-on:click.prevent="destroyItem(item.id, 2)"><i class="glyphicon glyphicon-transfer" title="Em processo"></i></a>
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em; color: #7F8C8D" class="text-info" v-on:click.prevent="destroyItem(item.id, 5)"><i class="glyphicon glyphicon-arrow-left" title="Reembolso"></i></a> 
                            </div>
                          </template>
                          <template v-if="item.status == 2">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-success" v-on:click.prevent="destroyItem(item.id, 3)"><i class="glyphicon glyphicon-ok" title="Pago"></i></a>
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-danger" v-on:click.prevent="destroyItem(item.id, 4)"><i class="glyphicon glyphicon-remove" title="Rejeitado"></i></a>
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em; color: #D35400" class="text-info" v-on:click.prevent="destroyItem(item.id, 6)"><i class="glyphicon glyphicon-alert" title="Revisão"></i></a> 
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em; color: #7F8C8D" class="text-info" v-on:click.prevent="destroyItem(item.id, 5)"><i class="glyphicon glyphicon-arrow-left" title="Reembolso"></i></a> 
                            </div>
                          </template>
                          <template v-if="item.status == 4">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-info" v-on:click.prevent="destroyItem(item.id, 2)"><i class="glyphicon glyphicon-transfer" title="Em processo"></i></a>
                            </div>
                          </template>
                          <template v-if="item.status == 6">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-info" v-on:click.prevent="destroyItem(item.id, 2)"><i class="glyphicon glyphicon-transfer" title="Em processo"></i></a>
                            </div>
                          </template>
                        </div>
                      </div>
                    </td>
                    <td class="ico_estado" align="center">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-6">
                          <a href="#" class="text-warning" v-on:click="editItem(item.id)"><i class="glyphicon glyphicon-pencil"; style="font-size: 1.5em"></i></a>
                        </div>
                          <div class="col-md-6">
                            <a target="_blank" href="{{ url('factura') }}/@{{item.id}}" style="font-size: 1.5em" class="text-warning print" title="Impressão"><i class="glyphicon glyphicon-print"></i></a>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr v-else style="color:black;">
                    <td><p>ARA.000@{{item.id}}</p></td>
                    <td><p>@{{dateFormat(item.fecha)}}</p></td>
                    <!--<td><p> @{{item.cliente.cep}}</p></td>-->
                    <td><p>@{{item.cliente.nombre}} @{{item.cliente.apellido}}</p></td>
                    <td><p>@{{item.beneficiario.nombre}} @{{item.beneficiario.apellido}}</p></td>
                    <td><p>@{{item.banco.nombre}}</p></td>
					<td><p> @{{format(item.monto,2)}}</p></td>
                    <td><p> @{{format(item.taza,2)}}</p></td>
                    <td><p> @{{format(item.total,2)}}</p></td>
                    <td align="center">
                      <template v-if="item.status == 1">
                        <span class="label label-info">
                          PENDENTE
                        </span>
                      </template>
                      <template v-if="item.status == 2">
                        <span class="label label-warning">
                          EM PROCESSO
                        </span>
                      </template>
                      <template v-if="item.status ==3">
                        <span class="label label-success">
                          PAGO
                        </span>
                      </template>
                      <template v-if="item.status == 4">
                        <span class="label label-danger">
                          REJEITADO
                        </span>
                      </template>
                      <template v-if="item.status == 5">
                        <span class="label" style="background-color: #7F8C8D">
                          REEMBOLSADO
                        </span>
                      </template>
                      <template v-if="item.status == 6">
                        <span class="label" style="background-color: #D35400">
                          EM REVISÃO
                        </span>
                      </template>
                    </td>
                    <td><p>@{{item.usuario.username}}</p></td>
                    <td class="ico_estado" align="center">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-6">
                            <a href="#" style="font-size: 1.5em" v-on:click.prevent="describir(item.id)" title="Adicionar Observações"><span class="glyphicon glyphicon-comment"></span></a>
                          </div>
                          <div class="col-md-6">
                            <a href="#" style="font-size: 1.5em"  v-on:click.prevent="reporte(item.comentario)" title="Historico Observações"><span class="glyphicon glyphicon-list-alt"></span></a>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td class="ico_estado">
                      <div class="container-fluid">
                        <div class="row">
                          <template v-if="item.status == 1">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-info" v-on:click.prevent="destroyItem(item.id, 2)"><i class="glyphicon glyphicon-transfer" title="Em processo"></i></a>
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em; color: #7F8C8D" class="text-info" v-on:click.prevent="destroyItem(item.id, 5)"><i class="glyphicon glyphicon-arrow-left" title="Reembolso"></i></a> 
                            </div>
                          </template>
                          <template v-if="item.status == 2">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-success" v-on:click.prevent="destroyItem(item.id, 3)"><i class="glyphicon glyphicon-ok" title="Pago"></i></a>
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-danger" v-on:click.prevent="destroyItem(item.id, 4)"><i class="glyphicon glyphicon-remove" title="Rejeitado"></i></a>
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em; color: #D35400" class="text-info" v-on:click.prevent="destroyItem(item.id, 6)"><i class="glyphicon glyphicon-alert" title="Revisão"></i></a> 
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em; color: #7F8C8D" class="text-info" v-on:click.prevent="destroyItem(item.id, 5)"><i class="glyphicon glyphicon-arrow-left" title="Reembolso"></i></a> 
                            </div>
                          </template>
                          <template v-if="item.status == 4">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-info" v-on:click.prevent="destroyItem(item.id, 2)"><i class="glyphicon glyphicon-transfer" title="Em processo"></i></a>
                            </div>
                          </template>
                          <template v-if="item.status == 6">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-info" v-on:click.prevent="destroyItem(item.id, 2)"><i class="glyphicon glyphicon-transfer" title="Em processo"></i></a>
                            </div>
                          </template>
                        </div>
                      </div>
                    </td>
                    <td class="ico_estado" align="center">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-6">
                          <a href="#" class="text-warning" v-on:click="editItem(item.id)"><i class="glyphicon glyphicon-pencil"; style="font-size: 1.5em"></i></a>
                        </div>
                          <div class="col-md-6">
                            <a target="_blank" href="{{ url('factura') }}/@{{item.id}}" style="font-size: 1.5em" class="text-warning print" title="Impressão"><i class="glyphicon glyphicon-print"></i></a>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </template>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-7 col-md-offset-5">
              <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                <ul class="pagination pull-right">
                  <li v-if="pagination.current_page > 1">
                    <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                      <span aria-hidden="true">«</span>
                    </a>
                  </li>
                  <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                    <a href="#" @click.prevent="changePage(page)">
                      @{{ page }}
                    </a>
                  </li>
                  <li v-if="pagination.current_page < pagination.last_page">
                    <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                      <span aria-hidden="true">»</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- /.content -->
  </div>
  <div id="impresion" style="display: none;"></div>
</section>
@endsection
@section('elements')
<!-- Modal -->
<div class="modal fade" id="agregar_cliente"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">CADASTRO DE CLIENTES</h4>
        <h5 class="modal-title" id="exampleModalLabel" style="padding-top: 10px;">CÓDIGO DO CLIENTE:  <b>@{{newItemCliente.codigo}}</b></h5>
        <span style="color: #721c24;background-color: #f8d7da;border-color: #f5c6cb;border: 1px solid transparent;border-radius: .25rem;padding: 17px;float: right;margin-top: -56px;" v-if="formErrorsCliente.message">@{{formErrorsCliente.message}}</span>
      </div>
      <div class="modal-body ">
        <form action="" v-on:submit="createItemCLiente">
          <div class="row"> 
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.nome}">
                <label for="exampleInputEmail1">NOME</label>
                <input type="text" class="form-control" v-model="newItemCliente.nome" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.nome">@{{formErrorsCliente.errors.nome}}</span>
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.ultimo_nome}">
                <label for="exampleInputEmail1">SOBRENOME </label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItemCliente.ultimo_nome" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errorsCliente.ultimo_nome">@{{formErrorsCliente.errors.ultimo_nome}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.cep}">
                <label for="exampleInputEmail1">DOC. IDENTIFICAÇÃO </label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItemCliente.cep" placeholder="Escreva aqui ">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.cep">@{{formErrorsCliente.errors.cep}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.data_de_nascimento}">
                <label for="exampleInputEmail1"> DATA DE NASCIMENTO</label>
                <input type="date" class="form-control" id="exampleInputEmail1" v-model="newItemCliente.data_de_nascimento" placeholder=" Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.data_de_nascimento">@{{formErrorsCliente.errors.dat_de_nacimiento}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group"  v-bind:class="{'has-error':formErrorsCliente.errors.profissao}">
                <label for="exampleInputEmail1">PROFISSÃO</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItemCliente.profissao" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.profissao">@{{formErrorsCliente.errors.profissao}}</span>
               </div>
            </div>
          </div>
        <div class="row"> 
          <div class="box-header with-border">
            <h2 class="box-title">DADOS DE LOCALIZAÇÃO</h2>
        </div>
         <div class="col-md-4">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.endereco_residencial}">
                <label for="exampleInputEmail1">ENDEREÇO</label>
                <input type="text" class="form-control" v-model="newItemCliente.endereco_residencial" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.endereco_residencial">@{{formErrorsCliente.errors.endereco_residencial}}</span>
               </div>
            </div>
         <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.bairrio}">
                 <label for="exampleInputEmail1">BAIRRO</label>
                <input type="text" class="form-control" v-model="newItemCliente.bairrio" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.bairrio">@{{formErrorsCliente.errors.bairrio}}</span>
              </div>
            </div>
         <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.cidades}">
                <label for="exampleInputEmail1">CIDADE</label>
                <input type="text" class="form-control" v-model="newItemCliente.cidades" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.cidades">@{{formErrorsCliente.errors.cidades}}</span>
              </div>
            </div>
        <div class="col-md-2">
              <div class="form-group"  v-bind:class="{'has-error':formErrorsCliente.errors.estado}">
                <label for="exampleInputEmail1">ESTADO</label>
                <input type="text" class="form-control" v-model="newItemCliente.estado" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.estado">@{{formErrorsCliente.errors.estado}}</span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.pais}">
                 <label for="exampleInputEmail1">Pais</label>
                <select class="form-control select2" v-model="newItemCliente.pais">
                  <option selected="selected" disabled="" value="">SELECIONE O PAÍS</option>
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>                          
                </select>
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.pais">@{{formErrorsCliente.errors.pais}}</span>
              </div>
            </div>
            </div>
          <div class="box-header with-border">
            <h2 class="box-title">DADOS DE CONTATO</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.telefone_de_localizacao}">
                <label for="exampleInputEmail1">TELEFONE </label>
                <input type="Text" class="form-control" v-model="newItemCliente.telefone_de_localizacao" id="exampleInputEmail1" placeholder ="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.telefone_de_localizacao">@{{formErrorsCliente.errors.telefone_de_localizacao}}</span> 
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.celular}">
                <label for="exampleInputEmail1">CELULAR</label>
              <input type="number" class="form-control" v-model="newItemCliente.celular" id="exampleInputEmail1" placeholder="Escreva aqui">
              <span class="help-block text-danger" v-if="formErrorsCliente.errors.celular">@{{formErrorsCliente.errors.celular}}</span>           </div>
            </div>
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCliente.errors.email}">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="Email" class="form-control" v-model="newItemCliente.email" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsCliente.errors.email">@{{formErrorsCliente.errors.email}}</span>
               </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" v-on:click.prevent="cerrar_modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click.prevent="createItemCLiente">Salvar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="agregar_beneficiario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">CADASTRO DE BENEFICIÁRIO</h4>
        <h5 class="modal-title" id="exampleModalLabel" style="padding-top: 10px;">Código do Beneficiário:   <b>@{{newItemBeneficiario.codigo}}</b></h5>
        <span style="color: #721c24;background-color: #f8d7da;border-color: #f5c6cb;border: 1px solid transparent;border-radius: .25rem;padding: 17px;float: right;margin-top: -56px;" v-if="formErrorsBeneficiario.message">@{{formErrorsBeneficiario.message}}</span>
      </div>
      <div class="modal-body ">
        <form action="" v-on:submit="createItemBeneficiario">
          <div class="row"> 
            <div class="col-md-2" >
              <div class="form-group" v-bind:class="{'has-error':formErrorsBeneficiario.errors.nome}">
                <label for="exampleInputEmail1">NOME</label>
                <input type="text" class="form-control" v-model="newItemBeneficiario.nome" id="exampleInputEmail1" placeholder="NOME DO BENEFICIÁRIO">
                <span class="help-block text-danger" v-if="formErrorsBeneficiario.errors.nome">@{{formErrorsBeneficiario.errors.nome}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsBeneficiario.errors.ultimo_nome}">
                <label for="exampleInputEmail1">SOBRENOME </label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItemBeneficiario.ultimo_nome" placeholder="SOBRENOME DO BENEFICIÁRIO">
                <span class="help-block text-danger" v-if="formErrorsBeneficiario.errors.ultimo_nome">@{{formErrorsBeneficiario.errors.ultimo_nome}}</span>
               </div>
            </div>
            <div class="col-md-3">
             <div class="form-group" v-bind:class="{'has-error':formErrorsBeneficiario.errors.cpf}">
                <label for="">DOC. IDENTIFICAÇÃO</label>
                <div class="input-group">
                  <select v-model="newItemBeneficiario.tipo" class="form-control prefijo" id="" style="width: 60px">
                            <option value="V">V - Venezuelano</option>
                            <option value="E">E - Estrageiro</option>
                            <option value="J">J - Juridico</option>
                            <option value="G">G - Governo</option>
                            <option value="P">P - Passaporte</option>
                          </select>
                  <span class="input-group-addon" style="width: 5px">-</span>
                  <input type="text" class="form-control identifacion" id="exampleInputEmail1" v-model="newItemBeneficiario.cpf" placeholder="INFORME O DOCUMENTO">
                </div>
              </div>
              <span class="help-block text-danger" v-if="formErrorsBeneficiario.errors.cpf">@{{formErrorsBeneficiario.errors.cpf}}</span>
             </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsBeneficiario.errors.ultimo_nome}" >
                <label for="exampleInputEmail1"> DATA DE NASCIMENTO</label>
                <input type="date" class="form-control" id="exampleInputEmail1" v-model="newItemBeneficiario.data_de_nascimento" placeholder=" Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsBeneficiario.errors.ultimo_nome">@{{formErrorsBeneficiario.errors.ultimo_nome}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsBeneficiario.errors.profissao}">
                <label for="exampleInputEmail1">PROFISSÃO</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItemBeneficiario.profissao" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsBeneficiario.errors.profissao">@{{formErrorsBeneficiario.errors.profissao}}</span>
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">LOCALIZAÇÃO</h2>
          </div>
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsBeneficiario.errors.pais}">
                 <label for="exampleInputEmail1">PAÍS</label>
                <select class="form-control select2" v-model="newItemBeneficiario.pais">
                  <option selected="selected" disabled="" value="">Selecione o País</option>
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>                          
                </select>
                <span class="help-block text-danger" v-if="formErrorsBeneficiario.errors.pais">@{{formErrorsBeneficiario.errors.pais}}</span>
              </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">DADOS DE CONTATO</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsBeneficiario.errors.telefone_de_localizacao}">
                <label for="exampleInputEmail1">TELEFONE </label>
                <input type="Text" class="form-control" v-model="newItemBeneficiario.telefone_de_localizacao" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsBeneficiario.errors.telefone_de_localizacao">@{{formErrorsBeneficiario.errors.telefone_de_localizacao}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsBeneficiario.errors.celular}">
                <label for="exampleInputEmail1">CELULAR</label>
                <input type="numer" class="form-control" v-model="newItemBeneficiario.celular" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsBeneficiario.errors.celular">@{{formErrorsBeneficiario.errors.celular}}</span>
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrorsBeneficiario.errors.email}">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="Email" class="form-control" v-model="newItemBeneficiario.email" id= "exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrorsBeneficiario.errors.email">@{{formErrorsBeneficiario.errors.email}}</span>
               </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div id="error" style="color: red; display: none">* RG/CEDULA já cadastrada! *</div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" v-on:click.prevent="cerrar_modal">Cancelar</button>
        <button type="button" id="guardar" class="btn btn-primary" v-on:click.prevent="createItemBeneficiario">SALVAR</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="agregar_cuenta" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">CADASTRO DE CONTA BANCÁRIA</h4>
        <h5 class="modal-title" id="exampleModalLabel" style="padding-top: 10px;">Beneficiário:   <b>@{{newItemCuenta.beneficario}}</b></h5>
        <span style="color: #721c24;background-color: #f8d7da;border-color: #f5c6cb;border: 1px solid transparent;border-radius: .25rem;padding: 17px;float: right;margin-top: -56px;" v-if="formErrorsCuenta.message">@{{formErrorsCuenta.message}}</span>
      </div>
      <div class="modal-body ">
        <form action="" v-on:submit="createItemCuenta">
          <div class="row">
            <div class="col-md-2" >
              <div class="form-group" v-bind:class="{'has-error':formErrorsCuenta.errors.banco}">
                <label for="exampleInputEmail1">BANCO</label>
                  <select class="form-control banco_select" v-selecttwo="newItemCuenta.banco" v-model="newItemCuenta.banco" :disabled="!bancos"  style="width: 100%">
                    <option selected="selected" value="" disabled>SELECIONE</option>
                    <template v-for="banco in bancos">
                      <option v-bind:value="banco.id">@{{banco.nombre}}</option>
                    </template>
                  </select>
                  <span class="help-block text-danger" v-if="formErrorsCuenta.errors.banco">@{{formErrorsCuenta.errors.banco}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCuenta.errors.tipo_da_conta}">
                 <label for="exampleInputEmail1">TIPO</label>
                <select v-model="newItemCuenta.tipo_da_conta" class="form-control" id="">
                  <option value="" disabled="">SELECIONE</option>
                  <option value="Corriente">Corriente</option>
                  <option value="Ahorro">Ahorro</option>
                </select>
                <span class="help-block text-danger" v-if="formErrorsCuenta.errors.tipo_da_conta">@{{formErrorsCuenta.errors.tipo_da_conta}}</span>
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrorsCuenta.errors.numero_da_conta}" >
               <label for="exampleInputEmail1">Nº DA CONTA</label>
                  <input type="text" maxlength="30" v-model="newItemCuenta.numero_da_conta" class="form-control" id="exampleInputEmail1" placeholder="Escreva aqui">
                  <span class="help-block text-danger" v-if="formErrorsCuenta.errors.numero_da_conta">@{{formErrorsCuenta.errors.numero_da_conta}}</span>
               </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" v-on:click.prevent="cerrar_modal">Cancelar</button>
        <button type="button" id="guardar" class="btn btn-primary" v-on:click.prevent="createItemCuenta">SALVAR</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="agregar" tabindex="'-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">REGISTRO DE SOLICITAÇÃO</h4>
        <h5 class="modal-title" id="exampleModalLabel" style="padding-top: 10px;">CÓDIGO DO PEDIDO: <b>@{{newItem.codigo}}</b></h5>
      </div>
      <div class="modal-body ">
        <form action="" v-on:submit="createItem">
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.prioridade}">
                <label for="exampleInputEmail1">PRIORIDADE </label>
                <select class="form-control select2" v-model="newItem.prioridade">
                      <option value="1" selected="">Normal</option>
                      <option value="2">Urgente</option>
                    </select>
                <span class="help-block text-danger" v-if="formErrors.errors.prioridade">@{{formErrors.errors.prioridade}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.tipo_pagamento}">
                <label for="exampleInputEmail1">TIPO DE PAGAMENTO </label>
                <select v-model="newItem.tipo_pagamento" class="form-control" id="">
                  <option value="" disabled="" selected="">SELECIONE</option>
                  <template v-for="pago in pagos">
                    <option value="@{{pago.id}}">@{{pago.descripcion}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.tipo_pagamento">@{{formErrors.errors.tipo_pagamento}}</span>
               </div>
            </div>

            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.usuario}">
                <label for="exampleInputEmail1">USUÁRIO </label>
                <select v-model="newItem.usuario" class="form-control" id="">
                  <template v-for="user in users">
                    <option value="@{{user.id}}">@{{user.nombre}} @{{user.apellido}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.usuario">@{{formErrors.errors.usuario}}</span>
               </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="exampleInputEmail1">OBSERVAÇÃO </label>
                <input type="text" class="form-control" v-model="newItem.descripcion" id="descripcion" name="descripcion" placeholder="Escreva aqui">
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">DADOS DO EMITENTE</h2>
          </div>
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cliente}">
                <label for="exampleInputEmail1">NOME</label>
                <select v-model="newItem.cliente" v-selecttwo="newItem.cliente" class="form-control cliente_select" id="select_cliente" style="width: 100%">
                    <option value="" selected="selected" disabled="">SELECIONE</option>
                    <template v-for="cliente in clientes">
                      <option style="" data-nombre="@{{cliente.nombre}} @{{cliente.apellido}}" data-id="@{{cliente.id}}"  data-cep="@{{cliente.cep}}" data-celular="@{{cliente.celular}}" value="" v-bind:value="cliente.id">@{{cliente.nombre}} @{{cliente.apellido}}</option>
                    </template>
                  </select>
                <span class="help-block text-danger" v-if="formErrors.errors.cliente">@{{formErrors.errors.cliente}}</span>
              </div>
              
            </div>
             <div class="col-md-1 nuevocliente">
              <i class="glyphicon glyphicon-plus" data-toggle="collapse" v-on:click.prevent="getCodeItemCliente" aria-expanded="false" aria-controls="cliente" style="padding-top: 30px;font-size: 25px ;cursor: pointer"></i>
            </div>
            <input type="hidden" id="hiddencep" value="">
            <div class="col-md-2 cliente" style="display: none;">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_cliente}">
                 <label for="exampleInputEmail1">DOC. IDENTIFICAÇÃO</label>
                <input type="text" class="form-control" v-model="newItem.cep_cliente" id="cep_cliente" name="cep_cliente" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.cep_cliente">@{{formErrors.errors.cep_cliente}}</span>
              </div>
            </div>
            <div class="col-md-3 cliente" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">NOME COMPLETO</label>
                <input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" placeholder="Escreva aqui" readonly>
              </div>
            </div>
            <div class="col-md-2 cliente" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">CELULAR</label>
                <input type="text" class="form-control" id="telefono_cliente" name="telefono_cliente" readonly placeholder="Escreva aqui">
              </div>
            </div>
            <div class="col-md-2 cliente2" style="display: none;">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_cliente}">
                 <label for="exampleInputEmail1">DOC. IDENTIFICAÇÃO</label>
                <input type="text" class="form-control" v-model="newItem.cep_cliente" id="cep_cliente2" name="cep_cliente" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.cep_cliente">@{{formErrors.errors.cep_cliente}}</span>
              </div>
            </div>
            <div class="col-md-3 cliente2" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">NOME COMPLETO</label>
                <input type="text" class="form-control" id="nombre_cliente2" name="nombre_cliente" placeholder="Escreva aqui" readonly>
              </div>
            </div>
            <div class="col-md-2 cliente2" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">CELULAR</label>
                <input type="text" class="form-control" id="telefono_cliente2" name="telefono_cliente" readonly placeholder="Escreva aqui">
              </div>
            </div>
            <div class="col-md-12 nuevocliente">  
              <div class="collapse" id="cliente">
                <div class="row">                       
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.nome_cliente}">
                      <label for="exampleInputEmail1">NOME</label>
                      <input type="text" class="form-control" v-model="newItem.nome_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.nome_cliente">@{{formErrors.errors.nome_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.sobrenome_cliente}">
                      <label for="exampleInputEmail1">SOBRENOME</label>
                      <input type="text" class="form-control" v-model="newItem.sobrenome_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.sobrenome_cliente">@{{formErrors.errors.sobrenome_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_cliente}">
                      <label for="exampleInputEmail1">DOC. IDENTIFICAÇÃO</label>
                      <input type="text" class="form-control" v-model="newItem.cep_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.cep_cliente">@{{formErrors.errors.cep_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.celular_cliente}">
                      <label for="exampleInputEmail1">CELULAR</label>
                      <input type="text" class="form-control" v-model="newItem.celular_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.celular_cliente">@{{formErrors.errors.celular_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.email_cliente}">
                      <label for="exampleInputEmail1">EMAIL</label>
                      <input type="text" class="form-control" v-model="newItem.email_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.email_cliente">@{{formErrors.errors.email_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.pais_cliente}">
                      <label for="exampleInputEmail1">PAÍS</label>
                      <select v-model="newItem.pais_cliente" id="" class="form-control">
                        <option value="" disabled="" selected="">SELECIONE</option>
                        <template v-for="pais in paises">
                          <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                        </template>
                      </select>
                      <span class="help-block text-danger" v-if="formErrors.errors.pais_cliente">@{{formErrors.errors.pais_cliente}}</span>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">DADOS DO BENEFICIÁRIO</h2>
          </div>
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.beneficiario}">
                <label for="exampleInputEmail1">NOME</label>
                 <select v-model="newItem.beneficiario" v-selecttwo="newItem.beneficiario" class="form-control beneficiario_select" id="select_beneficiario" style="width: 100%" v-on:change="getCuenta">
                    <option value="" selected="selected" disabled="">SELECIONE</option>
                    <template v-for="beneficiario in beneficiarios">
                      <option data-nombre="@{{beneficiario.nombre}} @{{beneficiario.apellido}}" data-id="@{{beneficiario.id}}"  data-cpf="@{{beneficiario.cpf}}" data-celular="@{{beneficiario.celular}}" value="" v-bind:value="beneficiario.id">@{{beneficiario.nombre}} @{{beneficiario.apellido}}</option>
                    </template>
                  </select>
                <span class="help-block text-danger" v-if="formErrors.errors.beneficiario">@{{formErrors.errors.beneficiario}}</span>
              </div>
            </div>
            <div class="col-md-1 nuevobeneficiario" style="width:3%;">
              <i class="glyphicon glyphicon-plus" data-toggle="collapse" v-on:click.prevent="getCodeItemBeneficiario" aria-expanded="false" aria-controls="beneficiario" style="padding-top: 30px;font-size: 25px ;cursor: pointer;"></i>
            </div>
            <input type="hidden" id="hiddencpf" value="">
            <div class="col-md-2 beneficiario" style="display: none;">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_beneficiario}">
                 <label for="exampleInputEmail1">DOC. IDENTIFICAÇÃO</label>
                <input type="numer" class="form-control" v-model="newItem.cep_beneficiario" id="cep_beneficiario" name="cep_beneficiario" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.cep_beneficiario">@{{formErrors.errors.cep_beneficiario}}</span>
              </div>
            </div>
            
            <div class="col-md-3 beneficiario" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">NOME COMPLETO</label>
                <input type="text" class="form-control" id="nombre_beneficiario" name="nombre_beneficiario" placeholder="Escreva aqui" readonly>
              </div>
            </div>
            <div class="col-md-2 beneficiario" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">CELULAR</label>
                <input type="text" class="form-control" id="telefono_beneficiario" name="telefono_beneficiario" readonly placeholder="Escreva aqui">
              </div>
            </div>
            <div class="col-md-2 beneficiario" style="display: none;">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cuenta}">
                <label for="exampleInputEmail1">CONTA BANCÁRIA</label>
                <select class="form-control" v-model="newItem.cuenta" v-selecttwo="newItem.cuenta" id="cuenta_bancaria" style="width: 100%">
                  <option selected="selected" value="">SELECIONE</option>
                  <template v-for="cuenta in cuentas">
                    <option value="cuenta.id" v-bind:value="cuenta.id" data-banco="@{{cuenta.banco.id}}">@{{cuenta.banco.nombre}} - @{{cuenta.cuenta}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.cuenta">@{{formErrors.errors.cuenta}}</span>
              </div> 
            </div> 
            <div class="col-md-2 beneficiario2" style="display: none;">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_beneficiario2}">
                 <label for="exampleInputEmail1">DOC. IDENTIFICAÇÃO</label>
                <input type="numer" class="form-control" v-model="newItem.cep_beneficiario" id="cep_beneficiario2" name="cep_beneficiario" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.cep_beneficiario">@{{formErrors.errors.cep_beneficiario}}</span>
              </div>
            </div>
            
            <div class="col-md-3 beneficiario2" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">NOME COMPLETO</label>
                <input type="text" class="form-control" id="nombre_beneficiario2" name="nombre_beneficiario" placeholder="Escreva aqui" readonly>
              </div>
            </div>
            <div class="col-md-2 beneficiario2" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">CELULAR</label>
                <input type="text" class="form-control" id="telefono_beneficiario2" name="telefono_beneficiario" readonly placeholder="Escreva aqui">
              </div>
            </div>
            <div class="col-md-2 beneficiario2" style="display: none;">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cuenta}">
                <label for="exampleInputEmail1">CONTA BANCÁRIA</label>
                <select class="form-control" v-model="newItem.cuenta" v-selecttwo="newItem.cuenta" id="cuenta_bancaria2" style="width: 100%">
                  <option selected="selected" value="">SELECIONE</option>
                  <template v-for="cuenta in cuentas">
                    <option value="cuenta.id" v-bind:value="cuenta.id" data-banco="@{{cuenta.banco}}">@{{cuenta.banco.nombre}} - @{{cuenta.cuenta}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.cuenta">@{{formErrors.errors.cuenta}}</span>
              </div> 
            </div> 
            <div class="col-md-1 nuevacuenta" style="width:3%;display:none;">
              <i class="glyphicon glyphicon-plus" data-toggle="collapse" v-on:click.prevent="openCuenta" aria-expanded="false" aria-controls="cuenta" style="padding-top: 30px;font-size: 25px ;cursor: pointer;"></i>
            </div>
            
            <div class="col-md-12 nuevobeneficiario">  
              <div class="collapse" id="beneficiario">
                <div class="row">                       
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.nome_beneficiario}">
                      <label for="exampleInputEmail1">NOME</label>
                      <input type="text" class="form-control" v-model="newItem.nome_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.nome_beneficiario">@{{formErrors.errors.nome_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.sobrenome_beneficiario}">
                      <label for="exampleInputEmail1">SOBRENOME</label>
                      <input type="text" class="form-control" v-model="newItem.sobrenome_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.sobrenome_beneficiario">@{{formErrors.errors.sobrenome_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_beneficiario}">
                      <label for="exampleInputEmail1">DOC. IDENTIFICAÇÃO</label>
                      <input type="text" class="form-control" v-model="newItem.cep_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.cep_beneficiario">@{{formErrors.errors.cep_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.celular_beneficiario}">
                      <label for="exampleInputEmail1">CELULAR</label>
                      <input type="text" class="form-control" v-model="newItem.celular_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.celular_beneficiario">@{{formErrors.errors.celular_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.email_beneficiario}">
                      <label for="exampleInputEmail1">EMAIL</label>
                      <input type="text" class="form-control" v-model="newItem.email_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.email_beneficiario">@{{formErrors.errors.email_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.pais_beneficiario}">
                      <label for="exampleInputEmail1">PAÍS</label>
                      <select v-model="newItem.pais_beneficiario" class="form-control" id="">
                        <option value="" disabled="" selected="">SELECIONE</option>
                        <template v-for="pais in paises">
                          <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                        </template>
                      </select>
                      <span class="help-block text-danger" v-if="formErrors.errors.pais_beneficiario">@{{formErrors.errors.pais_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.banco_beneficiario}">
                      <label for="exampleInputEmail1">BANCO</label>
                      <select v-model="newItem.banco_beneficiario" class="form-control select2" id="">
                        <option value="" disabled="" selected="">SELECIONE</option>
                        <template v-for="banco in bancos">
                          <option value="@{{banco.id}}">@{{banco.nombre}}</option>
                        </template>
                      </select>
                      <span class="help-block text-danger" v-if="formErrors.errors.banco_beneficiario">@{{formErrors.errors.banco_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.tipo_conta_beneficiario}">
                      <label for="exampleInputEmail1">TIPO DE CONTA</label>
                      <input type="text" class="form-control" v-model="newItem.tipo_conta_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.tipo_conta_beneficiario">@{{formErrors.errors.tipo_conta_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.conta_beneficiario}">
                      <label for="exampleInputEmail1">CONTA BANCÁRIA</label>
                      <input type="number" class="form-control" v-model="newItem.conta_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.conta_beneficiario">@{{formErrors.errors.conta_beneficiario}}</span>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">DINHEIRO</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.moneda}">
                <label for="exampleInputEmail1">MOEDA</label>
                <select v-model="newItem.moneda" class="form-control" id="moneda" name="moneda">
                  <option value="" disabled="" selected="">SELECIONE</option>
                  <template v-for="moneda in monedas">
                    <option data-tasa="@{{moneda.tasa}}" value="@{{moneda.id}}">@{{moneda.moneda}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.moneda">@{{formErrors.errors.moneda}}</span>
               </div>
            </div>
			<div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.quantidade}">
                <label for="exampleInputEmail1">R$ ENVIADO </label>
                <input type="number" class="form-control" v-model="newItem.quantidade" id="monto" name="monto" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.quantidade">@{{formErrors.errors.quantidade}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.tasa_cambio}">
                <label for="exampleInputEmail1">BSF TAXA </label>
                @if(Auth::user()->rol == "Administrador")
                <input type="text" class="form-control" v-model="newItem.tasa_cambio" id="tasa_cambio" name="tasa_cambio" placeholder="0">
                @else
                <input readonly type="text" class="form-control" v-model="newItem.tasa_cambio" id="tasa_cambio" name="tasa_cambio" placeholder="0">
                @endif
                <span class="help-block text-danger" v-if="formErrors.errors.tasa_cambio">@{{formErrors.errors.tasa_cambio}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.total_cambio}">
                <label for="exampleInputEmail1">BSF TOTAL </label>
                <input disabled type="text" class="form-control" v-model="newItem.total_cambio" id="total_cambio" name="total" placeholder="0">
                <span class="help-block text-danger" v-if="formErrors.errors.total_cambio">@{{formErrors.errors.total_cambio}}</span>
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">Transferências e Depósitos</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">Banco</label>
                <select class="form-control" v-model="newItem.banco_rpt" id="bancotransf">
                  <option value="" selected="">Seleccione</option>
                  <template v-for="banco in bancos">
                   <template v-if="banco.pais == 1">
                    <option value="@{{banco.id}}">@{{banco.nombre}}</option>
                     </template>
                  </template>
                </select>
              </div> 
            </div> 
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">Nro. Referência</label>
                <input type="text" class="form-control" v-model="newItem.nro_comprobante_rpt" id="comprobantetransf">
               </div>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click.prevent="createItem">Adicionar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="observacion" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Observações</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="observacoes">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="">Observações</label>
                <textarea v-model="observacion.value" class="form-control"></textarea>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="observacoes" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="reporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Observações</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="font-size: 22px">Observação</th>
              <th style="font-size: 22px">Usuário</th>
              <th style="font-size: 22px">Estado</th>
              <th style="font-size: 22px">Data</th>
              <th style="font-size: 22px">Hora</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in reports">
              <tr>
                <td><p> @{{item.descripcion}}</p></td>
                <td><p> @{{item.usuario.nombre}} @{{item.usuario.apellido}}</p></td>
                <td align="center">
                  <template v-if="item.status == 1">
                    <span class="label label-info">
                      Pendiente
                    </span>
                  </template>
                  <template v-if="item.status == 2">
                    <span class="label label-warning">
                      Em processo
                    </span>
                  </template>
                  <template v-if="item.status == 3">
                    <span class="label label-success">
                      Pago
                    </span>
                  </template>
                  <template v-if="item.status == 4">
                    <span class="label label-danger">
                      Rejeitado
                    </span>
                  </template>
                  <template v-if="item.status == 5">
                    <span class="label" style="background-color: #7F8C8D">
                      Reembolso
                    </span>
                  </template>
                  <template v-if="item.status == 6">
                    <span class="label" style="background-color: #D35400">
                      Revisão
                    </span>
                  </template>

                </td>
                <td><p> @{{item.fecha}}</p></td>
                <td><p> @{{item.hora}}</p></td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Pedido ARA.000@{{fillItem.id}} - @{{fillItem.fecha}} </h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
            <div class="box-header with-border">
              <h2 class="box-title" style="color:#3c8dbc">Remetente</h2>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nome</label>
                   <input type="text" value="@{{fillItem.nombre_cliente}} @{{fillItem.apellido_cliente}}" class="form-control" readonly style="border:none; background-color: white">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Identidade</label>
                   <input type="text" maxlength="20" v-model="fillItem.identidad_cliente" class="form-control" readonly style="border:none; background-color: white">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Telefone</label>
                   <input type="text" maxlength="20" v-model="fillItem.telefono_cliente" class="form-control" readonly style="border:none; background-color: white">
                </div>
              </div>
            </div>
            <div class="box-header with-border">
              <h2 class="box-title" style="color:#3c8dbc">Beneficiario</h2>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nome</label>
                   <input type="text" value="@{{fillItem.nombre_beneficiario}} @{{fillItem.apellido_beneficiario}}" class="form-control" readonly style="border:none; background-color: white">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group" readonly>
                  <label for="exampleInputEmail1">Identidade</label>
                   <input type="text" maxlength="20" v-model="fillItem.identidad_beneficiario" class="form-control" readonly  style="border:none; background-color: white">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Telefone</label>
                   <input type="text" maxlength="20" v-model="fillItem.telefono_beneficiario" class="form-control" readonly  style="border:none; background-color: white">
                </div>
              </div>
            </div>
            <div class="box-header with-border">
              <h2 class="box-title" style="color:#3c8dbc">Dados Bancarios</h2>
            </div>
            <div class="row">
              <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">CONTA BANCÁRIA</label>
                <select class="form-control select2" v-model="fillItem.cuenta" v-selecttwo="fillItem.cuenta" id="cuenta_b" style="width: 100%">
                  <option selected="selected" value="">SELECIONE</option>
                  <template v-for="cuenta in cuentas">
                    <option value="cuenta.id" v-bind:value="cuenta.id" data-banco="@{{cuenta.banco.id}}" data-cuenta="@{{cuenta.cuenta}}" data-tipocuenta="@{{cuenta.tipo_cuenta}}">@{{cuenta.banco.nombre}} - @{{cuenta.cuenta}}</option>
                  </template>
                </select>
              </div> 
            </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Banco de recebimento</label>
                  <select class="form-control select2" v-model="fillItem.banco" id="banco_b" disabled style="border:none; background-color: white">
                    <option selected="selected" disabled>Seleccione</option>
                    <template v-for="banco in bancos">
                      <option v-bind:value="banco.id">@{{banco.nombre}}</option>
                    </template>
                  </select>
                  <input type="hidden" id="id_banco">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">N. da Conta</label>
                   <input type="text" style="border:none; background-color: white" id="cuenta_ba" v-model="fillItem.nrocuenta" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tipo da conta</label>
                   <input type="text" style="border:none; background-color: white" id="tipo_b" v-model="fillItem.tipo_cuenta" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Banco emissor</label>
                  <select class="form-control select2" v-selecttwo="fillItem.banco_emisor" v-model="fillItem.banco_emisor" style="width: 100%">
                    <option value="">Seleccione</option>
                    <template v-for="banco in bancos">
                      <template v-if="banco.id == fillItem.banco_emisor">
                        <option selected v-bind:value="banco.id">@{{banco.nombre}}</option>
                      </template>
                      <template v-if="banco.pais == 2">
                      <option v-bind:value="banco.id">@{{banco.nombre}}</option>
                      </template>
                    </template>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nro. de referência (emissão)</label>
                   <input type="text" maxlength="20" v-model="fillItem.referencia" class="form-control">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Banco de pagamento (ARA)</label>
                  <select class="form-control select2" v-model="fillItem.banco_rpt" disabled style="border:none; background-color: white">
                    <option selected="selected" disabled>Seleccione</option>
                    <template v-for="banco in bancos">
                      <option v-bind:value="banco.id">@{{banco.nombre}}</option>
                    </template>
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nro. de referência</label>
                   <input type="text" maxlength="20" v-model="fillItem.nro_comprobante_rpt" class="form-control" readonly style="border:none; background-color: white">
                </div>
              </div>
            </div>
            <div class="box-header with-border">
              <h2 class="box-title" style="color:#3c8dbc">Valores</h2>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Moeda</label>
                  <input type="text" style="border:none; background-color: white"  v-model="fillItem.moneda" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Valor</label>
                   <input type="text"  value="@{{format(fillItem.monto,2)}}" v-model="fillItem.monto"  id="monto_e" class="form-control">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Taxa</label>
                  @if(Auth::user()->rol != "Administrador")
                   <input type="text" style="border:none; background-color: white" value="@{{format(fillItem.tasa,2)}}" v-model="fillItem.tasa" id="tasa_e" class="form-control" readonly>
                    @else
                   <input type="text" value="@{{format(fillItem.tasa,2)}}"  v-model="fillItem.tasa" id="tasa_e" class="form-control">
                   @endif
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Total</label>
                   <input type="text" style="border:none; background-color: white"  value="@{{format(fillItem.total,2)}}" v-model="fillItem.total" id="total_e" class="form-control" readonly>
                   <input type="hidden" id="total2">
                </div>
              </div>
            </div>
             <div class="box-header with-border">
              <h2 class="box-title" style="color:#3c8dbc">Status e Observação</h2>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Status</label>
                   <select class="form-control" v-model="fillItem.statusf" style="width:100% ">
                      <option v-bind:value="1" value="1">Pendientes</option>
                      <option v-bind:value="2" value="2">Em processo</option>
                      <option v-bind:value="3" value="3">Pago</option>
                      <option v-bind:value="4" value="4">Rejeitado</option>
                      <option v-bind:value="5" value="5">Reembolso</option>
                      <option v-bind:value="6" value="6">Revisão</option>
                    </select>
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <label for="exampleInputEmail1">Observação</label>
                   <input type="text" v-model="fillItem.observacion" class="form-control">
                </div>
              </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('js/moment.js')!!}
{!!Html::script('js/moment-with-locales.js')!!}
{!!Html::script('js/vue/solicitud.js?1.0')!!}
{!!Html::script('js/solicitudes.js?1.0')!!}
<script>
  $('.beneficiario_select').select2({
    theme: "bootstrap"
  });
  $('.cliente_select').select2({
    theme: "bootstrap"
  });
  $('.banco_select').select2({
    theme: "bootstrap"
  });
</script>
<script>

  $(document).ready(function(){

    $(".clientebuscar").change(function() {

      var idCliente = this.value;
      console.log(idCliente);

       setTimeout(
         function(){
           $( ".cliente" ).trigger( "click" );
         }, 1000);
    });
  $(".prioridadbuscar").change(function() {

       setTimeout(
         function(){
           $( ".prioridad" ).trigger( "click" );
         }, 1000);
    });
    $(".pagobuscar").change(function() {

       setTimeout(
         function(){
           $( ".pago" ).trigger( "click" );
         }, 1000);
    });
    $(".bancobuscar").change(function() {

      var idbanco = this.value;
      console.log(idbanco);

       setTimeout(
         function(){
           $( ".banco" ).trigger( "click" );
         }, 1000);
    });
    
    $(".statusfbuscar").change(function() {

      var idstatusf = this.value;
      console.log(idstatusf);

       setTimeout(
         function(){
           $( ".statusf" ).trigger( "click" );
         }, 1000);
    });
    $(".unidadbuscar").change(function() {

      var idunidad = this.value;
      console.log(idunidad);

       setTimeout(
         function(){
           $( ".unidad" ).trigger( "click" );
         }, 1000);
    });
    $(".usuariobuscar").change(function() {

      var idusuario = this.value;
      console.log(idusuario);

       setTimeout(
         function(){
           $( ".usuario" ).trigger( "click" );
         }, 1000);
    });




  });


</script>
@endsection

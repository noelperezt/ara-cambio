@section('title', 'Relatório de Movimentação')
@section('description', 'Relatório Geral do Dia')
@extends('layouts.backend.template2')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <div class="col-md-10">
            <h2 class="box-title">Movimentação - <strong>Desde a</strong> <div style="display: inline;" id="fechas1"><?php echo date('d/m/Y') ?></div><strong> Até</strong> <div style="display: inline;" id="fechas2"><?php echo date('d/m/Y') ?></div></h2>
          </div>
          <div class="col-md-2" style="text-align: right;">
            <a href="javascript:window.print(); void 0;"><i class="glyphicon glyphicon-print hidden-print" style="padding-top: 7px;font-size: 25px; width: 15%; padding-right:50px;"></i></a>

            <i class="glyphicon glyphicon-search hidden-print" data-toggle="collapse" href="#busqueda" aria-expanded="false" aria-controls="busqueda" style="padding-top: 7px;font-size: 25px ;cursor: pointer"></i>

          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body hidden-print">
          <div class="collapse" id="busqueda">
            <div class="row">                       
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Data Inicio</label>
                  <input type="date" class="form-control fechainicio" id="exampleInputEmail1" v-model="filter.data_de_pedido" v-on:change="filterItem" placeholder=" Escreva aqui" value="<?php echo date('Y-m-d') ?>">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Data Final</label>
                  <input type="date" class="form-control fechafin" id="exampleInputEmail1" v-model="filter.data_de_pedido2" placeholder="Escreva aqui" v-on:change="filterItem" value="<?php echo date('Y-m-d') ?>">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tipo de Pagamento</label>
                  <select class="form-control pagobuscar" v-model="filter.tipo_pagamento"  v-selecttwo="filter.tipo_pagamento" :disabled="!pagos" v-on:change="filterItem" style="width:100% ">
                        <option value="">Selecione</option>
                        <template v-for="pago in pagos">
                          <option v-bind:value="@{{pago.id}}" value="@{{pago.id}}">@{{pago.descripcion}}</option>
                        </template>
                      </select>
                   <input type="text" class="form-control pago" id="exampleInputEmail1" v-model="filter.tipo_pagamento" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1" style="margin-left: 5px">Usuários</label>
                  <select class="form-control usuariobuscar" v-model="filter.usuario"  v-selecttwo="filter.usuario" :disabled="!usuarios" v-on:change="filterItem" style="width:100% ">
                      <option value="">Selecione</option>
                      <template v-for="usuario in usuarios">
                        <option v-bind:value="@{{usuario.id}}" value="@{{usuario.id}}">@{{usuario.nombre}} @{{usuario.apellido}}</option>
                      </template>
                    </select>
                 <input type="text" class="form-control usuario" id="exampleInputEmail1" v-model="filter.usuario" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                </div> 
              </div> 
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Status</label>
                  <select class="form-control statusfbuscar" v-model="filter.statusf"  v-selecttwo="filter.statusf"  v-on:change="filterItem" style="width:100% ">
                    <option value="">Todos</option>
                    <option v-bind:value="1" value="1">Pedente</option>
                    <option v-bind:value="2" value="2">Em processo</option>
                    <option v-bind:value="3" value="3">Pago</option>
                    <option v-bind:value="4" value="4">Rejeitado</option>
                    <option v-bind:value="5" value="5">Reembolsado</option>
                    <option v-bind:value="6" value="6">Em Revisão</option>
                  </select>

                 <input type="text" class="form-control statusf" id="exampleInputEmail1" v-model="filter.statusf" placeholder="Escreva aqui" v-on:click="filterItem" style="display: none">
                </div> 
              </div> 
              </div>
            </div>
          </div>
      </div>
    </div>
  <!-- /.content -->
  </div>
  <div class="box" style="border:none; padding: 0px; margin: 0px;">
    <template v-for="item in items"> 
      <template v-if="item.solicitud!=''">
      <div class="col-md-6">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">@{{item.nombre}} @{{item.apellido}}</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus hidden-print"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table no-margin" style="font-size: 13px">
              <thead>
                <tr>
                  <th style="font-size: 13px">ID</th>
                  <th style="width: 7%;font-size: 13px">&nbsp;&nbsp;Data&nbsp;&nbsp;</th>
                  <th style="font-size: 13px">T Pagamento</th>
                  <th style="font-size: 13px"></th>
                  <th style="font-size: 13px"></th>
                </tr>
              </thead>
              <tbody>
                <template v-for="solicitud in item.solicitud">
                  <tr>
                    <td><p>ARA.000@{{solicitud.id}}</p></td>
                    <td><p>@{{dateFormat(solicitud.fecha)}}</p></td>
                    <td><p>@{{solicitud.tipo_pago.descripcion}}</p></td>
                    <td align="right"><p>BSF @{{format(solicitud.total,2)}}</p></td>
                    <td align="right"><p>BRL @{{format(solicitud.monto,2)}}</p></td>
                  </tr>
                </template>
              </tbody>
              <tfoot>
                <template v-for="subtotal in subtotales">
                  <template v-if="subtotal.usuario == item.id">
                    <tr style="font-weight: bold">
                      <td colspan="3" align="right">SUBTOTAL</td>
                      <td align="right">BSF @{{format(subtotal.total_bolivares,2)}}</td>
                      <td align="right">BRL @{{format(subtotal.monto_reales,2)}}</td>
                    </tr>
                  </template>
                </template>
                <tr>
                  <td colspan="5">&nbsp;</td>
                </tr>
                <template v-for="total in totales">
                  <template v-if="total.usuario == item.id">
                    <tr style="font-weight: bold">
                      <td colspan="4" align="right">@{{total.tipo_pago.descripcion}}</td>
                      <td align="right">BRL @{{format(total.monto_reales,2)}}</td>
                    </tr>
                  </template>
                </template>
                
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      </template>                    
    </template>
    <template v-for="item in items"> 
      <template v-if="item.solicitud==''">
      <div class="col-md-6">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">@{{item.nombre}} @{{item.apellido}}</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus hidden-print"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table no-margin" style="font-size: 13px">
              <thead>
                <tr>
                  <th style="font-size: 13px">ID</th>
                  <th style="width: 7%;font-size: 13px">&nbsp;&nbsp;Data&nbsp;&nbsp;</th>
                  <th style="font-size: 13px">Pagamento</th>
                  <th style="font-size: 13px"></th>
                  <th style="font-size: 13px"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <td><p>ARA.000</p></td>
                    <td><p>00/00/0000</p></td>
                    <td><p>--</p></td>
                    <td align="right"><p>BSF 0</p></td>
                    <td align="right"><p>BRL 0</p></td>
                  </tr>
              </tbody>
              <tfoot>
                <tr style="font-weight: bold">
                  <td colspan="3" align="right">SUBTOTAL</td>
                  <td align="right">BSF 0</td>
                  <td align="right">BRL 0</td>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      </template>                    
    </template>
  </div>
</section>
@endsection
@section('js')
{!!Html::script('js/moment.js')!!}
{!!Html::script('js/moment-with-locales.js')!!}
{!!Html::script('js/vue/estadocuenta.js?1.5')!!}
<script>
  $('.cliente_select').select2({
    theme: "bootstrap"
  });
</script>
<script>

  $(document).ready(function(){

    $(".pagobuscar").change(function() {

       setTimeout(
         function(){
           $( ".pago" ).trigger( "click" );
         }, 1000);
    });
    $(".statusfbuscar").change(function() {

      var idstatusf = this.value;
      console.log(idstatusf);

       setTimeout(
         function(){
           $( ".statusf" ).trigger( "click" );
         }, 1000);
    });
    $(".usuariobuscar").change(function() {

      var idusuario = this.value;
      console.log(idusuario);

       setTimeout(
         function(){
           $( ".usuario" ).trigger( "click" );
         }, 1000);
    });
    $(".fechainicio").change(function() {
       var fecha1 = this.value;
       var res = fecha1.split("-");
       document.getElementById("fechas1").innerHTML = res[2]+'/'+res[1]+'/'+res[0];
    });
    $(".fechafin").change(function() {
        var fecha2 = this.value;
       var res = fecha2.split("-");
       document.getElementById("fechas2").innerHTML = res[2]+'/'+res[1]+'/'+res[0];
    });

  });


</script>
@endsection

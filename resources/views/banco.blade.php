@if(Auth::user()->rol != "Administrador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@section('title', 'Cadastro de Bancos')
@section('description', 'Lista de bancos')
@extends('layouts.backend.template')
@section('content')
<section class="content" style="min-height: 100px">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Adicionar Bancos</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" v-on:submit.prevent="createItem">
          <div class="box-body">
            <div class="row"> 

            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.pais}">
                <label for="exampleInputEmail1">País</label>
                <select v-model="newItem.pais" class="form-control" id="">
                  <option value="" disabled="">Seleccione</option>
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.pais">@{{formErrors.errors.pais}}</span>
              </div>
            </div>
            
          <div class="col-md-3">
            <div class="form-group" v-bind:class="{'has-error':formErrors.errors.nome}">
              <label for="exampleInputEmail1">Nome do Banco</label>
              <input type="text" v-model="newItem.nome" class="form-control" id="exampleInputEmail1" placeholder="Nome do Banco">
              <span class="help-block text-danger" v-if="formErrors.errors.nome">@{{formErrors.errors.nome}}</span>
            </div>
          </div>
            
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.codigo}" >
                <label for="exampleInputEmail1">Código do Banco</label>
                <input type="text" v-model="newItem.codigo" class="form-control" id="exampleInputEmail1" placeholder="Código do Banco">
                <span class="help-block text-danger" v-if="formErrors.errors.codigo">@{{formErrors.errors.codigo}}</span>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-2">
                <a href="#" v-on:click="createItem"><i class="  glyphicon glyphicon-plus"; style="padding-top: 30px;font-size: 25px" ></i></a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

</section>

    <section class="col-lg-12 connectedSortable">
      <div class="box box-info">
      <div class="box-header with-border">
      <div class="col-md-10">
        <h2 class="box-title">Lista de Bancos Cadastrados</h2>
      </div>
      <div class="col-md-2">
      <i class="glyphicon glyphicon-search" data-toggle="collapse" href="#busqueda" aria-expanded="false" aria-controls="busqueda" style="padding-top: 7px;font-size: 25px ;cursor: pointer"></i>
       </div>
    </div>
  <div class="box-body">
  <div class="collapse" id="busqueda">
            <div class="row">                       
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">País</label>
                  <select v-model="filter.pais" class="form-control" id="" v-on:change="filterItem">
                  <option value="" disabled="">Selecione</option>
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>
                </select>
                </div>
               </div>
             </div>
          </div>
    <div class="table-responsive">
      <table class="table no-margin">
        <thead>
          <tr>

            <th style="width: 20%;font-size: 22px">Descrição</th>
            <th style="width: 15%;font-size: 22px">País</th>
            <th style="width: 15%;font-size: 22px">Código</th>
            <th style="width: 15%;font-size: 22px">Status</th>
            <th style="width: 20%;font-size: 22px">Opções</th>
          </tr>
        </thead>
        <tbody>
          <template v-for="item in items">
            <tr>
              <td><p>@{{item.nombre}}</p></td>
              <td><p>@{{item.pais.descripcion}}</p></td>
              <td><p>@{{item.codigo}}</p></td>
              <td>
                  <template v-if="item.status == 1">
                    <span class="label label-success">
                      Ativo
                    </span>
                  </template>
				  <template v-if="item.status == 0">
                    <span class="label label-danger">
                      Inativo
                    </span>
                  </template>
              </td>
              <td>
                <div class="icon">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-md-4">
                        <a href="#" class="text-warning" v-on:click.prevent="editItem(item.id)"><i class="glyphicon glyphicon-pencil"; style="width: 20%;font-size: 20px"></i></a>
                      </div>
                      
                      <template v-if="item.status == 1">
                        <div class="col-md-4">
                          <a href="#" style="font-size: 1.5em" class="text-danger" v-on:click.prevent="destroyItem(item.id)"><i class="glyphicon glyphicon-trash"></i></a>
                        </div>
                      </template>
                      <template v-if="item.status == 0">
                        <div class="col-md-4">
                          <a href="#" style="font-size: 1.5em" class="text-success" v-on:click.prevent="destroyItem(item.id)"><i class="glyphicon glyphicon-check"></i></a>
                        </div>
                      </template>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          </template>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <div class="box-footer">
      <div class="row">
        <div class="col-sm-7 col-md-offset-5">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            <ul class="pagination pull-right">
              <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                  <span aria-hidden="true">«</span>
                </a>
              </li>
              <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#" @click.prevent="changePage(page)">
                  @{{ page }}
                </a>
              </li>
              <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                  <span aria-hidden="true">»</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
</div>
</section>
@endsection
@section('elements')
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar.</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Nome do banco</label>
                <input type="text" v-model="fillItem.nome" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Código</label>
                <input type="text" v-model="fillItem.codigo" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">País</label>
                <select v-model="fillItem.pais" class="form-control">
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deposito" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Depósito na caixa</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="deposite">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="">Montante a depositar</label>
                <input type="number" v-model="deposito.value" class="form-control">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="deposite" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('js/vue/banco.js')!!}
@endsection
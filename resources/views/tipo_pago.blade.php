@if(Auth::user()->rol != "Administrador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@extends('layouts.backend.template')
@section('title','Tipo de pagamento')
@section('description', '')
@section('content')
<section class="content" style="min-height: 100px">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
            CADASTRAR TIPOS DE PAGAMENTO
          </h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" v-on:submit.prevent="createItem">
          <div class="box-body">
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.descricao}">
                <label for="exampleInputEmail1">
                  DESCRIÇÃO
                </label>
                <input class="form-control" v-model="newItem.descricao" id="exampleInputEmail1" placeholder="Pagamento" type="text">
                <span class="help-block text-danger" v-if="formErrors.errors.descricao">@{{formErrors.errors.descricao}}</span>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3">
                <a href="#" v-on:click="createItem">
                  <i ;="" class=" glyphicon glyphicon-plus" style="padding-top: 30px;font-size: 25px">
                  </i>
                </a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
      <h2 class="box-title">
        Lista de Pagamentos Cadastrados
      </h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="width: 50%;font-size: 22px">
                DESCRIÇÃO
              </th>
              <th style="width: 25%;font-size: 22px">
                STATUS
              </th>
              <th style="width: 25%;font-size: 22px">
                OPÇÕES
              </th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in items">
              <tr>
                <td>
                  <a href="pages/examples/invoice.html">
                    @{{item.descripcion}}
                  </a>
                </td>
                <td>
                  <template v-if="item.status == 1">
                    <span class="label label-success">
                      Ativo
                    </span>
                  </template>
                  <template v-if="item.status == 0">
                    <span class="label label-danger">
                      Inativo
                    </span>
                  </template>
                </td>
                <td>
                  <div class="icon">
                    <a href="#" class="text-warning" v-on:click.prevent="editItem(item.id)">
                      <i ;="" class="glyphicon glyphicon-pencil" style="width: 20%;font-size: 20px">
                      </i>
                    </a>
                  </div>
                </td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <div class="box-footer">
      <div class="row">
        <div class="col-sm-7 col-md-offset-5">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            <ul class="pagination pull-right">
              <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                  <span aria-hidden="true">«</span>
                </a>
              </li>
              <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#" @click.prevent="changePage(page)">
                  @{{ page }}
                </a>
              </li>
              <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                  <span aria-hidden="true">»</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('elements')
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ALTERAR TIPO DE PAGAMENTO</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label for="">DESCRIÇÃO</label>
                <input type="text" v-model="fillItem.descricao" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">STATUS</label>
                <select v-model="fillItem.status" class="form-control">
                  <option value="1">Ativo</option>
                  <option value="0">Inativo</option>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Salvar </button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('js/vue/tipo_pago.js')!!}
@endsection
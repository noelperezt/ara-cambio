@if(Auth::user()->rol == "Operador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@extends('layouts.backend.template')
@section('title', 'CAIXA')
@section('content')
@if(Auth::user()->rol == "Administrador")
<section class="content" style="min-height: 100px">
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <div class="row">
            <div class="col-md-10">
              <h3 class="box-title">ADCIONAR SALDO DE CAIXA</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
              <form role="form" v-on:submit.prevent="createItem">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="exampleInputEmail1">PAÍS</label>
                    <select class="form-control select2" v-model="newItem.pais" v-on:change="getBanco">
                      <option selected="selected" value="">SELECIONE</option>
                      <template v-for="pais in paises">
                        <option value="" v-bind:value="pais.id">@{{pais.descripcion}} </option>
                      </template>
                    </select>
                  </div> 
                </div> 
                <div class="col-md-3">
                  <div class="form-group" v-bind:class="{'has-error':formErrors.errors.banco}">
                    <label for="exampleInputEmail1">BANCO</label>
                    <select class="form-control select2" v-model="newItem.banco" v-bind:disabled="bancos.length == 0">
                      <option selected="selected" disabled="" value="">SELECIONE</option>
                      <template v-for="banco in bancos">
                        <option value="" v-bind:value="banco.id">@{{banco.codigo}} - @{{banco.nombre}}</option>
                      </template>
                    </select>
                    <span class="help-block text-danger" v-if="formErrors.errors.banco">@{{formErrors.errors.banco}}</span>
                  </div> 
                </div> 
                <div class="col-md-3">
                  <div class="form-group" v-bind:class="{'has-error':formErrors.errors.montante}">
                    <label for="exampleInputEmail1">VALOR</label>
                    <input type="text" class="form-control" id="number1" placeholder="Escreva aqui" v-model="newItem.montante">
                    <span class="help-block text-danger" v-if="formErrors.errors.montante">@{{formErrors.errors.montante}}</span>
                  </div>
                </div>
                <div class="col-md-3">
                  <br>
                  <a href="#" class="text-primary" v-on:click.prevent="createItem"><i class="glyphicon glyphicon-plus"; style="padding-top: 10px;font-size: 25px;"></i></a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endif
<section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
    <div class="col-md-10">
      <h2 class="box-title">Movimento de Caixa</h2>
    </div>
 <div class="col-md-2">
     <i class="glyphicon glyphicon-search" data-toggle="collapse" href="#busqueda" aria-expanded="false" aria-controls="busqueda" style="padding-top: 7px;font-size: 25px ;cursor: pointer"></i>
  </div>
</div>

    <!-- /.box-header -->
    <div class="box-body">

     <div class="collapse" id="busqueda">
            <div class="row">                       
              <div class="col-md-2">
                <div class="form-group">
                  <label  for="exampleInputEmail1">Data Inicial</label>
                  <input type="date" class="form-control" id="exampleInputEmail1" v-model="filter.data" placeholder="Escreva aqui" v-on:change="filterItem">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Data Final</label>
                  <input type="date" class="form-control" id="exampleInputEmail1" v-model="filter.data2" placeholder="Escreva aqui" v-on:change="filterItem">
                </div>
              </div>
          </div>
        </div>
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="width: 8%;font-size: 22px">ID</th>
              <th style="width: 20%;font-size: 22px">BANCO</th>
              <th style="width: 19%;font-size: 22px" class="text-center">DATA</th>
              <th style="width: 25%;font-size: 22px" class="text-center">SALDO</th>
              <th style="width: 25%;font-size: 22px" class="text-center">OPÇÕES</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in items">
              <tr>
                <td><p> @{{item.id}}</p></td>
                <td><p> @{{item.banco.nombre}}</p></td>
                <td><p style="text-align: center;"> @{{dateFormat(item.fecha)}}</p></td>
                <td><p class="text-right" style="text-align: center;"> @{{format(item.monto,2)}}</p></td>
                <td class="text-center">
                  <a href="#" style="font-size: 1.5em" class="text-info"  v-on:click.prevent="reporte(item.historico)" title="Observações"><span class="glyphicon glyphicon-list-alt"></span></a>

                  <a href="#" style="font-size: 1.5em; margin-left: 10px"   class="text-info" v-on:click.prevent="depositar(item.id)" title="Depósito"><span class="glyphicon glyphicon-piggy-bank"></span></a>

                  <a href="#" class="text-warning" v-on:click.prevent="editItem(item.id)"><i class="glyphicon glyphicon-pencil" style="width: 20%;font-size: 20px"></i></a>
                 </td>
              </tr>
            </template>
            <tr>
              <td colspan="6">
                <div class="row">
                  <div class="col-sm-7 col-md-offset-5">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                      <ul class="pagination pull-right">
                        <li v-if="pagination.current_page > 1">
                          <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                            <span aria-hidden="true">«</span>
                          </a>
                        </li>
                        <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                          <a href="#" @click.prevent="changePage(page)">
                            @{{ page }}
                          </a>
                        </li>
                        <li v-if="pagination.current_page < pagination.last_page">
                          <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                            <span aria-hidden="true">»</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
            <tr align="right">
              <td colspan="3" align="right" style="font-size: 12px; font-weight:bold">TOTAL DE DEPÓSITO</td>
              <td style="color: green"><p class="text-right">@{{format(cajas,2)}}</p></td>
            </tr>
            <tr>
              <td colspan="3" align="right" style="font-size: 12px; font-weight:bold">TOTAL DE SAÍDAS</td>
              <td  style="color: red"><p class="text-right">@{{format(solicitudes,2)}}</p></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3" align="right" style="font-size: 12px; font-weight:bold">SALDO DISPONÍVEL</td>
              <td><p class="text-right">@{{format(cajas-solicitudes,2)}}</p></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
  </div> 
</section>
@endsection
@section('elements')
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar País.</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Nome do país</label>
                <input type="text" v-model="fillItem.nome" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Estado</label>
                <select v-model="fillItem.status" class="form-control">
                  <option value="1">Ativo</option>
                  <option value="0">Inativo</option>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deposito" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Depósito na caixa</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="deposite">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="">Montante a depositar</label>
                <input type="text" v-model="deposito.value" id="number2" class="form-control">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="deposite" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ALTERAR DEPÓSITO</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="">VALOR</label>
                <input type="text" v-model="deposito.value" class="form-control" id="number">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem" class="btn btn-primary">Salvar </button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="reporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Histórico de depósitos</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="font-size: 22px">BANCO</th>
              <th style="font-size: 22px">USUÁRIO</th>
              <th style="font-size: 22px">VALOR</th>
              <th style="font-size: 22px">DATA</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in reports">
              <tr>
                <td><p> @{{item.caja.banco.nombre}}</p></td>
                <td><p> @{{item.usuario.nombre}} @{{item.usuario.apellido}}</p></td>
                <td><p> @{{format(item.monto,2)}}</p></td>
                <td><p> @{{item.created_at}}</p></td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>-->
        <button type="button" class="btn btn-primary" data-dismiss="modal">SAIR</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('js/moment.js')!!}
{!!Html::script('js/moment-with-locales.js')!!}
{!!Html::script('js/vue/caja.js')!!}
<script>
  $("#number").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value ) {
            return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
        });
    }
});
  $("#number1").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value ) {
            return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
        });
    },
    "change": function (event) {
        $(event.target).val(function (index, value ) {
            return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
        });
    }
});
  $("#number2").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value ) {
            return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
        });
    }
});
</script>
@endsection
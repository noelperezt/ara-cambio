<!DOCTYPE doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>
      Ara Cambio
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link href="{!! asset('bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{!! asset('font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="{!! asset('Ionicons/css/ionicons.min.css') !!}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{!! asset('dist/css/AdminLTE.min.css') !!}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{!! asset('plugins/iCheck/square/blue.css') !!}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet">

  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="../../index2.html">
          <b>
            ARA
          </b>
          Câmbio
        </a>
      </div>
      <!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">
          Registre uma nova associação
        </p>
        {!!Form::open(['autocomplete'=>'off'])!!}
          <div class="form-group has-feedback">
            {!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>'Nome completo'])!!}
            <span class="glyphicon glyphicon-user form-control-feedback">
            </span>
          </div>
          <div class="form-group has-feedback">
            {!!Form::email('email', null,['class'=>'form-control', 'placeholder'=>'Email'])!!}
            <span class="glyphicon glyphicon-envelope form-control-feedback">
            </span>
          </div>
          <div class="form-group has-feedback">
            {!!Form::password('password', ['class'=>'form-control', 'placeholder'=>'Senha'])!!}
              <span class="glyphicon glyphicon-lock form-control-feedback">
              </span>
            </input>
          </div>
          <div class="form-group has-feedback">
            {!!Form::password('cpassword', ['class'=>'form-control', 'placeholder'=>'Redigite a senha'])!!}
              <span class="glyphicon glyphicon-log-in form-control-feedback">
              </span>
            </input>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox">
                    Lembre de mim <a href="#">terms</a>
                  </input>
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
              <button class="btn btn-primary btn-block btn-flat" type="submit">
                Register
              </button>
            </div>
            <!-- /.col -->
          </div>
        {!!Form::close()!!}
        <div class="social-auth-links text-center">
          <p style="height: 20px">
          </p>
          <a class="btn btn-block btn-social btn-google btn-flat" href="#">
            <i class="fa fa-sign-in">
            </i>
            Eu já tenho uma associação
          </a>
        </div>
        <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
      <!-- jQuery 3 -->
      <script src="{!! asset('jquery/dist/jquery.min.js') !!}">
      </script>
      <!-- Bootstrap 3.3.7 -->
      <script src="{!! asset('dist/js/bootstrap.min.js') !!}">
      </script>
      <!-- iCheck -->
      <script src="{!! asset('plugins/iCheck/icheck.min.js') !!}">
      </script>
      <script>
        $(function () {
          $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
          });
        });
      </script>
    </div>
  </body>
</html>
@if(Auth::user()->rol != "Administrador" && Auth::user()->rol != "Operador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@section('title', 'Beneficiario')
@section('description', 'Lista do Beneficiario')
@extends('layouts.backend.template2')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <div class="col-md-10">
            <h2 class="box-title">Lista de Beneficiarios</h2>
          </div>
          <div class="col-md-2" style="text-align: right;">
            <a href="#"><i class="glyphicon glyphicon-plus" v-on:click.prevent="getCodeItem" style="padding-top: 7px;font-size: 25px; width: 15%; padding-right:50px;"></i></a>
            <i class="glyphicon glyphicon-search" data-toggle="collapse" href="#busqueda" aria-expanded="false" aria-controls="busqueda" style="padding-top: 7px;font-size: 25px ;cursor: pointer"></i>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="collapse" id="busqueda">
            <div class="row">                       
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">DOC. IDENTIFICAÇÃO</label>
                  <input type="number" class="form-control" id="exampleInputEmail1" v-model="filter.cpf" placeholder="Escreva aqui" v-on:keyUp="filterItem">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">NOME</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" v-model="filter.nome_completo" placeholder="Escreva aqui" v-on:keyUp="filterItem">
                </div>
              </div>
              <div class="row"> 
                <!-- <div class="col-md-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">PAÍS</label>
                    <select class="form-control select2" v-model="filter.pais" v-on:change="filterItem">
                      <option value="" selected="">Todos</option>
                      <template v-for="pais in paises">
                        <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                      </template>
                    </select>
                  </div> 
                </div>  -->
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">FILTRAR</label>
                    <select class="form-control select2" v-model="filter.status" v-on:change="filterItem">
                      <option value="">Todos</option>
                      <option value="1" selected="">Ativos</option>
                      <option value="0">Inativos</option>
                    </select>
                  </div> 
                </div> 
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
              <tr>
                <th style="width: 15%;font-size: 16px">NOME</th>
                <th style="width: 20%;font-size: 16px">DOC. IDENTIFICAÇÃO</th>
                <th style="width: 10%;font-size: 16px">PAÍS</th>
                <th style="width: 10%;font-size: 16px">CELULAR</th>
                <th style="width: 15%;font-size: 16px ;text-align: center;">Email</th>
                <th style="width: 15%;font-size: 16px">STATUS</th>
                <th style="width: 15%;font-size: 16px">OPÇÕES</th>
              </tr>
              </thead>
              <tbody>
                <template v-for="item in items">
                  <tr>
                    <td><p> @{{item.nombre}} @{{item.apellido}}</p></td>
                    <td><p style="text-align: center;">@{{item.cpf}}</p></td>
                    <td><p>@{{item.pais.descripcion}}</p></td>
                    <td><p>@{{item.celular}}</p></td>
                    <td><p> @{{item.email}}</p></td>
                    <td>
                      <template v-if="item.status == 1">
                        <span class="label label-success">
                          Ativo
                        </span>
                      </template>
                      <template v-if="item.status == 0">
                        <span class="label label-danger">
                          Inativo
                        </span>
                      </template>
                    </td>
                    <td class="ico_estado">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-6">
                            <a href="#" style="font-size: 1.5em" class="text-warning" v-on:click.prevent="editItem(item.id)"><i class="glyphicon glyphicon-pencil"></i></a>
                          </div>
                          <template v-if="item.status == 1">
                            <div class="col-md-6">
                              <a href="#" style="font-size: 1.5em" class="text-danger" v-on:click.prevent="destroyItem(item.id)"><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                          </template>
                          <template v-if="item.status == 0">
                            <div class="col-md-6">
                              <a href="#" style="font-size: 1.5em" class="text-success" v-on:click.prevent="destroyItem(item.id)"><i class="glyphicon glyphicon-check"></i></a>
                            </div>
                          </template>
                        </div>
                      </div>
                    </td>
                  </tr>
                </template>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-7 col-md-offset-5">
              <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                <ul class="pagination pull-right">
                  <li v-if="pagination.current_page > 1">
                    <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                      <span aria-hidden="true">«</span>
                    </a>
                  </li>
                  <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                    <a href="#" @click.prevent="changePage(page)">
                      @{{ page }}
                    </a>
                  </li>
                  <li v-if="pagination.current_page < pagination.last_page">
                    <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                      <span aria-hidden="true">»</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- /.content -->
  </div>
</section>
@endsection
@section('elements')
<!-- Modal -->
<div class="modal fade" id="agregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">CADASTRO DE BENEFICIÁRIO</h4>
        <h5 class="modal-title" id="exampleModalLabel" style="padding-top: 10px;">Código do Beneficiário:   <b>@{{newItem.codigo}}</b></h5>
        <span style="color: #721c24;background-color: #f8d7da;border-color: #f5c6cb;border: 1px solid transparent;border-radius: .25rem;padding: 17px;float: right;margin-top: -56px;" v-if="formErrors.message">@{{formErrors.message}}</span>
      </div>
      <div class="modal-body ">
        <form action="" v-on:submit="createItem">
          <div class="row"> 
            <div class="col-md-2" >
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.nome}">
                <label for="exampleInputEmail1">NOME</label>
                <input type="text" class="form-control" v-model="newItem.nome" id="exampleInputEmail1" placeholder="NOME DO BENEFICIÁRIO">
                <span class="help-block text-danger" v-if="formErrors.errors.nome">@{{formErrors.errors.nome}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.ultimo_nome}">
                <label for="exampleInputEmail1">SOBRENOME </label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItem.ultimo_nome" placeholder="SOBRENOME DO BENEFICIÁRIO">
                <span class="help-block text-danger" v-if="formErrors.errors.ultimo_nome">@{{formErrors.errors.ultimo_nome}}</span>
               </div>
            </div>
            <div class="col-md-3">
             <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cpf}">
                <label for="">DOC. IDENTIFICAÇÃO</label>
                <div class="input-group">
                  <select v-model="newItem.tipo" class="form-control prefijo" id="" style="width: 60px">
                            <option value="V">V - Venezuelano</option>
                            <option value="E">E - Estrageiro</option>
                            <option value="J">J - Juridico</option>
                            <option value="G">G - Governo</option>
                            <option value="P">P - Passaporte</option>
                          </select>
                  <span class="input-group-addon" style="width: 5px">-</span>
                  <input type="text" class="form-control identifacion" id="exampleInputEmail1" v-model="newItem.cpf" placeholder="INFORME O DOCUMENTO">
                </div>
              </div>
              <span class="help-block text-danger" v-if="formErrors.errors.cpf">@{{formErrors.errors.cpf}}</span>
             </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.ultimo_nome}" >
                <label for="exampleInputEmail1"> DATA DE NASCIMENTO</label>
                <input type="date" class="form-control" id="exampleInputEmail1" v-model="newItem.data_de_nascimento" placeholder=" Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.ultimo_nome">@{{formErrors.errors.ultimo_nome}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.profissao}">
                <label for="exampleInputEmail1">PROFISSÃO</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItem.profissao" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.profissao">@{{formErrors.errors.profissao}}</span>
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">LOCALIZAÇÃO</h2>
          </div>
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.pais}">
                 <label for="exampleInputEmail1">PAÍS</label>
                <select class="form-control select2" v-model="newItem.pais">
                  <option selected="selected" disabled="" value="">Selecione o País</option>
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>                          
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.pais">@{{formErrors.errors.pais}}</span>
              </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">DADOS DE CONTATO</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.telefone_de_localizacao}">
                <label for="exampleInputEmail1">TELEFONE </label>
                <input type="Text" class="form-control" v-model="newItem.telefone_de_localizacao" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.telefone_de_localizacao">@{{formErrors.errors.telefone_de_localizacao}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.celular}">
                <label for="exampleInputEmail1">CELULAR</label>
                <input type="numer" class="form-control" v-model="newItem.celular" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.celular">@{{formErrors.errors.celular}}</span>
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.email}">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="Email" class="form-control" v-model="newItem.email" id= "exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.email">@{{formErrors.errors.email}}</span>
               </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div id="error" style="color: red; display: none">* RG/CEDULA já cadastrada! *</div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="guardar" class="btn btn-primary" v-on:click.prevent="createItem">SALVAR</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal editar-->
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">ALTERAÇÃO DE CADASTRO</h4>
        <h5 class="modal-title" id="exampleModalLabel" style="padding-top: 10px;">CÓDIGO DO BENEFICIÁRIO: <b>@{{fillItem.codigo}}</b></h5>
      </div>
      <div class="modal-body ">
        <form action="" v-on:submit="updateItem(fillItem.id)">
          <div class="row"> 
            <div class="col-md-3">
              <div class="form-group">
                <label for="exampleInputEmail1">NOME </label>
                <input type="text" class="form-control" v-model="fillItem.nome" id="exampleInputEmail1" placeholder="Escreva aqui">
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="exampleInputEmail1">SOBRENOME </label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="fillItem.ultimo_nome" placeholder="Escreva aqui">
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">DOC IDENTIFICAÇÃO</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="fillItem.cpf" placeholder="Escreva aqui ">
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1"> DATA DE NASCIMENTO</label>
                <input type="date" class="form-control" id="exampleInputEmail1" v-model="fillItem.data_de_nascimento" placeholder=" Escreva aqui">
               </div>
            </div>  
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">PROFISSÃO</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="fillItem.profissao" placeholder="Escreva aqui">
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">LOCALIZAÇÃO</h2>
          </div>
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group">
                 <label for="exampleInputEmail1">PAÍS</label>
                <select class="form-control select2" v-model="fillItem.pais">

                  <option selected="selected" disabled="">SELECIONE O PÁIS</option>
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>                          
                </select>
              </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">DADOS DE CONTATO</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">TELEFONE </label>
                <input type="Text" class="form-control" v-model="fillItem.telefone_de_localizacao" id="exampleInputEmail1" placeholder="Escreva aqui">
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">CELULAR</label>
                <input type="numer" class="form-control" v-model="fillItem.celular" id="exampleInputEmail1" placeholder="Escreva aqui">
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="exampleInputEmail1">EMAIL</label>
                <input type="Email" class="form-control" v-model="fillItem.email" id="exampleInputEmail1" placeholder="Escreva aqui">
               </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click.prevent="updateItem(fillItem.id)">Salvar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('js/vue/beneficiario.js')!!}
<script type="text/javascript">
  $(document).ready(function(){
    
    $(".identifacion" ).keyup(function() {
      var prefijo = $( ".prefijo" ).val();
      var idBeneficiario = prefijo+''+this.value;
      //console.log(idBeneficiario);
      
      $.get("../beneficiario/getcedula/"+idBeneficiario+"", function(response, state){
        if(response!=""){
          $("#error").css( "display", "block" );
          $("#guardar").prop('disabled', true);
        }else{
          $("#error").css( "display", "none" );
          $("#guardar").prop('disabled', false);
        }
      });

    });
     $(".prefijo" ).change(function() {
      var numced = $( ".identifacion" ).val();
      var idBeneficiario = this.value+''+numced;
      //console.log(idBeneficiario);
      
      $.get("../beneficiario/getcedula/"+idBeneficiario+"", function(response, state){
        if(response!=""){
          $("#error").css( "display", "block" );
          $("#guardar").prop('disabled', true);
        }else{
          $("#error").css( "display", "none" );
          $("#guardar").prop('disabled', false);
        }
      });

    });


});
</script>
@endsection

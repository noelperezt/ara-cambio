@section('title', 'Usuário')
@section('description', 'Alterar minha senha')
@extends('layouts.backend.template')
@section('content')

@if (Session::has('message'))
 <div class="text-danger">
 {{Session::get('message')}}
 </div>
@endif

<section class="content" style="min-height: 100px;max-width: 50%;">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title" style="margin-top: 5px">Dados do usuário</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <form method="post" action="{{url('user/updatepassword')}}">
                {{csrf_field()}}
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="mypassword">Nome</label>
                      <input type="text"  class="form-control" value="{{Auth::user()->nombre}} {{Auth::user()->apellido}}" placeholder="Escreva aqui" readonly style="background-color: white; border: none">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="mypassword">Usuário</label>
                      <input type="text"  class="form-control" value="{{Auth::user()->username}}" placeholder="Escreva aqui" readonly style="background-color: white; border: none">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="mypassword">Rol</label>
                      <input type="text"  class="form-control" value="{{Auth::user()->rol}}" placeholder="Escreva aqui" readonly style="background-color: white; border: none">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="mypassword">Digite sua senha atual:</label>
                      <input type="password" name="mypassword" class="form-control" placeholder="Escreva aqui">
                      <div class="text-danger">{{$errors->first('mypassword')}}</div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group" :class="{'has-error': formErrors.errors.banco}">
                      <label for="password">Digite sua nova senha:</label>
                      <input type="password" name="password" class="form-control" placeholder="Escreva aqui">
                      <div class="text-danger">{{$errors->first('password')}}</div>
                    </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group" v-bind:class="{'has-error':formErrors.errors.tipo_da_conta}">
                       <label for="mypassword">Confirme sua nova senha:</label>
                       <input style="padding-top: 10px"  type="password" name="password_confirmation" class="form-control" placeholder="Escreva aqui">
                     </div>
                   </div>
                   <div class="col-md-12">
                    <hr style="width: 100%; padding: 5px">
                   </div>
                   <div class="col-md-12">
                     <div class="pull-right" v-bind:class="{'has-error':formErrors.errors.tipo_da_conta}">
                       <button  type="submit" class="btn btn-primary">Alterar minha senha</button>
                     </div>
                   </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
@stop

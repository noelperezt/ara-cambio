@extends('layouts.backend.template')
@section('title', 'Historicos de Alteração de Taxas')
@section('description', '')
@section('content')
<section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
      <h2 class="box-title">Histórico do taxas</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="width: 20%;font-size: 12px">PAÍS</th>
              <th style="width: 20%;font-size: 12px">MOEDA</th>
              <th style="width: 20%;font-size: 12px">BSF TAXA</th>
              <th style="width: 20%;font-size: 12px"><center>STATUS</center></th>
              <th style="width: 20%;font-size: 22px">OPÇÕES</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in items">
              <tr>
                <td><p> @{{item.pais.descripcion}}</p></td>
                <td><p> @{{item.moneda}}</p></td>
                <td><p> @{{format(item.tasa,2)}}</p></td>
                <td align="center">
                  <template v-if="item.status == 1">
                    <span class="label label-success">
                      Ativo
                    </span>
                  </template>
				          <template v-if="item.status == 0">
                    <span class="label label-danger">
                      Inativo
                    </span>
                  </template>
                </td>
                <td class="ico_estado">
                  <div class="container-fluid">
                    <div class="col-md-4">
                      <a href="#" style="font-size: 1.5em" class="text-info"  v-on:click.prevent="reporte(item.historico)" title="Taxas"><span class="glyphicon glyphicon-list-alt"></span></a>
                    </div>
                  </div>
                </td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <div class="box-footer">
      <div class="row">
        <div class="col-sm-7 col-md-offset-5">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            <ul class="pagination pull-right">
              <li v-if="pagination.current_page > 1">
                <a href="#" aria-label="Previous" @click.prevent="changePage(pagination.current_page - 1)">
                  <span aria-hidden="true">«</span>
                </a>
              </li>
              <li v-for="page in pagesNumber" v-bind:class="[ page == isActived ? 'active' : '']">
                <a href="#" @click.prevent="changePage(page)">
                  @{{ page }}
                </a>
              </li>
              <li v-if="pagination.current_page < pagination.last_page">
                <a href="#" aria-label="Next" @click.prevent="changePage(pagination.current_page + 1)">
                  <span aria-hidden="true">»</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('elements')

<div class="modal fade" id="reporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">TAXAS</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="font-size: 22px">MOEDA</th>
              <th style="font-size: 22px">TAXA</th>
              <th style="font-size: 22px">USUÁRIO</th>
              <th style="font-size: 22px">DATA</th>
              <th style="font-size: 22px">HORA</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in reports">
              <tr>
                <td><p> @{{item.moneda.moneda}}</p></td>
                <td><p> @{{format(item.monto,2)}}</p></td>
                <td><p> @{{item.usuario.nombre}} @{{item.usuario.apellido}}</p></td>
                <td><p> @{{item.fecha}}</p></td>
                <td><p> @{{item.hora}}</p></td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>-->
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
{!!Html::script('js/vue/historico.js')!!} 
@endsection
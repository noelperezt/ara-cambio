@if(Auth::user()->rol != "Administrador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@extends('layouts.backend.template')
@section('title','Tipo do pagamento')
@section('description', 'Tipo do pagamento')
@section('content')
<section class="content" style="min-height: 100px">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">
            Adicionar pagamentos
          </h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" v-on:submit.prevent="createItem">
          <div class="box-body">
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.descricao}">
                <label for="exampleInputEmail1">
                  Agregar pagamentos
                </label>
                <input class="form-control" v-model="newItem.descricao" id="exampleInputEmail1" placeholder="Pagamento" type="text">
                <span class="help-block text-danger" v-if="formErrors.errors.descricao">@{{formErrors.errors.descricao}}</span>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3">
                <a href="#" v-on:click="createItem">
                  <i ;="" class=" glyphicon glyphicon-plus" style="padding-top: 30px;font-size: 25px">
                  </i>
                </a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
      <h2 class="box-title">
        Lista do Pagamentos
      </h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="width: 50%;font-size: 22px">
                Descrição
              </th>
              <th style="width: 25%;font-size: 22px">
                Estado
              </th>
              <th style="width: 25%;font-size: 22px">
                Opções
              </th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in items">
              <tr>
                <td>
                  <a href="pages/examples/invoice.html">
                    @{{item.descripcion}}
                  </a>
                </td>
                <td>
                  <template v-if="item.status == 1">
                    <span class="label label-success">
                      Ativo
                    </span>
                  </template>
                  <template v-if="item.status == 0">
                    <span class="label label-danger">
                      Inativo
                    </span>
                  </template>
                </td>
                <td>
                  <div class="icon">
                    <a href="#" class="text-warning" v-on:click.prevent="editItem(item.id)">
                      <i ;="" class="glyphicon glyphicon-pencil" style="width: 20%;font-size: 20px">
                      </i>
                    </a>
                  </div>
                </td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
  </div>
</section>
@endsection
@section('elements')
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Tipo do pagamento</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label for="">Descriçao</label>
                <input type="text" v-model="fillItem.descricao" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">Estado</label>
                <select v-model="fillItem.status" class="form-control">
                  <option value="1">Ativo</option>
                  <option value="0">Inativo</option>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('public/js/vue/tipo_pago.js')!!}
@endsection
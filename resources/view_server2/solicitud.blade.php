@section('title', 'Pedido')
@section('description', 'Lista do Pedidos')
@extends('layouts.backend.template')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <div class="col-md-10">
            <h2 class="box-title">Lista do Pedidos</h2>
          </div>
          <div class="col-md-2" style="text-align: right;">
            <a href="#"><i class="glyphicon glyphicon-plus" v-on:click.prevent="getCodeItem" style="padding-top: 7px;font-size: 25px; width: 15%; padding-right:50px;"></i></a>
            <i class="glyphicon glyphicon-search" data-toggle="collapse" href="#busqueda" aria-expanded="false" aria-controls="busqueda" style="padding-top: 7px;font-size: 25px ;cursor: pointer"></i>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="collapse" id="busqueda">
            <div class="row">                       
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Data de Pedido</label>
                  <input type="date" class="form-control" id="exampleInputEmail1" v-model="filter.data_de_pedido" v-on:change="filterItem" placeholder=" Escreva aqui">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Prioridade</label>
                  <select class="form-control select2" v-model="filter.prioridad" v-on:change="filterItem">
                      <option value="" disabled="" selected="">Seleccione</option>
                      <option value="1">Normal</option>
                      <option value="2">Urgente</option>
                    </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Cliente</label>
                  <select v-model="filter.cliente" class="form-control" v-on:change="filterItem">
                  <option value="" selected="">Seleccione</option>
                  <template v-for="cliente in clientes">
                    <option data-nombre="@{{cliente.nombre}} @{{cliente.apellido}}" data-id="@{{cliente.id}}"  data-cep="@{{cliente.cep}}" data-celular="@{{cliente.celular}}" value="@{{cliente.id}}">@{{cliente.nombre}} @{{cliente.apellido}}</option>
                  </template>
                </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tipo de Pagamento</label>
                  <select v-model="filter.tipo_pagamento" class="form-control" id="" v-on:change="filterItem">
                  <option value="" disabled="" selected="">Seleccione</option>
                  <template v-for="pago in pagos">
                    <option value="@{{pago.id}}">@{{pago.descripcion}}</option>
                  </template>
                </select>
                </div>
              </div>
              <div class="row"> 
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Banco</label>
                    <select class="form-control select2" v-model="filter.banco" v-on:change="filterItem">
                      <option value="" disabled="" selected="">Todos</option>
                      <template v-for="banco in bancos">
                        <option value="@{{banco.id}}">@{{banco.nombre}}</option>
                      </template>
                    </select>
                  </div> 
                </div> 
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mostrar</label>
                    <select class="form-control select2" v-model="filter.statusf" v-on:change="filterItem">
                      <option value="">Todos</option>
                      <option value="1">Pendientes</option>
                      <option value="2">Em processo</option>
                      <option value="3">Pago</option>
                      <option value="4">Rejeitado</option>
                      <option value="5">Reembolso</option>
                      <option value="6">Revisão</option>
                    </select>
                  </div> 
                </div> 
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table no-margin" style="font-size: 12px">
              <thead>
              <tr>
                <th style="font-size: 16px">ID</th>
                <th style="width: 7%;font-size: 16px">Data</th>
                <!--<th style="width: 7%;font-size: 16px">CPF</th>-->
                <th style="font-size: 16px">Cliente</th>
                <th style="font-size: 16px">Beneficiario</th>
                <th style="font-size: 16px">Banco</th>
                <th style="font-size: 16px">Taxa</th>
                <th style="font-size: 16px">Monto</th>
                <th style="font-size: 16px">Total</th>
                <th style="font-size: 16px">Estado</th>
                <th style="font-size: 16px">Usuário</th>
                <th style="font-size: 16px">Observações</th>
                <th style="font-size: 16px">Opções</th>
                <th style="font-size: 16px">Visualizar</th>
              </tr>
              </thead>
              <tbody>
                <template v-for="item in items">
                  <tr>
                    <td><p>ARA.000@{{item.id}}</p></td>
                    <td><p>@{{item.fecha}}</p></td>
                    <!--<td><p> @{{item.cliente.cep}}</p></td>-->
                    <td><p>@{{item.cliente.nombre}} @{{item.cliente.apellido}}</p></td>
                    <td><p>@{{item.beneficiario.nombre}} @{{item.beneficiario.apellido}}</p></td>
                    <td><p>@{{item.banco.nombre}}</p></td>
                    <td><p> @{{format(item.taza,2)}}</p></td>
                    <td><p> @{{format(item.monto,2)}}</p></td>
                    <td><p> @{{format(item.total,2)}}</p></td>
                    <td align="center">
                      <template v-if="item.status == 1">
                        <span class="label label-info">
                          Pendiente
                        </span>
                      </template>
                      <template v-if="item.status == 2">
                        <span class="label label-warning">
                          Em processo
                        </span>
                      </template>
                      <template v-if="item.status ==3">
                        <span class="label label-success">
                          Pago
                        </span>
                      </template>
                      <template v-if="item.status == 4">
                        <span class="label label-danger">
                          Rejeitado
                        </span>
                      </template>
                      <template v-if="item.status == 5">
                        <span class="label" style="background-color: #7F8C8D">
                          Reembolso
                        </span>
                      </template>
                      <template v-if="item.status == 6">
                        <span class="label" style="background-color: #D35400">
                          Revisão
                        </span>
                      </template>
                    </td>
                    <td><p>@{{item.usuario.username}}</p></td>
                    <td class="ico_estado" align="center">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-6">
                            <a href="#" style="font-size: 1.5em" v-on:click.prevent="describir(item.id)" title="Adicionar Observações"><span class="glyphicon glyphicon-comment"></span></a>
                          </div>
                          <div class="col-md-6">
                            <a href="#" style="font-size: 1.5em"  v-on:click.prevent="reporte(item.comentario)" title="Historico Observações"><span class="glyphicon glyphicon-list-alt"></span></a>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td class="ico_estado">
                      <div class="container-fluid">
                        <div class="row">
                          <template v-if="item.status == 1">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-info" v-on:click.prevent="destroyItem(item.id, 2)"><i class="glyphicon glyphicon-transfer" title="Em processo"></i></a>
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em; color: #7F8C8D" class="text-info" v-on:click.prevent="destroyItem(item.id, 5)"><i class="glyphicon glyphicon-arrow-left" title="Reembolso"></i></a> 
                            </div>
                          </template>
                          <template v-if="item.status == 2">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-success" v-on:click.prevent="destroyItem(item.id, 3)"><i class="glyphicon glyphicon-ok" title="Pago"></i></a>
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-danger" v-on:click.prevent="destroyItem(item.id, 4)"><i class="glyphicon glyphicon-remove" title="Rejeitado"></i></a>
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em; color: #D35400" class="text-info" v-on:click.prevent="destroyItem(item.id, 6)"><i class="glyphicon glyphicon-alert" title="Revisão"></i></a> 
                            </div>
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em; color: #7F8C8D" class="text-info" v-on:click.prevent="destroyItem(item.id, 5)"><i class="glyphicon glyphicon-arrow-left" title="Reembolso"></i></a> 
                            </div>
                          </template>
                          <template v-if="item.status == 4">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-info" v-on:click.prevent="destroyItem(item.id, 2)"><i class="glyphicon glyphicon-transfer" title="Em processo"></i></a>
                            </div>
                          </template>
                          <template v-if="item.status == 6">
                            <div class="col-md-1">
                              <a href="#" style="font-size: 1.5em" class="text-info" v-on:click.prevent="destroyItem(item.id, 2)"><i class="glyphicon glyphicon-transfer" title="Em processo"></i></a>
                            </div>
                          </template>
                        </div>
                      </div>
                    </td>
                    <td class="ico_estado" align="center">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-6">
                          <a href="#" class="text-warning" v-on:click="editItem(item.id)"><i class="glyphicon glyphicon-search"; style="font-size: 1.5em"></i></a>
                        </div>
                          <div class="col-md-6">
                            <a target="_blank" href="{{ url('factura') }}/@{{item.id}}" style="font-size: 1.5em" class="text-warning print" title="Impressão"><i class="glyphicon glyphicon-print"></i></a>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </template>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
      </div>
    </div>
  <!-- /.content -->
  </div>
  <div id="impresion" style="display: none;"></div>
</section>
@endsection
@section('elements')
<!-- Modal -->
<div class="modal fade" id="agregar" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Informação Pedido</h4>
        <h5 class="modal-title" id="exampleModalLabel" style="padding-top: 10px;">Código Pedido: <b>@{{newItem.codigo}}</b></h5>
      </div>
      <div class="modal-body ">
        <form action="" v-on:submit="createItem">
          <div class="row"> 
            <div class="col-md-2" >
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.data_de_pedido}">
                <label for="exampleInputEmail1">Data Pedido </label>
                <input type="date" class="form-control" id="exampleInputEmail1" v-model="newItem.data_de_pedido" placeholder=" Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.data_de_pedido">@{{formErrors.errors.data_de_pedido}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.prioridade}">
                <label for="exampleInputEmail1">Prioridade </label>
                <select class="form-control select2" v-model="newItem.prioridade">
                      <option value="1" selected="">Normal</option>
                      <option value="2">Urgente</option>
                    </select>
                <span class="help-block text-danger" v-if="formErrors.errors.prioridade">@{{formErrors.errors.prioridade}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.tipo_pagamento}">
                <label for="exampleInputEmail1">Tipo de pagamento </label>
                <select v-model="newItem.tipo_pagamento" class="form-control" id="">
                  <option value="" disabled="" selected="">Seleccione</option>
                  <template v-for="pago in pagos">
                    <option value="@{{pago.id}}">@{{pago.descripcion}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.tipo_pagamento">@{{formErrors.errors.tipo_pagamento}}</span>
               </div>
            </div>

            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.usuario}">
                <label for="exampleInputEmail1">Usuário </label>
                <select v-model="newItem.usuario" class="form-control" id="">
                  <template v-for="user in users">
                    <option value="@{{user.id}}">@{{user.nombre}} @{{user.apellido}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.usuario">@{{formErrors.errors.usuario}}</span>
               </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="exampleInputEmail1">Observación </label>
                <input type="text" class="form-control" v-model="newItem.descripcion" id="descripcion" name="descripcion" placeholder="Escreva aqui">
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">Cliente</h2>
          </div>
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cliente}">
                <label for="exampleInputEmail1">Nome</label>
                <select v-model="newItem.cliente" v-selecttwo="newItem.cliente" class="form-control cliente_select" id="select_cliente" style="width: 100%">
                    <option value="" selected="selected" disabled="">Seleccione</option>
                    <template v-for="cliente in clientes">
                      <option data-nombre="@{{cliente.nombre}} @{{cliente.apellido}}" data-id="@{{cliente.id}}"  data-cep="@{{cliente.cep}}" data-celular="@{{cliente.celular}}" value="" v-bind:value="cliente.id">@{{cliente.nombre}} @{{cliente.apellido}}</option>
                    </template>
                  </select>
                <span class="help-block text-danger" v-if="formErrors.errors.celular">@{{formErrors.errors.celular}}</span>
              </div>
            </div>
            <div class="col-md-2 cliente" style="display: none;">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_cliente}">
                 <label for="exampleInputEmail1">Nro. identificação</label>
                <input type="text" class="form-control" v-model="newItem.cep_cliente" id="cep_cliente" name="cep_cliente" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.cep_cliente">@{{formErrors.errors.cep_cliente}}</span>
              </div>
            </div>
            <!--<div class="col-md-1 nuevocliente">
              <i class="glyphicon glyphicon-plus" data-toggle="collapse" href="#cliente" aria-expanded="false" aria-controls="cliente" style="padding-top: 30px;font-size: 25px ;cursor: pointer"></i>
            </div>-->
            <div class="col-md-3 cliente" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">Nome Completo</label>
                <input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" placeholder="Escreva aqui" readonly>
              </div>
            </div>
            <div class="col-md-2 cliente" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">Celular</label>
                <input type="text" class="form-control" id="telefono_cliente" name="telefono_cliente" readonly placeholder="Escreva aqui">
              </div>
            </div>
            <div class="col-md-12 nuevocliente">  
              <div class="collapse" id="cliente">
                <div class="row">                       
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.nome_cliente}">
                      <label for="exampleInputEmail1">Nome</label>
                      <input type="text" class="form-control" v-model="newItem.nome_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.nome_cliente">@{{formErrors.errors.nome_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.sobrenome_cliente}">
                      <label for="exampleInputEmail1">Último Nome</label>
                      <input type="text" class="form-control" v-model="newItem.sobrenome_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.sobrenome_cliente">@{{formErrors.errors.sobrenome_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_cliente}">
                      <label for="exampleInputEmail1">Nro. identificação</label>
                      <input type="text" class="form-control" v-model="newItem.cep_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.cep_cliente">@{{formErrors.errors.cep_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.celular_cliente}">
                      <label for="exampleInputEmail1">Celular</label>
                      <input type="text" class="form-control" v-model="newItem.celular_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.celular_cliente">@{{formErrors.errors.celular_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.email_cliente}">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="text" class="form-control" v-model="newItem.email_cliente" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.email_cliente">@{{formErrors.errors.email_cliente}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.pais_cliente}">
                      <label for="exampleInputEmail1">País</label>
                      <select v-model="newItem.pais_cliente" id="" class="form-control">
                        <option value="" disabled="" selected="">Seleccione</option>
                        <template v-for="pais in paises">
                          <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                        </template>
                      </select>
                      <span class="help-block text-danger" v-if="formErrors.errors.pais_cliente">@{{formErrors.errors.pais_cliente}}</span>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">Beneficiario</h2>
          </div>
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.beneficiario}">
                <label for="exampleInputEmail1">Nome</label>
                <!--<select v-model="newItem.beneficiario" class="form-control selectpicker" id="select_beneficiario" v-on:change="getCuenta">
                  <template v-for="beneficiario in beneficiarios">
                    <option data-nombre="@{{beneficiario.nombre}} @{{beneficiario.apellido}}" data-id="@{{beneficiario.id}}"  data-cpf="@{{beneficiario.cpf}}" data-celular="@{{beneficiario.celular}}" value="@{{beneficiario.id}}">@{{beneficiario.nombre}} @{{beneficiario.apellido}}</option>
                  </template>
                </select>-->
                 <select v-model="newItem.beneficiario" v-selecttwo="newItem.beneficiario" class="form-control beneficiario_select" id="select_beneficiario" style="width: 100%" v-on:change="getCuenta">
                    <option value="" selected="selected" disabled="">Seleccione</option>
                    <template v-for="beneficiario in beneficiarios">
                      <option data-nombre="@{{beneficiario.nombre}} @{{beneficiario.apellido}}" data-id="@{{beneficiario.id}}"  data-cpf="@{{beneficiario.cpf}}" data-celular="@{{beneficiario.celular}}" value="" v-bind:value="beneficiario.id">@{{beneficiario.nombre}} @{{beneficiario.apellido}}</option>
                    </template>
                  </select>
                <span class="help-block text-danger" v-if="formErrors.errors.beneficiario">@{{formErrors.errors.beneficiario}}</span>
              </div>
            </div>
            <div class="col-md-2 beneficiario" style="display: none;">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_beneficiario}">
                 <label for="exampleInputEmail1">Nro. identificação</label>
                <input type="numer" class="form-control" v-model="newItem.cep_beneficiario" id="cep_beneficiario" name="cep_beneficiario" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.cep_beneficiario">@{{formErrors.errors.cep_beneficiario}}</span>
              </div>
            </div>
            <!--<div class="col-md-1 nuevobeneficiario">
              <i class="glyphicon glyphicon-plus" data-toggle="collapse" href="#beneficiario" aria-expanded="false" aria-controls="beneficiario" style="padding-top: 30px;font-size: 25px ;cursor: pointer"></i>
            </div>-->
            <div class="col-md-3 beneficiario" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">Nome Completo</label>
                <input type="text" class="form-control" id="nombre_beneficiario" name="nombre_beneficiario" placeholder="Escreva aqui" readonly>
              </div>
            </div>
            <div class="col-md-2 beneficiario" style="display: none;">
              <div class="form-group">
                <label for="exampleInputEmail1">Celular</label>
                <input type="text" class="form-control" id="telefono_beneficiario" name="telefono_beneficiario" readonly placeholder="Escreva aqui">
              </div>
            </div>
            <div class="col-md-2 beneficiario" style="display: none;">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cuenta}">
                <label for="exampleInputEmail1">Conta Bancárias</label>
                <select class="form-control select2" v-model="newItem.cuenta" id="cuenta_bancaria" v-bind:disabled="cuentas.length == 0">
                  <option selected="selected" disabled="" value="">Seleccione</option>
                  <template v-for="cuenta in cuentas">
                    <option value="" v-bind:value="cuenta.id" data-banco="@{{cuenta.banco.id}}">@{{cuenta.banco.nombre}} - @{{cuenta.cuenta}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.cuenta">@{{formErrors.errors.cuenta}}</span>
              </div> 
            </div> 
            <div class="col-md-12 nuevobeneficiario">  
              <div class="collapse" id="beneficiario">
                <div class="row">                       
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.nome_beneficiario}">
                      <label for="exampleInputEmail1">Nome</label>
                      <input type="text" class="form-control" v-model="newItem.nome_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.nome_beneficiario">@{{formErrors.errors.nome_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.sobrenome_beneficiario}">
                      <label for="exampleInputEmail1">Último Nome</label>
                      <input type="text" class="form-control" v-model="newItem.sobrenome_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.sobrenome_beneficiario">@{{formErrors.errors.sobrenome_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep_beneficiario}">
                      <label for="exampleInputEmail1">Nro. identificação</label>
                      <input type="text" class="form-control" v-model="newItem.cep_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.cep_beneficiario">@{{formErrors.errors.cep_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.celular_beneficiario}">
                      <label for="exampleInputEmail1">Celular</label>
                      <input type="text" class="form-control" v-model="newItem.celular_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.celular_beneficiario">@{{formErrors.errors.celular_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.email_beneficiario}">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="text" class="form-control" v-model="newItem.email_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.email_beneficiario">@{{formErrors.errors.email_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.pais_beneficiario}">
                      <label for="exampleInputEmail1">País</label>
                      <select v-model="newItem.pais_beneficiario" class="form-control" id="">
                        <option value="" disabled="" selected="">Seleccione</option>
                        <template v-for="pais in paises">
                          <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                        </template>
                      </select>
                      <span class="help-block text-danger" v-if="formErrors.errors.pais_beneficiario">@{{formErrors.errors.pais_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.banco_beneficiario}">
                      <label for="exampleInputEmail1">Banco</label>
                      <select v-model="newItem.banco_beneficiario" class="form-control" id="">
                        <option value="" disabled="" selected="">Seleccione</option>
                        <template v-for="banco in bancos">
                          <option value="@{{banco.id}}">@{{banco.nombre}}</option>
                        </template>
                      </select>
                      <span class="help-block text-danger" v-if="formErrors.errors.banco_beneficiario">@{{formErrors.errors.banco_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.tipo_conta_beneficiario}">
                      <label for="exampleInputEmail1">Tipo de Conta</label>
                      <input type="text" class="form-control" v-model="newItem.tipo_conta_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.tipo_conta_beneficiario">@{{formErrors.errors.tipo_conta_beneficiario}}</span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group" v-bind:class="{'has-error':formErrors.errors.conta_beneficiario}">
                      <label for="exampleInputEmail1">Conta Bancária</label>
                      <input type="number" class="form-control" v-model="newItem.conta_beneficiario" id="exampleInputEmail1" placeholder="Escreva aqui">
                      <span class="help-block text-danger" v-if="formErrors.errors.conta_beneficiario">@{{formErrors.errors.conta_beneficiario}}</span>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">Dinheiro</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.quantidade}">
                <label for="exampleInputEmail1">Quantidade </label>
                <input type="number" class="form-control" v-model="newItem.quantidade" id="monto" name="monto" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.quantidade">@{{formErrors.errors.quantidade}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.moneda}">
                <label for="exampleInputEmail1">Moneda</label>
                <select v-model="newItem.moneda" class="form-control" id="moneda" name="moneda">
                  <option value="" disabled="" selected="">Seleccione</option>
                  <template v-for="moneda in monedas">
                    <option data-tasa="@{{moneda.tasa}}" value="@{{moneda.id}}">@{{moneda.moneda}}</option>
                  </template>
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.moneda">@{{formErrors.errors.moneda}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.tasa_cambio}">
                <label for="exampleInputEmail1">Taxa </label>
                @if(Auth::user()->rol == "Administrador")
                <input type="text" class="form-control" v-model="newItem.tasa_cambio" id="tasa_cambio" name="tasa_cambio" placeholder="0">
                @else
                <input readonly type="text" class="form-control" v-model="newItem.tasa_cambio" id="tasa_cambio" name="tasa_cambio" placeholder="0">
                @endif
                <span class="help-block text-danger" v-if="formErrors.errors.tasa_cambio">@{{formErrors.errors.tasa_cambio}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.total_cambio}">
                <label for="exampleInputEmail1">Total </label>
                <input disabled type="text" class="form-control" v-model="newItem.total_cambio" id="total_cambio" name="total" placeholder="0">
                <span class="help-block text-danger" v-if="formErrors.errors.total_cambio">@{{formErrors.errors.total_cambio}}</span>
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">Transferências e Depósitos</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">Banco</label>
                <select class="form-control" v-model="newItem.banco_rpt">
                  <option value="" selected="">Seleccione</option>
                  <template v-for="banco in bancos">
                   <template v-if="banco.pais == 1">
                    <option value="@{{banco.id}}">@{{banco.nombre}}</option>
                     </template>
                  </template>
                </select>
              </div> 
            </div> 
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">Nro. Referência</label>
                <input type="text" class="form-control" v-model="newItem.nro_comprobante_rpt">
               </div>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click.prevent="createItem">Adicionar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="observacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Observações</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="observacoes">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="">Observações</label>
                <textarea v-model="observacion.value" class="form-control"></textarea>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="observacoes" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="reporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Observações</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="font-size: 22px">Observação</th>
              <th style="font-size: 22px">Usuário</th>
              <th style="font-size: 22px">Estado</th>
              <th style="font-size: 22px">Data</th>
              <th style="font-size: 22px">Hora</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in reports">
              <tr>
                <td><p> @{{item.descripcion}}</p></td>
                <td><p> @{{item.usuario.nombre}} @{{item.usuario.apellido}}</p></td>
                <td align="center">
                  <template v-if="item.status == 1">
                    <span class="label label-info">
                      Pendiente
                    </span>
                  </template>
                  <template v-if="item.status == 2">
                    <span class="label label-warning">
                      Em processo
                    </span>
                  </template>
                  <template v-if="item.status == 3">
                    <span class="label label-success">
                      Pago
                    </span>
                  </template>
                  <template v-if="item.status == 4">
                    <span class="label label-danger">
                      Rejeitado
                    </span>
                  </template>
                  <template v-if="item.status == 5">
                    <span class="label" style="background-color: #7F8C8D">
                      Reembolso
                    </span>
                  </template>
                  <template v-if="item.status == 6">
                    <span class="label" style="background-color: #D35400">
                      Revisão
                    </span>
                  </template>

                </td>
                <td><p> @{{item.fecha}}</p></td>
                <td><p> @{{item.hora}}</p></td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceitar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Pedido ARA.000@{{fillItem.id}} - @{{fillItem.fecha}} </h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
            <div class="box-header with-border">
              <h2 class="box-title">Remetente</h2>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nome</label>
                   <input type="text" maxlength="20" v-model="fillItem.nombre_cliente" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Sobrenome</label>
                   <input type="text" maxlength="20" v-model="fillItem.apellido_cliente" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Identidade</label>
                   <input type="text" maxlength="20" v-model="fillItem.identidad_cliente" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Telefone</label>
                   <input type="text" maxlength="20" v-model="fillItem.telefono_cliente" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="box-header with-border">
              <h2 class="box-title">Beneficiario</h2>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nome</label>
                   <input type="text" maxlength="20" v-model="fillItem.nombre_beneficiario" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Sobrenome</label>
                   <input type="text" maxlength="20" v-model="fillItem.apellido_beneficiario" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group" readonly>
                  <label for="exampleInputEmail1">Identidade</label>
                   <input type="text" maxlength="20" v-model="fillItem.identidad_beneficiario" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Telefone</label>
                   <input type="text" maxlength="20" v-model="fillItem.telefono_beneficiario" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="box-header with-border">
              <h2 class="box-title">Dados Bancarios</h2>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Banco de recebimento</label>
                  <select class="form-control select2" v-model="fillItem.banco" disabled>
                    <option selected="selected" disabled>Seleccione</option>
                    <template v-for="banco in bancos">
                      <option v-bind:value="banco.id">@{{banco.nombre}}</option>
                    </template>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">N. da Conta</label>
                   <input type="text" maxlength="20" v-model="fillItem.cuenta" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tipo da conta</label>
                   <input type="text" maxlength="20" v-model="fillItem.tipo_cuenta" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nro. de referência</label>
                   <input type="text" maxlength="20" v-model="fillItem.referencia" class="form-control">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Banco de transferência</label>
                  <select class="form-control select2" v-model="fillItem.banco_rpt" disabled>
                    <option selected="selected" disabled>Seleccione</option>
                    <template v-for="banco in bancos">
                      <option v-bind:value="banco.id">@{{banco.nombre}}</option>
                    </template>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nro. de referência</label>
                   <input type="text" maxlength="20" v-model="fillItem.nro_comprobante_rpt" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="box-header with-border">
              <h2 class="box-title">Valores</h2>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Moeda</label>
                  <input type="text" maxlength="20" v-model="fillItem.moneda" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Valor</label>
                   <input type="text" maxlength="20" v-model="fillItem.monto" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Taxa</label>
                   <input type="text" maxlength="20" v-model="fillItem.tasa" class="form-control" readonly>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Bolivar</label>
                   <input type="text" maxlength="20" v-model="fillItem.total" class="form-control" readonly>
                </div>
              </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('public/js/vue/solicitud.js')!!}
{!!Html::script('public/js/solicitudes.js?1.0')!!}
<script>
 $('.beneficiario_select').select2({
    theme: "bootstrap"
  });
  $('.cliente_select').select2({
    theme: "bootstrap"
  });
</script>
@endsection
@if(Auth::user()->rol != "Administrador" && Auth::user()->rol != "Operador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@section('title', 'Cliente')
@section('description', 'Lista do Cliente')
@extends('layouts.backend.template2')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <div class="col-md-10">
            <h2 class="box-title">Lista da Cliente</h2>
          </div>
          <div class="col-md-2" style="text-align: right;">
            <a href="#"><i class="glyphicon glyphicon-plus" v-on:click.prevent="getCodeItem" style="padding-top: 7px;font-size: 25px; width: 15%; padding-right:50px;"></i></a>
            <i class="glyphicon glyphicon-search" data-toggle="collapse" href="#busqueda" aria-expanded="false" aria-controls="busqueda" style="padding-top: 7px;font-size: 25px ;cursor: pointer"></i>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="collapse" id="busqueda">
            <div class="row">                       
              <div class="col-md-2">
                <div class="form-group">
                  <label for="exampleInputEmail1">Cep</label>
                  <input type="number" class="form-control" id="exampleInputEmail1" v-model="filter.cep" placeholder="Escreva aqui" v-on:keyUp="filterItem">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nome Completo </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" v-model="filter.nome_completo" placeholder="Escreva aqui" v-on:keyUp="filterItem">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleInputEmail1">Profissão</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" v-model="filter.profissao" placeholder="Escreva aqui" v-on:keyUp="filterItem">
                </div>
              </div>
              <div class="row"> 
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Pais</label>
                    <select class="form-control select2" v-model="filter.pais" v-on:change="filterItem">
                      <option value="" selected="">Todos</option>
                      <template v-for="pais in paises">
                        <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                      </template>
                    </select>
                  </div> 
                </div> 
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mostrar</label>
                    <select class="form-control select2" v-model="filter.status" v-on:change="filterItem">
                      <option value="">Todos</option>
                      <option value="1" selected="">Ativos</option>
                      <option value="0">Inativos</option>
                    </select>
                  </div> 
                </div> 
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
              <tr>
                <th style="width: 15%;font-size: 16px">Nome Completo</th>
                <th style="width: 20%;font-size: 16px">Nro. identificação</th>
                <th style="width: 10%;font-size: 16px">Pais</th>
                <th style="width: 10%;font-size: 16px">Celular</th>
                <th style="width: 15%;font-size: 16px">Email</th>
                <th style="width: 15%;font-size: 16px">Estado</th>
                <th style="width: 15%;font-size: 16px">Opções</th>
              </tr>
              </thead>
              <tbody>
                <template v-for="item in items">
                  <tr>
                    <td><p> @{{item.nombre}} @{{item.apellido}}</p></td>
                    <td><p>@{{item.cep}}</p></td>
                    <td><p>@{{item.pais.descripcion}}</p></td>
                    <td><p>@{{item.celular}}</p></td>
                    <td><p> @{{item.email}}</p></td>
                    <td>
                      <template v-if="item.status == 1">
                        <span class="label label-success">
                          Ativo
                        </span>
                      </template>
                      <template v-if="item.status == 0">
                        <span class="label label-danger">
                          Inativo
                        </span>
                      </template>
                    </td>
                    <td class="ico_estado">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-6">
                            <a href="#" style="font-size: 1.5em" class="text-warning" v-on:click.prevent="editItem(item.id)"><i class="glyphicon glyphicon-pencil"></i></a>
                          </div>
                          <template v-if="item.status == 1">
                            <div class="col-md-6">
                              <a href="#" style="font-size: 1.5em" class="text-danger" v-on:click.prevent="destroyItem(item.id)"><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                          </template>
                          <template v-if="item.status == 0">
                            <div class="col-md-6">
                              <a href="#" style="font-size: 1.5em" class="text-success" v-on:click.prevent="destroyItem(item.id)"><i class="glyphicon glyphicon-check"></i></a>
                            </div>
                          </template>
                        </div>
                      </div>
                    </td>
                  </tr>
                </template>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
      </div>
    </div>
  <!-- /.content -->
  </div>
</section>
@endsection
@section('elements')
<!-- Modal -->
<div class="modal fade" id="agregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Informação pessoal</h4>
        <h5 class="modal-title" id="exampleModalLabel" style="padding-top: 10px;">Codigo Cliente: <b>@{{newItem.codigo}}</b></h5>
      </div>
      <div class="modal-body ">
        <form action="" v-on:submit="createItem">
          <div class="row"> 
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.nome}">
                <label for="exampleInputEmail1">Nome </label>
                <input type="text" class="form-control" v-model="newItem.nome" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.nome">@{{formErrors.errors.nome}}</span>
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.ultimo_nome}">
                <label for="exampleInputEmail1">Último Nome </label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItem.ultimo_nome" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.ultimo_nome">@{{formErrors.errors.ultimo_nome}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cep}">
                <label for="exampleInputEmail1">Nro. identificação</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItem.cep" placeholder="Escreva aqui ">
                <span class="help-block text-danger" v-if="formErrors.errors.cep">@{{formErrors.errors.cep}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.data_de_nascimento}">
                <label for="exampleInputEmail1"> Data de nascimento</label>
                <input type="date" class="form-control" id="exampleInputEmail1" v-model="newItem.data_de_nascimento" placeholder=" Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.data_de_nascimento">@{{formErrors.errors.dat_de_nacimiento}}</span>
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group"  v-bind:class="{'has-error':formErrors.errors.profissao}">
                <label for="exampleInputEmail1">Profissão</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="newItem.profissao" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.profissao">@{{formErrors.errors.profissao}}</span>
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">Localização</h2>
        </div>
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.pais}">
                 <label for="exampleInputEmail1">Pais</label>
                <select class="form-control select2" v-model="newItem.pais">
                  <option selected="selected" disabled="" value="">Seleccione país</option>
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>                          
                </select>
                <span class="help-block text-danger" v-if="formErrors.errors.pais">@{{formErrors.errors.pais}}</span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group"  v-bind:class="{'has-error':formErrors.errors.estado}">
                <label for="exampleInputEmail1">Estado</label>
                <input type="text" class="form-control" v-model="newItem.estado" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.estado">@{{formErrors.errors.estado}}</span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.cidades}">
                <label for="exampleInputEmail1">Cidades</label>
                <input type="text" class="form-control" v-model="newItem.cidades" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.cidades">@{{formErrors.errors.cidades}}</span>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.bairrio}">
                 <label for="exampleInputEmail1">Bairrio</label>
                <input type="text" class="form-control" v-model="newItem.bairrio" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.bairrio">@{{formErrors.errors.bairrio}}</span>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.endereco_residencial}">
                <label for="exampleInputEmail1">Endereço Residencial</label>
                <input type="text" class="form-control" v-model="newItem.endereco_residencial" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.endereco_residencial">@{{formErrors.errors.endereco_residencial}}</span>
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">Dados de localização</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.telefone_de_localizacao}">
                <label for="exampleInputEmail1">telefone de localização </label>
                <input type="Text" class="form-control" v-model="newItem.telefone_de_localizacao" id="exampleInputEmail1" placeholder ="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.telefone_de_localizacao">@{{formErrors.errors.telefone_de_localizacao}}</span> 
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.celular}">
                <label for="exampleInputEmail1">Celular</label>
              <input type="number" class="form-control" v-model="newItem.celular" id="exampleInputEmail1" placeholder="Escreva aqui">
              <span class="help-block text-danger" v-if="formErrors.errors.celular">@{{formErrors.errors.celular}}</span>           </div>
            </div>
            <div class="col-md-3">
              <div class="form-group" v-bind:class="{'has-error':formErrors.errors.email}">
                <label for="exampleInputEmail1">Email</label>
                <input type="Email" class="form-control" v-model="newItem.email" id="exampleInputEmail1" placeholder="Escreva aqui">
                <span class="help-block text-danger" v-if="formErrors.errors.email">@{{formErrors.errors.email}}</span>
               </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click.prevent="createItem">Adicionar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal editar-->
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 90%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Informação pessoal</h4>
        <h5 class="modal-title" id="exampleModalLabel" style="padding-top: 10px;">Codigo Cliente: <b>@{{fillItem.codigo}}</b></h5>
      </div>
      <div class="modal-body ">
        <form action="" v-on:submit="updateItem(fillItem.id)">
          <div class="row"> 
            <div class="col-md-3">
              <div class="form-group">
                <label for="exampleInputEmail1">Nome </label>
                <input type="text" class="form-control" v-model="fillItem.nome" id="exampleInputEmail1" placeholder="Escreva aqui">
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="exampleInputEmail1">Último Nome </label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="fillItem.ultimo_nome" placeholder="Escreva aqui">
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">Nro. identificação</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="fillItem.cep" placeholder="Escreva aqui ">
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1"> Data de nascimento</label>
                <input type="date" class="form-control" id="exampleInputEmail1" v-model="fillItem.data_de_nascimento" placeholder=" Escreva aqui">
               </div>
            </div>  
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">Profissão</label>
                <input type="text" class="form-control" id="exampleInputEmail1" v-model="fillItem.profissao" placeholder="Escreva aqui">
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">Localização</h2>
          </div>
          <div class="row"> 
            <div class="col-md-2">
              <div class="form-group">
                 <label for="exampleInputEmail1">Pais</label>
                <select class="form-control select2" v-model="fillItem.pais">

                  <option selected="selected" disabled="">Seleccione país</option>
                  <template v-for="pais in paises">
                    <option value="@{{pais.id}}">@{{pais.descripcion}}</option>
                  </template>                          
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">Estado</label>
                <input type="text" class="form-control" v-model="fillItem.estado">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">Cidades</label>
                <input type="text" class="form-control" v-model="fillItem.cidades">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                 <label for="exampleInputEmail1">Bairrio</label>
                <input type="text" class="form-control" v-model="fillItem.bairrio">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="exampleInputEmail1">Endereço Residencial</label>
                <input type="text" class="form-control" v-model="fillItem.endereco_residencial" id="exampleInputEmail1" placeholder="Residencial">
               </div>
            </div>
          </div>
          <div class="box-header with-border">
            <h2 class="box-title">Dados de localização</h2>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">telefone de localização </label>
                <input type="Text" class="form-control" v-model="fillItem.telefone_de_localizacao" id="exampleInputEmail1" placeholder="Escreva aqui">
               </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="exampleInputEmail1">Celular</label>
                <input type="numer" class="form-control" v-model="fillItem.celular" id="exampleInputEmail1" placeholder="Escreva aqui">
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="Email" class="form-control" v-model="fillItem.email" id="exampleInputEmail1" placeholder="Escreva aqui">
               </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click.prevent="updateItem(fillItem.id)">Adicionar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('public/js/vue/cliente.js')!!}
@endsection

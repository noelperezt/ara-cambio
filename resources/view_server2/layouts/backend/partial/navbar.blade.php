<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a class="sidebar-toggle" data-toggle="push-menu" href="#" role="button">
    <span class="sr-only">
      Toggle navigation
    </span>
  </a>
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <li class="dropdown user user-menu">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          Bem vindo,
            <span class="hidden-xs">
               &nbsp;<strong>{{Auth::user()->nombre}} {{Auth::user()->apellido}}</strong>
            </span>
          </img>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <img alt="User Image" class="img-circle" src="public/dist/img/user.png">
              <p>
                {{Auth::user()->nombre}} {{Auth::user()->apellido}}
                <small>
                  {{Auth::user()->rol}}
                </small>
              </p>
            </img>
          </li>
          <li class="user-footer">
            <div class="pull-left">
              <a class="btn btn-default btn-flat" href="#">
                Profile
              </a>
            </div>
            <div class="pull-right">
              <a class="btn btn-danger btn-flat" href="{{ url('login/logout') }}">
              Fechar Sessão
              </a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
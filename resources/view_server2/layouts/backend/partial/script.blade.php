<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="{!! asset('public/jquery/dist/jquery.min.js') !!}">
</script>
<!-- jQuery UI 1.11.4 -->
<script src="{!! asset('public/jquery-ui/jquery-ui.min.js') !!}">
</script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{!! asset ('public/bootstrap/js/bootstrap.min.js')!!}">
</script>
<!-- FastClick -->
<script src="{!! asset ('public/fastclick/lib/fastclick.js') !!}">
</script>
<!-- AdminLTE App -->
<script src="{!! asset ('public/dist/js/adminlte.min.js') !!}">
</script>

<!-- Vue -->
<script src="{!! asset ('public/js/vue.min.js') !!}">
</script>
<!-- Vue-Resource -->
<script src="{!! asset ('public/js/vue-resource.min.js') !!}">
</script>
<script src="{!! asset ('public/select2/js/select2.min.js') !!}">
</script>
@yield('js')
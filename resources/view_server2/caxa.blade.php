@if(Auth::user()->rol == "Operador")
<?php
echo "
<script language='JavaScript'>
location.href = '../'
</script>";
?>
@endif
@extends('layouts.backend.template')
@section('title', 'Caxa')
@section('content')
@if(Auth::user()->rol == "Administrador")
<section class="content" style="min-height: 100px">
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <div class="row">
            <div class="col-md-10">
              <h3 class="box-title">Adicionar pagamentos</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
              <form role="form" v-on:submit.prevent="createItem">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Pais</label>
                    <select class="form-control select2" v-model="newItem.pais" v-on:change="getBanco">
                      <option selected="selected" value="">Seleccione</option>
                      <template v-for="pais in paises">
                        <option value="" v-bind:value="pais.id">@{{pais.descripcion}} </option>
                      </template>
                    </select>
                  </div> 
                </div> 
                <div class="col-md-3">
                  <div class="form-group" v-bind:class="{'has-error':formErrors.errors.banco}">
                    <label for="exampleInputEmail1">Banco</label>
                    <select class="form-control select2" v-model="newItem.banco" v-bind:disabled="bancos.length == 0">
                      <option selected="selected" disabled="" value="">Seleccione</option>
                      <template v-for="banco in bancos">
                        <option value="" v-bind:value="banco.id">@{{banco.codigo}} - @{{banco.nombre}}</option>
                      </template>
                    </select>
                    <span class="help-block text-danger" v-if="formErrors.errors.banco">@{{formErrors.errors.banco}}</span>
                  </div> 
                </div> 
                <div class="col-md-3">
                  <div class="form-group" v-bind:class="{'has-error':formErrors.errors.montante}">
                    <label for="exampleInputEmail1">Montante</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Escreva aqui" v-model="newItem.montante">
                    <span class="help-block text-danger" v-if="formErrors.errors.montante">@{{formErrors.errors.montante}}</span>
                  </div>
                </div>
                <div class="col-md-3">
                  <br>
                  <a href="#" class="text-primary" v-on:click.prevent="createItem"><i class="glyphicon glyphicon-plus"; style="padding-top: 10px;font-size: 25px;"></i></a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endif
<section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
      <h2 class="box-title">Movimento de caixa</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="width: 20%;font-size: 22px">ID</th>
              <th style="width: 20%;font-size: 22px">Banco</th>
              <th style="width: 15%;font-size: 19px" class="text-center">Nro transações</th>
              <th style="width: 25%;font-size: 22px" class="text-center">Saldo Depositado</th>
              <th style="width: 25%;font-size: 22px" class="text-center">Opciones</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in items">
              <tr>
                <td><p> @{{item.id}}</p></td>
                <td><p> @{{item.banco.nombre}}</p></td>
                <td><p class="text-center"> @{{transacciones(item.historico)}}</p></td>
                <td><p class="text-right"> @{{format(item.monto,2)}}</p></td>
                <td class="text-center">
                  <a href="#" style="font-size: 1.5em" class="text-info"  v-on:click.prevent="reporte(item.historico)" title="Observações"><span class="glyphicon glyphicon-list-alt"></span></a>
                  <a href="#" style="font-size: 1.5em; margin-left: 10px"   class="text-info" v-on:click.prevent="depositar(item.id)" title="Depósito"><span class="glyphicon glyphicon-piggy-bank"></span></a>
                      
                </td>
              </tr>
            </template>
            <tr align="right">
              <td colspan="3" align="right" style="font-size: 12px; font-weight:bold">Renda Total</td>
              <td style="color: green"><p class="text-right">@{{format(cajas,2)}}</p></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3" align="right" style="font-size: 12px; font-weight:bold">Despesas Totais</td>
              <td  style="color: red"><p class="text-right">@{{format(solicitudes,2)}}</p></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3" align="right" style="font-size: 12px; font-weight:bold">Total disponível</td>
              <td><p class="text-right">@{{format(cajas-solicitudes,2)}}</p></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
  </div> 
</section>
@endsection
@section('elements')
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar País.</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="updateItem(fillItem.id)">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Nome do país</label>
                <input type="text" v-model="fillItem.nome" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Estado</label>
                <select v-model="fillItem.status" class="form-control">
                  <option value="1">Ativo</option>
                  <option value="0">Inativo</option>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="updateItem(fillItem.id)" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deposito" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Depósito na caixa</h4>
      </div>
      <div class="modal-body">
        <form action="" v-on:submit.prevent="deposite">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="">Montante a depositar</label>
                <input type="number" v-model="deposito.value" class="form-control">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" v-on:click.prevent="deposite" class="btn btn-primary">Guardar mudanças.</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="reporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Histórico de depósitos</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="font-size: 22px">Banco</th>
              <th style="font-size: 22px">Usuário</th>
              <th style="font-size: 22px">Monto</th>
              <th style="font-size: 22px">Data</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in reports">
              <tr>
                <td><p> @{{item.caja.banco.nombre}}</p></td>
                <td><p> @{{item.usuario.nombre}} @{{item.usuario.apellido}}</p></td>
                <td><p> @{{format(item.monto,2)}}</p></td>
                <td><p> @{{item.created_at}}</p></td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceitar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
{!!Html::script('public/js/vue/caja.js')!!}
@endsection
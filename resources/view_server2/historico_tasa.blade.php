@extends('layouts.backend.template')
@section('title', 'Moedas')
@section('description', 'Histórico do Taxas')
@section('content')
<section class="col-lg-12 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
      <h2 class="box-title">Histórico do taxas</h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="width: 20%;font-size: 22px">País</th>
              <th style="width: 20%;font-size: 22px">Moeda</th>
              <th style="width: 20%;font-size: 22px">Taxa</th>
              <th style="width: 20%;font-size: 22px"><center>Estado</center></th>
              <th style="width: 20%;font-size: 22px">Opções</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in items">
              <tr>
                <td><p> @{{item.pais.descripcion}}</p></td>
                <td><p> @{{item.moneda}}</p></td>
                <td><p> @{{format(item.tasa,2)}}</p></td>
                <td align="center">
                  <template v-if="item.status == 1">
                    <span class="label label-success">
                      Ativo
                    </span>
                  </template>
          <template v-if="item.status == 0">
                    <span class="label label-danger">
                      Inativo
                    </span>
                  </template>
                </td>
                <td class="ico_estado">
                      <div class="container-fluid">
                          <div class="col-md-4">
                            <a href="#" style="font-size: 1.5em" class="text-info"  v-on:click.prevent="reporte(item.historico)" title="Taxas"><span class="glyphicon glyphicon-list-alt"></span></a>
                          </div>
                        </div>
                      </div>
                    </td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
  </div>
</section>
@endsection

@section('elements')

<div class="modal fade" id="reporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Taxas</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th style="font-size: 22px">Moeda</th>
              <th style="font-size: 22px">Taxa</th>
              <th style="font-size: 22px">Usuário</th>
              <th style="font-size: 22px">Data</th>
              <th style="font-size: 22px">Hora</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="item in reports">
              <tr>
                <td><p> @{{item.moneda.moneda}}</p></td>
                <td><p> @{{format(item.monto,2)}}</p></td>
                <td><p> @{{item.usuario.nombre}} @{{item.usuario.apellido}}</p></td>
                <td><p> @{{item.fecha}}</p></td>
                <td><p> @{{item.hora}}</p></td>
              </tr>
            </template>
          </tbody>
        </table>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceitar</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
{!!Html::script('public/js/vue/historico.js')!!} 
@endsection
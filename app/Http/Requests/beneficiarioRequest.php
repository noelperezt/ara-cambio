<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class beneficiarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'codigo'=> 'required|max:20',
            'nome' => 'required|max:30',
            'ultimo_nome'=> 'required|max:30',
            'celular'=> 'max:30',
            'cpf'=> 'required|max:30',
        ];
    }
}

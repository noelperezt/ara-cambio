<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class cuentaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'beneficario'   => 'required',
            'banco'         => 'required',
            'tipo_da_conta' => 'required',
            'numero_da_conta'=> 'required|unique:cuentas,cuenta|min:19|max:21',
        ];
    }
}

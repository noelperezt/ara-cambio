<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\usuarioRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Unidad;

class usuarioController extends Controller
{
    public function index()
    {
        $items = User::orderBy('nombre', 'asc')->paginate(15);
        return response()->json($items);
    }

     public function getUnidad()
    {
        $items = Unidad::orderBy('descripcion', 'asc')->get();
        return response()->json($items);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'senha' => 'required|min:6|max:255',
            'senha1' => 'required|min:6|max:255',
            'nome' => 'required|max:20',
            'nome2' => 'required|max:20',
            'username' => 'required|max:20|min:4|unique:users',
            'rol' => 'required|max:20',
            'unidad' => 'required|max:11'
        ]);
        User::create([
            'nombre' => $request -> nome, 
            'apellido' => $request -> nome2, 
            'username' => $request -> username, 
            'password' => bcrypt($request -> senha), 
            'unidad' => $request-> unidad,
            'rol' => $request -> rol, 
            

        ]);
        return response()->json('Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::find($id);
        return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = User::find($id);
        if ($request->senha == '') {
            $item->fill([
            'nombre' => $request -> nome, 
            'apellido' => $request -> nome2, 
            'username' => $request -> username, 
            'unidad' => $request-> unidad,
            'rol' => $request -> rol,
        ]);
        }else{
            $item->fill([
            'nombre' => $request -> nome, 
            'apellido' => $request -> nome2, 
            'username' => $request -> username, 
            'password' => bcrypt($request -> senha), 
            'unidad' => $request-> unidad,
            'rol' => $request -> rol,
        ]);
        }
        
        $item->save();

        return response()->json('Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = User::find($id);
        $item->destroy;

        return response()->json('Success');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }


}

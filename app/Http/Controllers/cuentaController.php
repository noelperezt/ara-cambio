<?php

namespace App\Http\Controllers;

use App\Http\Requests\cuentaRequest;
use Illuminate\Http\Request;
use App\Cuenta;
use App\Banco;
use App\Beneficiario;

class cuentaController extends Controller
{
    public function index()
    {
        $items = Cuenta::orderBy('beneficiario', 'asc')->with('banco')->with('beneficiario')->paginate(15);
        return response()->json($items);
    }

    public function getBanco()
    {
    	$items = Banco::where('status',1)->orderBy('nombre', 'asc')->get();
        return response()->json($items);
    }

    public function getBeneficiario()
    {
        $items = Beneficiario::orderBy('nombre', 'asc')->get();
        return response()->json($items);
    }

public function filterData(Request $request)

    {

       if(($request->beneficiario) && ($request->banco)){
            $items = Cuenta::where('beneficiario', $request->beneficiario)

            ->where('banco', $request->banco)

            ->where('tipo_cuenta', 'LIKE', '%'.$request->tipo_da_conta.'%')

            ->with('banco')

            ->with('beneficiario')

            ->orderBy('beneficiario', 'asc')

            ->paginate(15);
        }
        if(!($request->beneficiario) && ($request->banco)){
            $items = Cuenta::where('banco', $request->banco)

            ->where('tipo_cuenta', 'LIKE', '%'.$request->tipo_da_conta.'%')

            ->with('banco')

            ->with('beneficiario')

            ->orderBy('beneficiario', 'asc')

            ->paginate(15);
        }
        if(($request->beneficiario) && !($request->banco)){
            $items = Cuenta::where('beneficiario', $request->beneficiario)

            ->where('tipo_cuenta', 'LIKE', '%'.$request->tipo_da_conta.'%')

            ->with('banco')

            ->with('beneficiario')

            ->orderBy('beneficiario', 'asc')

            ->paginate(15);
        }

        if(!($request->beneficiario) && !($request->banco)){
            $items = Cuenta::where('tipo_cuenta', 'LIKE', '%'.$request->tipo_da_conta.'%')

            ->with('banco')

            ->with('beneficiario')

            ->orderBy('beneficiario', 'asc')

            ->paginate(15);
        }


        return response()->json($items);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(cuentaRequest $request)
    {
        $cuenta = Cuenta::create([
            'beneficiario'  => $request->beneficario,
            'banco'         => $request->banco,
            'tipo_cuenta'   => $request->tipo_da_conta,
            'cuenta'        => $request->numero_da_conta
        ]);
        return response()->json($cuenta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Cuenta::find($id);
        return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Cuenta::find($id);
        //$item->fill($request->all());
        $item->fill([
            'beneficiario'      => $request->beneficario,
            'banco'      => $request->banco,
            'tipo_cuenta'      => $request->tipo_da_conta,
            'cuenta'      => $request->numero_da_conta,
        ]);
        $item->save();

        return response()->json('Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Cuenta::find($id);
        if ($item->status) {
            $item->fill([
                'status' => 0
            ]);
        }else{
            $item->fill([
                'status' => 1
            ]);
        }
        $item->save();

        return response()->json('Success');
    }
}

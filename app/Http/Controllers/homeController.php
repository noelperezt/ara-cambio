<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Moneda;

class homeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Moneda::where('status',1)->orderBy('moneda', 'asc')->get();
        return view('index', compact('items'));
    }


    public function __construct()
    {
        $this->middleware('auth');
    }

}

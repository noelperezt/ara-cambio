<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solicitud;
use App\Cliente;
use App\Beneficiario;
use App\Cuenta;
use App\Banco;
use App\Moneda;
use App\User;

class facturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        $solicitud = Solicitud::find($id);
        $cliente = Cliente::find($solicitud->cliente);
        $beneficiario = Beneficiario::find($solicitud->beneficiario);
        $cuenta = Cuenta::find($solicitud->cuenta);
        $banco = Banco::find($solicitud->banco);
        $moneda = Moneda::find($solicitud->moneda);
        $usuario = User::find($solicitud->usuario);
        return view('factura')->with(compact('solicitud'))->with(compact('cliente'))->with(compact('beneficiario'))->with(compact('cuenta'))->with(compact('banco'))->with(compact('moneda'))->with(compact('usuario'));

    }


    public function __construct()
    {
        $this->middleware('auth');
    }

}

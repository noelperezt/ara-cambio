<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\pagosRequest;
use App\Tipo;

class tipoPagoController extends Controller
{
    public function index()
    {
        $items = Tipo::orderBy('status', 'desc')->paginate(15);
        return response()->json($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(pagosRequest $request)
    {
        Tipo::create([
            'descripcion'   => $request->descricao,
        ]);
        return response()->json('Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Tipo::find($id);
        return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Tipo::find($id);
        $item->fill([
            'descripcion'   => $request->descricao,
            'status'        => $request->status
        ]);
        $item->save();

        return response()->json('Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Tipo::find($id);
        $item->destroy;

        return response()->json('Success');
    }

     public function __construct()
    {
        $this->middleware('auth');
    }
    
}

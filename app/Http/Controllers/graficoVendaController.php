<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\Solicitud;



use DB; 


class graficoVendaController extends Controller

{

   

   public function index()

    {
        $unidades=DB::select(DB::raw ('select unidades.descripcion as unidades, meses.descripcion as mes,ifnull((select sum(monto) from solicitudes where solicitudes.status!=4 and solicitudes.status!=5  and unidad=unidades.id and meses.id=  MONTH(fecha)),0) as total  from meses, unidades order by unidades.id, meses.id asc'));
    
        $json_response = json_encode($unidades);
        $items = json_decode($json_response,TRUE);


        $k = 0;
        for($i=0; $i < count($items)/12; $i++){
            $aux = $i + 1;
            $aux2 = ($aux * 12) - 1;
            $resultado[$i]['Filial'] = $items[$aux2]['unidades'];    
            for ($j = 0; $j < 12 ; $j++){
                $consulta[$j]['Mes'] = $items[$k]['mes'];
                $consulta[$j]['Total'] =number_format($items[$k]['total'],2,'.','');//; number_format($numero, 2);
                $resultado[$i]['Valores'] =  $consulta;
                $k++;
            }
        }

        return json_encode($resultado);
    }



    public function filterData(Request $request)

    {

        $items = \App\Solicitud::where('fecha', 'LIKE', '%'.$request->data_de_pedido.'%')

        ->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

        ->where('cliente', 'LIKE', '%'.$request->cliente.'%')

        ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

        ->where('banco', 'LIKE', '%'.$request->banco.'%')

        ->where('status', 'LIKE', '%'.$request->statusf.'%')

        ->with('cliente')->with('beneficiario')->with('banco')

        ->paginate(15);

        return response()->json($items);

    }



    public function __construct()

    {

        $this->middleware('auth');

    }



}


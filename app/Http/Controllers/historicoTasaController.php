<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Moneda;

class historicoTasaController extends Controller
{
    public function index()
    {
        $items = Moneda::orderBy('updated_at', 'asc')->with(['historico','historico.usuario','historico.moneda'])->with('pais')->paginate(15);
        return response()->json($items);
    }
    
     public function __construct()
    {
        $this->middleware('auth');
    }
    
}

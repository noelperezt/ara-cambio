<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\bancoRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Pais;
use App\Banco;

class bancoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $items = Banco::orderBy('nombre', 'asc')->with('pais')->paginate(15);
        return response()->json($items);

    }

    public function getPais()
    {
    	$items = Pais::orderBy('descripcion', 'asc')->get();
        return response()->json($items);
    }


    public function filterData(Request $request)
    {
        $items = Banco::where('pais', 'LIKE', '%'.$request->pais.'%')
        ->with('pais')
        ->orderBy('pais', 'asc')
        ->paginate(15);
        return response()->json($items);
    }
    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(bancoRequest $request)
    {
        Banco::create([
            'nombre'    => $request->nome,
            'pais'      => $request->pais,
            'codigo'    => $request->codigo,
        ]);
        return response()->json('Success');
    }

    public function deposito(Request $request){
        $item = Banco::find($request->id);
        $item->fill([
            'caja' => (int)$item->caja + (int)$request->value
        ]);
        $item->save();
        return response()->json('Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Banco::find($id);
        return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Banco::find($id);
        $item->fill([
            'nombre'    => $request->nome,
            'pais'      => $request->pais,
            'codigo'    => $request->codigo,
            'status'    => $request->status,
        ]);
        $item->save();

        return response()->json('Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Banco::find($id);
        if ($item->status) {
            $item->fill([
                'status' => 0
            ]);
        }else{
            $item->fill([
                'status' => 1
            ]);
        }
        $item->save();

        return response()->json('Success');
    }

     public function __construct()
    {
        $this->middleware('auth');
    }
    
}

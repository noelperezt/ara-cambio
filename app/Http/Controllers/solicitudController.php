<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Http\Requests\solicitudRequest;

use App\Http\Requests\observacionRequest;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Response;

use App\Solicitud;

use App\Tipo;

use App\User;

use App\Pais;

use App\Banco;

use App\Beneficiario;

use App\Moneda;

use App\Cliente;

use App\Cuenta;

use App\Observacion;

use App\Caja;

use Carbon\Carbon;

use App\Unidad;


class solicitudController extends Controller

{

   

   public function index()

    {

        

        if(Auth::user()->rol == "Administrador" || Auth::user()->rol == "Receptor"){

            $items = Solicitud::orderBy('created_at', 'desc')->with('cliente')->with('beneficiario')->with('banco')->with('usuario')->with(['comentario','comentario.usuario'])->paginate(15);

        }else{

           $items = Solicitud::where('usuario', Auth::user()->id)->orderBy('created_at', 'desc')->with('cliente')->with('beneficiario')->with('usuario')->with('banco')->with(['comentario','comentario.usuario'])->paginate(15); 

        }

        return response()->json($items);

    }



    public function filterData(Request $request)

    {



        if(Auth::user()->rol == "Administrador" || Auth::user()->rol == "Receptor"){

            if(($request->data_de_pedido) && ($request->data_de_pedido2)){

                $items = \App\Solicitud::orderBy('created_at', 'desc')->whereBetween('fecha',[$request->data_de_pedido, $request->data_de_pedido2])

                ->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

                ->where('cliente', 'LIKE', $request->cliente)

                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

                ->where('banco', 'LIKE', $request->banco)

                ->where('status', 'LIKE', '%'.$request->statusf.'%')

                ->where('usuario', 'LIKE', $request->usuario)

                ->where('unidad', 'LIKE', '%'.$request->unidad.'%')

                ->with('cliente')->with('beneficiario')->with('banco')->with(['comentario','comentario.usuario'])

                ->with('usuario')

                ->paginate(15);

            }

            if(!($request->data_de_pedido) && ($request->data_de_pedido2)){

                $items = \App\Solicitud::orderBy('created_at', 'desc')->whereBetween('fecha',['1900-01-01', $request->data_de_pedido2])

                ->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

                ->where('cliente', 'LIKE', $request->cliente)

                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

                ->where('banco', 'LIKE', $request->banco)

                ->where('status', 'LIKE', '%'.$request->statusf.'%')

                ->where('usuario', 'LIKE', $request->usuario)

                ->where('unidad', 'LIKE', '%'.$request->unidad.'%')

                ->with('cliente')->with('beneficiario')->with('banco')->with(['comentario','comentario.usuario'])

                ->with('usuario')

                ->paginate(15);

            }

            if(($request->data_de_pedido) && !($request->data_de_pedido2)){

                $items = \App\Solicitud::orderBy('created_at', 'desc')->whereBetween('fecha',[$request->data_de_pedido, '2500-12-31'])

                ->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

                ->where('cliente', 'LIKE', $request->cliente)

                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

                ->where('banco', 'LIKE', $request->banco)

                ->where('status', 'LIKE', '%'.$request->statusf.'%')

                ->where('usuario', 'LIKE', $request->usuario)

                ->where('unidad', 'LIKE', '%'.$request->unidad.'%')

                ->with('cliente')->with('beneficiario')->with('banco')->with(['comentario','comentario.usuario'])

                ->with('usuario')

                ->paginate(15);

            }

            if(!($request->data_de_pedido) && !($request->data_de_pedido2)){

                $items = \App\Solicitud::orderBy('created_at', 'desc')->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

                ->where('cliente', 'LIKE', $request->cliente)

                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

                ->where('banco', 'LIKE', $request->banco)

                ->where('status', 'LIKE', '%'.$request->statusf.'%')

                ->where('usuario', 'LIKE', $request->usuario)

                ->where('unidad', 'LIKE', '%'.$request->unidad.'%')

                ->with('cliente')->with('beneficiario')->with('banco')->with(['comentario','comentario.usuario'])

                ->with('usuario')

                ->paginate(15);

            }

            

            return response()->json($items);

        }else{

            if(($request->data_de_pedido) && ($request->data_de_pedido2)){

                $items = \App\Solicitud::orderBy('created_at', 'desc')->whereBetween('fecha',[$request->data_de_pedido, $request->data_de_pedido2])

                ->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

                ->where('cliente', 'LIKE', $request->cliente)

                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

                ->where('banco', 'LIKE', $request->banco)

                ->where('status', 'LIKE', '%'.$request->statusf.'%')

                ->where('usuario', 'LIKE', $request->usuario)

                ->where('unidad', 'LIKE', '%'.$request->unidad.'%')

                ->where('usuario', Auth::user()->id)

                ->with('cliente')->with('beneficiario')->with('banco')->with(['comentario','comentario.usuario'])

                ->with('usuario')

                ->paginate(15);

            }

            if(!($request->data_de_pedido) && ($request->data_de_pedido2)){

                $items = \App\Solicitud::orderBy('created_at', 'desc')->whereBetween('fecha',['1900-01-01', $request->data_de_pedido2])

                ->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

                ->where('cliente', 'LIKE', $request->cliente)

                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

                ->where('banco', 'LIKE', $request->banco)

                ->where('status', 'LIKE', '%'.$request->statusf.'%')

                ->where('usuario', 'LIKE', $request->usuario)

                ->where('unidad', 'LIKE', '%'.$request->unidad.'%')

                ->where('usuario', Auth::user()->id)

                ->with('cliente')->with('beneficiario')->with('banco')->with(['comentario','comentario.usuario'])

                ->with('usuario')

                ->paginate(15);

            }

            if(($request->data_de_pedido) && !($request->data_de_pedido2)){

                 $items = \App\Solicitud::orderBy('created_at', 'desc')->whereBetween('fecha',[$request->data_de_pedido, '2500-12-31'])

                ->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

                ->where('cliente', 'LIKE', $request->cliente)

                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

                ->where('banco', 'LIKE', $request->banco)

                ->where('status', 'LIKE', '%'.$request->statusf.'%')

                ->where('usuario', 'LIKE', $request->usuario)

                ->where('unidad', 'LIKE', '%'.$request->unidad.'%')

                ->where('usuario', Auth::user()->id)

                ->with('cliente')->with('beneficiario')->with('banco')->with(['comentario','comentario.usuario'])

                ->with('usuario')

                ->paginate(15);

            }

            if(!($request->data_de_pedido) && !($request->data_de_pedido2)){

                $items = \App\Solicitud::orderBy('created_at', 'desc')->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

                ->where('cliente', 'LIKE', $request->cliente)

                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

                ->where('banco', 'LIKE', $request->banco)

                ->where('status', 'LIKE', '%'.$request->statusf.'%')

                ->where('usuario', 'LIKE', $request->usuario)

                ->where('usuario', Auth::user()->id)

                ->where('unidad', 'LIKE', '%'.$request->unidad.'%')

                ->with('cliente')->with('beneficiario')->with('banco')->with(['comentario','comentario.usuario'])

                ->with('usuario')

                ->paginate(15);

            }

            

            return response()->json($items);

        }

        

    }



    public function store(solicitudRequest $request)

    {

       $item = Cuenta::find($request->cuenta);

       $moneda = Moneda::find($request->moneda);



       if($request->tasa_cambio=='' || $request->tasa_cambio==null){

            if(Auth::user()->rol == "Operador"){
                $tasa=$moneda->tasa;
            }else{
                $tasa=$moneda->tasa;
            }
        
       }else{

           if(Auth::user()->rol == "Administrador"){
                $tasa1= str_replace(".", "", $request->tasa_cambio);
                $tasa2= str_replace(",", ".", $request->tasa_cambio);
                $tasa=number_format($tasa2, 2, '.', '');
            }else{
                $tasa=$moneda->tasa;
            }

       }

       $total= $request->quantidade *  $tasa;

       $solicitud=Solicitud::create([

            'codigo'      => $request->codigo,

            'monto'      => $request->quantidade,

            'taza'      => $tasa,

            'total'      => $total,

            'fecha'      => Carbon::now(),

            'prioriad'      => $request->prioridade,

            'banco'      => $item->banco,

            'banco_rpt'      => $request->banco_rpt, 

            'nro_comprobante_rpt'      => $request->nro_comprobante_rpt,

            'cliente'      => $request->cliente,

            'beneficiario'      => $request->beneficiario,

            'cuenta'      => $request->cuenta,

            'tipo_pago'      => $request->tipo_pagamento,

            'moneda'      => $request->moneda,

            'usuario'      => Auth::user()->id,

            'unidad'      => Auth::user()->unidad,

            'status'      => 1,

            'estado'      => 0,

        ]);



       if($request->descripcion==null || $request->descripcion==''){

            $descripcion="Pedido criado com sucesso";

       }else{

            $descripcion=$request->descripcion;

       }



       Observacion::create([

            'descripcion'    => $descripcion,

            'status'      => 1,

            'usuario'      => Auth::user()->id,

            'solicitud'      => $solicitud->id,

            'fecha'      => date('Y-m-d'),

            'hora'      => date('H:i:s')

        ]);





        return response()->json('Success');

    }



    public function observaciones(observacionRequest $request)

    {



       $item = Solicitud::find($request->id);

       Observacion::create([

            'descripcion'    => $request->value,

            'status'      => $item->status,

            'usuario'      => Auth::user()->id,

            'solicitud'      => $request->id,

            'fecha'      => date('Y-m-d'),

            'hora'      => date('H:i:s')

        ]);





        return response()->json('Success');

    }



    public function edit($id)

    {

        $item = Solicitud::where('id', $id)->with('cliente')->with(['beneficiario','beneficiario.cuenta'])->with('banco')->with('cuenta')->with('moneda')->first();

        return response()->json($item);

    }



    public function update(Request $request, $id)

    {

        $item = Solicitud::find($id);

        //$item->fill($request->all());

        $item->fill([

            'nro_comprobante'      => $request->referencia,
            'status'      => $request->statusf,
            'banco_emisor'      => $request->banco_emisor,
            'monto'     => $request->monto,
            'taza'      => $request->tasa,
            'cuenta'    => $request->cuenta,
            'banco'    => $request->id_banco,
            'total'    => $request->total2,

        ]);

        $item->save();


        if(empty($request->observacion)){
            if($request->statusf==2){ $descripcion="Em processo"; }
            if($request->statusf==3){ 
                $descripcion="Pago"; 
                if($request->statusf!=$item->status){
                     Caja::create([

                        'banco'    => $item->banco,

                        'monto'      => $item->total,

                        'codigo'    => $item->codigo,

                        'fecha'    => Carbon::now(),

                        'tipo_ope' =>2,

                        ]);
                }
            }
            if($request->statusf==4){ $descripcion="Rejeitado"; }
            if($request->statusf==5){ $descripcion="Reembolso"; }
            if($request->statusf==6){ $descripcion="Revisão"; }
        }else{
            $descripcion=$request->observacion;
        }

        if($request->statusf!=$item->status || !empty($request->observacion)){
             Observacion::create([

                'descripcion'    => $descripcion,

                'status'      => $request->statusf,

                'usuario'      => Auth::user()->id,

                'solicitud'      => $id,

                'fecha'      => date('Y-m-d'),

                'hora'      => date('H:i:s')

            ]);
        }
        
        if($request->id_banco!=$request->banco){
             Observacion::create([

                'descripcion'    => 'Mudanca na conta do beneficiario',
                
                'status'      => $request->statusf,

                'usuario'      => Auth::user()->id,

                'solicitud'      => $id,

                'fecha'      => date('Y-m-d'),

                'hora'      => date('H:i:s'),

            ]);
        }
        
        if($request->total2!=$request->total){
             Observacion::create([

                'descripcion'    => 'Mudanca em valor da transacao',
                
                'status'      => $request->statusf,

                'usuario'      => Auth::user()->id,

                'solicitud'      => $id,

                'fecha'      => date('Y-m-d'),

                'hora'      => date( 'H:i:s'),

            ]);
        }


        return response()->json($item);

    }





    public function destroy($id, $status, Request $request)

    {

        $item = Solicitud::find($id);

        $item->fill([

                'status' => $status

            ]);

        $item->save();



        if($status==2){

            $descripcion="Em processo";

        }

        if($status==3){

            $descripcion="Pago";

            Caja::create([

            'banco'    => $item->banco,

            'monto'      => $item->total,

            'codigo'    => $item->codigo,

            'fecha'    => Carbon::now(),

            'tipo_ope' =>2,

            ]);

        }

        if($status==4){

            $descripcion="Rejeitado";

        }

        if($status==5){

            $descripcion="Reembolso";

        }

        if($status==6){

            $descripcion="Revisão";

        }



        Observacion::create([

            'descripcion'    => $descripcion,

            'status'      => $status,

            'usuario'      => Auth::user()->id,

            'solicitud'      => $id,

            'fecha'      => date('Y-m-d'),

            'hora'      => date('H:i:s')

        ]);



        return response()->json('Success');

    }



    public function getUser()

    {

    	$items = User::where('id',Auth::user()->id)->get();

        return response()->json($items);

    }

     public function getUsuarios()

    {

        if(Auth::user()->rol == "Administrador"  || Auth::user()->rol == "Receptor"){
            $items = User::where('status',1)->get();
        }else{
            $items = User::where('id',Auth::user()->id)->get();
        }
        return response()->json($items);

    }

    public function getUnidades()

    {

        $items = Unidad::where('status',1)->get();

        return response()->json($items);

    }



    public function getPago()

    {

        $items = Tipo::where('status',1)->get();

        return response()->json($items);

    }



    public function getPais()

    {

        $items = Pais::where('status',1)->orderBy('descripcion', 'asc')->get();

        return response()->json($items);

    }



    public function getBanco()

    {

    	$items = Banco::where('status',1)->orderBy('nombre', 'asc')->get();

        return response()->json($items);

    }



    public function getBeneficiario()

    {

    	$items = Beneficiario::where('status',1)->orderBy('nombre', 'asc')->get();

        return response()->json($items);

    }



    public function getCliente()

    {

    	$items = Cliente::where('status',1)->orderBy('nombre', 'asc')->get();

        return response()->json($items);

    }



    public function getMoneda()

    {

    	$items = Moneda::where('status',1)->orderBy('moneda', 'asc')->get();

        return response()->json($items);

    }



    public function getCuenta($id)

    {

        $items = Cuenta::where('beneficiario', $id)->with('banco')->get();

        return response()->json($items);

    }



    public function __construct()

    {

        $this->middleware('auth');

    }



}


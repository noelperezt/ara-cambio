<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\unidadRequest;
use App\Unidad;

class unidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Unidad::orderBy('descripcion', 'asc')->paginate(15);
        return response()->json($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(unidadRequest $request)
    {
        Unidad::create([
            'descripcion' => $request->nome,
        ]);
        return response()->json('Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Unidad::find($id);
        return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Unidad::find($id);
        $item->fill([
            'descripcion'   => $request->nome,
            'status'        => $request->status
        ]);
        $item->save();

        return response()->json('Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Unidad::find($id);
        $item->destroy;

        return response()->json('Success');
    }

     public function __construct()
    {
        $this->middleware('auth');
    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\beneficiarioRequest;
use App\Http\Controllers\Controller;
use App\Beneficiario;

class beneficiarioController extends Controller
{
        public function index()
    {

        $items = Beneficiario::orderBy('nombre', 'asc')->with('pais')->paginate(15);
        return response()->json($items);
    }

    public function getPais()
    {
    	$items = Pais::where('status',1)->orderBy('descripcion', 'asc')->get();
        return response()->json($items);
    }

    public function filterData(Request $request)
    {
    	$items = Beneficiario::where('cpf', 'LIKE', '%'.$request->cpf.'%')
    	->where('nombre', 'LIKE', '%'.$request->nome_completo.'%')
    	->where('status', 'LIKE', '%'.$request->status.'%')
    	->with('pais')
    	->orderBy('nombre', 'asc')
    	->paginate(15);
    	return response()->json($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(beneficiarioRequest $request)
    {
       $moneda=Beneficiario::create([
            'codigo'      => $request->codigo,
            'nombre'      => $request->nome,
            'apellido'      => $request->ultimo_nome,
            'fecha_nac'      => $request->data_de_nascimento,
            'profesion'      => $request->profissao,
            'pais'      => $request->pais,
            'celular'      => $request->celular,
            'telefono'      => $request->telefone_de_localizacao,
            'email'      => $request->email,
            'cpf'      => $request->tipo.$request->cpf,
            'lugar'      => $request->endereco_residencial,
            'numero'      => $request->pais,
            'barrio'      => $request->bairrio,
            'ciudad'      => $request->cidades,
            'estado'      => $request->estado,
        ]);
        return response()->json($moneda);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Beneficiario::find($id);
        return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Beneficiario::find($id);
        $item->fill([
            'nombre'      => $request->nome,
            'apellido'      => $request->ultimo_nome,
            'fecha_nac'      => $request->data_de_nascimento,
            'profesion'      => $request->profissao,
            'pais'      => $request->pais,
            'celular'      => $request->celular,
            'telefono'      => $request->telefone_de_localizacao,
            'email'      => $request->email,
            'cpf'      => $request->cpf,
            'lugar'      => $request->endereco_residencial,
            'numero'      => $request->pais,
            'barrio'      => $request->bairrio,
            'ciudad'      => $request->cidades,
            'estado'      => $request->estado,
        ]);
        $item->save();
        

        return response()->json('Success');
    }

     public function getCedula($id)

    {

        $items = Beneficiario::where('cpf', $id)->get();

        return response()->json($items);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Beneficiario::find($id);
        if ($item->status) {
        	$item->fill([
	        	'status' => 0
	        ]);
        }else{
        	$item->fill([
	        	'status' => 1
	        ]);
        }
        $item->save();

        return response()->json('Success');
    }

     public function __construct()
    {
        $this->middleware('auth');
    }
    
}

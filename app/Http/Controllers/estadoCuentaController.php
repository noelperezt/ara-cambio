<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Http\Requests\solicitudRequest;

use App\Http\Requests\observacionRequest;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Response;

use App\Solicitud;

use App\Tipo;

use App\User;

use Carbon\Carbon;

use App\Unidad;


class estadoCuentaController extends Controller

{

   

   public function index()

    {

        
        if(Auth::user()->rol == "Administrador" || Auth::user()->rol == "Receptor"){

            $items = User::orderBy('nombre', 'asc')->with(['solicitud' => function ($query) { $query->where('fecha',date('Y-m-d')); }, 'solicitud.tipo_pago', 'solicitud.moneda'])->paginate(15);

        }else{

           $items = User::where('id', Auth::user()->id)->orderBy('nombre', 'asc')->with(['solicitud' => function ($query) { $query->where('fecha',date('Y-m-d')); }, 'solicitud.tipo_pago', 'solicitud.moneda'])->paginate(15);

        }

        return response()->json($items);

    }

    public function totalizar()

    {
 
        $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'), 'tipo_pago')->where('fecha', date('Y-m-d'))->where('status', '<>', 4)->groupBy('usuario', 'tipo_pago')->with('tipo_pago')->paginate(15);

        return response()->json($items);

    }


    public function sumar()

    {

        
        $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'))->where('fecha', date('Y-m-d'))->where('status', '<>', 4)->groupBy('usuario')->paginate(15);

        return response()->json($items);

    }

    public function filterData(Request $request)

    {

        if(Auth::user()->rol == "Administrador" || Auth::user()->rol == "Receptor"){

            if(($request->data_de_pedido) && ($request->data_de_pedido2)){
                
                $fecha_incio=$request->data_de_pedido;
                $fecha_fin=$request->data_de_pedido2;
                $statusf=$request->statusf;
                $tipo_pago=$request->tipo_pagamento;

                 $items = User::orderBy('nombre', 'asc')
                 ->where('id','LIKE',$request->usuario)
                 ->with(['solicitud' => function ($query) use ($fecha_incio, $fecha_fin, $tipo_pago, $statusf) { $query->whereBetween('fecha',[ $fecha_incio, $fecha_fin])->where('status', 'LIKE', $statusf)
                    ->where('tipo_pago', 'LIKE', $tipo_pago); }, 
                    'solicitud.tipo_pago', 'solicitud.moneda'])
                 ->paginate(15);

            }

            if(!($request->data_de_pedido) && ($request->data_de_pedido2)){

                $fecha_incio=$request->data_de_pedido;
                $fecha_fin=$request->data_de_pedido2;
                $statusf=$request->statusf;
                $tipo_pago=$request->tipo_pagamento;

                 $items = User::orderBy('nombre', 'asc')
                 ->where('id','LIKE',$request->usuario)
                 ->with(['solicitud' => function ($query) use ($fecha_incio, $fecha_fin, $tipo_pago, $statusf) { $query->whereBetween('fecha',[ '1900-01-01', $fecha_fin])->where('status', 'LIKE', $statusf)
                    ->where('tipo_pago', 'LIKE', $tipo_pago); }, 
                    'solicitud.tipo_pago', 'solicitud.moneda'])
                 ->paginate(15);

            }

            if(($request->data_de_pedido) && !($request->data_de_pedido2)){

                $fecha_incio=$request->data_de_pedido;
                $fecha_fin=$request->data_de_pedido2;
                $statusf=$request->statusf;
                $tipo_pago=$request->tipo_pagamento;

                 $items = User::orderBy('nombre', 'asc')
                 ->where('id','LIKE',$request->usuario)
                 ->with(['solicitud' => function ($query) use ($fecha_incio, $fecha_fin, $tipo_pago, $statusf) { $query->whereBetween('fecha',[ $fecha_incio, '2500-12-31'])->where('status', 'LIKE', $statusf)
                    ->where('tipo_pago', 'LIKE', $tipo_pago); }, 
                    'solicitud.tipo_pago', 'solicitud.moneda'])
                 ->paginate(15);

            }

            if(!($request->data_de_pedido) && !($request->data_de_pedido2)){

                $statusf=$request->statusf;
                $tipo_pago=$request->tipo_pagamento;

                 $items = User::orderBy('nombre', 'asc')
                 ->where('id','LIKE',$request->usuario)
                 ->with(['solicitud' => function ($query) use ($tipo_pago, $statusf) { $query->where('status', 'LIKE', $statusf)
                    ->where('tipo_pago', 'LIKE', $tipo_pago); }, 
                    'solicitud.tipo_pago', 'solicitud.moneda'])
                 ->paginate(15);

            }

            

            return response()->json($items);

        }else{

            if(($request->data_de_pedido) && ($request->data_de_pedido2)){
                
                $fecha_incio=$request->data_de_pedido;
                $fecha_fin=$request->data_de_pedido2;
                $statusf=$request->statusf;
                $tipo_pago=$request->tipo_pagamento;

                 $items = User::where('id', Auth::user()->id)->orderBy('nombre', 'asc')
                 ->with(['solicitud' => function ($query) use ($fecha_incio, $fecha_fin, $tipo_pago, $statusf) { $query->whereBetween('fecha',[ $fecha_incio, $fecha_fin])->where('status', 'LIKE', $statusf)
                    ->where('tipo_pago', 'LIKE', $tipo_pago); }, 
                    'solicitud.tipo_pago', 'solicitud.moneda'])
                 ->paginate(15);

            }

            if(!($request->data_de_pedido) && ($request->data_de_pedido2)){

                $fecha_incio=$request->data_de_pedido;
                $fecha_fin=$request->data_de_pedido2;
                $statusf=$request->statusf;
                $tipo_pago=$request->tipo_pagamento;

                 $items = User::where('id', Auth::user()->id)->orderBy('nombre', 'asc')
                 ->where('id','LIKE',$request->usuario)
                 ->with(['solicitud' => function ($query) use ($fecha_incio, $fecha_fin, $tipo_pago, $statusf) { $query->whereBetween('fecha',[ '1900-01-01', $fecha_fin])->where('status', 'LIKE', $statusf)
                    ->where('tipo_pago', 'LIKE', $tipo_pago); }, 
                    'solicitud.tipo_pago', 'solicitud.moneda'])
                 ->paginate(15);

            }

            if(($request->data_de_pedido) && !($request->data_de_pedido2)){

                $fecha_incio=$request->data_de_pedido;
                $fecha_fin=$request->data_de_pedido2;
                $statusf=$request->statusf;
                $tipo_pago=$request->tipo_pago;

                 $items = User::where('id', Auth::user()->id)->orderBy('nombre', 'asc')
                 ->where('id','LIKE',$request->usuario)
                 ->with(['solicitud' => function ($query) use ($fecha_incio, $fecha_fin, $tipo_pago, $statusf) { $query->whereBetween('fecha',[ $fecha_incio, '2500-12-31'])->where('status', 'LIKE', $statusf)
                    ->where('tipo_pago', 'LIKE', $tipo_pago); }, 
                    'solicitud.tipo_pago', 'solicitud.moneda'])
                 ->paginate(15);

            }

            if(!($request->data_de_pedido) && !($request->data_de_pedido2)){

                $statusf=$request->statusf;
                $tipo_pago=$request->tipo_pagamento;

                 $items = User::where('id', Auth::user()->id)->orderBy('nombre', 'asc')
                ->where('id','LIKE',$request->usuario)
                 ->with(['solicitud' => function ($query) use ($tipo_pago, $statusf) { $query->where('status', 'LIKE', $statusf)
                    ->where('tipo_pago', 'LIKE', $tipo_pago); }, 
                    'solicitud.tipo_pago', 'solicitud.moneda'])
                 ->paginate(15);

            }

            

            return response()->json($items);

        }

        

    }

    public function filterDataTotalizar(Request $request)

    {

            if(($request->data_de_pedido) && ($request->data_de_pedido2)){
                

                $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'), 'tipo_pago')
                ->whereBetween('fecha',[ $request->data_de_pedido, $request->data_de_pedido2])
                ->where('status', 'LIKE', '%'.$request->statusf.'%')
                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')
                ->where('usuario', 'LIKE', $request->usuario)
                ->groupBy('usuario', 'tipo_pago')->with('tipo_pago')->paginate(15);

            }

            if(!($request->data_de_pedido) && ($request->data_de_pedido2)){

                $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'), 'tipo_pago')
                ->whereBetween('fecha',[ '1900-01-01', $request->data_de_pedido2])
                ->where('status', 'LIKE', '%'.$request->statusf.'%')
                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')
                ->where('usuario', 'LIKE', $request->usuario)
                ->groupBy('usuario', 'tipo_pago')->with('tipo_pago')->paginate(15);

            }

            if(($request->data_de_pedido) && !($request->data_de_pedido2)){

                $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'), 'tipo_pago')
                ->whereBetween('fecha',[ $request->data_de_pedido, '2500-12-31'])
                ->where('status', 'LIKE', '%'.$request->statusf.'%')
                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')
                ->where('usuario', 'LIKE', $request->usuario)
                ->groupBy('usuario', 'tipo_pago')->with('tipo_pago')->paginate(15);

            }

            if(!($request->data_de_pedido) && !($request->data_de_pedido2)){

                $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'), 'tipo_pago')
                ->where('status', 'LIKE', '%'.$request->statusf.'%')
                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')
                ->where('usuario', 'LIKE', $request->usuario)
                ->groupBy('usuario', 'tipo_pago')->with('tipo_pago')->paginate(15);

            }

            

            return response()->json($items);
        

    }

    public function filterDataSumar(Request $request)

    {

            if(($request->data_de_pedido) && ($request->data_de_pedido2)){
                

                $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'))
                ->whereBetween('fecha',[ $request->data_de_pedido, $request->data_de_pedido2])
                ->where('status', 'LIKE', '%'.$request->statusf.'%')
                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')
                ->where('usuario', 'LIKE', $request->usuario)
                ->groupBy('usuario')->paginate(15);

            }

            if(!($request->data_de_pedido) && ($request->data_de_pedido2)){

               $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'))
                ->whereBetween('fecha',[ '1900-01-01', $request->data_de_pedido2])
                ->where('status', 'LIKE', '%'.$request->statusf.'%')
                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')
                ->where('usuario', 'LIKE', $request->usuario)
                ->groupBy('usuario')->paginate(15);

            }

            if(($request->data_de_pedido) && !($request->data_de_pedido2)){

                $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'))
                ->whereBetween('fecha',[ $request->data_de_pedido, '2500-12-31'])
                ->where('status', 'LIKE', '%'.$request->statusf.'%')
                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')
                ->where('usuario', 'LIKE', $request->usuario)
                ->groupBy('usuario')->paginate(15);

            }

            if(!($request->data_de_pedido) && !($request->data_de_pedido2)){

                $items = Solicitud::select('usuario', DB::raw('SUM(monto) as monto_reales'), DB::raw('SUM(total) as total_bolivares'))
                ->where('status', 'LIKE', '%'.$request->statusf.'%')
                ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')
                ->where('usuario', 'LIKE', $request->usuario)
                ->groupBy('usuario')->paginate(15);

            }

            

            return response()->json($items);
        

    }


     public function getUsuarios()

    {

        if(Auth::user()->rol == "Administrador"  || Auth::user()->rol == "Receptor"){
            $items = User::where('status',1)->get();
        }else{
            $items = User::where('id',Auth::user()->id)->get();
        }
        return response()->json($items);

    }

    public function getUnidades()

    {

        $items = Unidad::where('status',1)->get();

        return response()->json($items);

    }



    public function getPago()

    {

        $items = Tipo::where('status',1)->get();

        return response()->json($items);

    }

    public function __construct()

    {

        $this->middleware('auth');

    }



}


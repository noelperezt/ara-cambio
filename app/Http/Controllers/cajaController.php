<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\cajaRequest;
use App\Http\Controllers\Controller;
use App\Caja;
use App\Banco;
use App\Pais;
use App\Solicitud;
use App\cajaHistorico;
use Carbon\Carbon;
class cajaController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
       // $items = Banco::orderBy('created_at', 'desc')->where('status', 1)->with(['caixa','caixa.banco.pais','caixa.banco.pais.monedas','pais.monedas'])->paginate(15);
        $items = Caja::orderBy('created_at', 'desc')->with('banco')->with(['historico','historico.caja','historico.caja.banco','historico.usuario'])->paginate(15);
        return response()->json($items);

    }

    public function getBanco($id)
    {
    	$items = Banco::orderBy('nombre', 'asc')->where('status', 1)->where('pais', $id)->get();
        return response()->json($items);
    }
    public function getPais()
    {
    	$items = Pais::orderBy('descripcion', 'asc')->where('status', 1)->get();
        return response()->json($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function deposito(Request $request){
        $item = Caja::find($request->id);
        
        $valor=1;
        $monto= (float)$item->monto + (float)$request->value;
        $transacciones=(int)$item->transacciones + (int)$valor;

        $item->fill([
            'monto' => $monto,
            'transacciones' => $transacciones,
        ]);
        $item->save();

        cajaHistorico::create([
            'caja'    => $request->id,
            'monto'      => (double)$request->value,
            'usuario'    => Auth::user()->id,
        ]);



        return response()->json('Success');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(cajaRequest $request)
    {
       $caja= Caja::create([
            'banco'    => $request->banco,
            'monto'      => $request->montante,
            'codigo'      => $request->codigo,
            'fecha'    => Carbon::now(),
            'fecha2'    => Carbon::now(),
            'tipo_ope'  => 0
        ]);
       
       cajaHistorico::create([
            'caja'    => $caja->id,
            'monto'      => $request->montante,
            'usuario'      => Auth::user()->id,
        ]);


        return response()->json('Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Caja::find($id);
        return response()->json($item);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Caja::find($id);
        $item->fill([

            'monto'      => $request->value,
        ]);
        $item->save();

        return response()->json('Success');
    }

    public function getSolicitudes()
    {
        $items = Solicitud::where('status', 3)->sum('total');
        return response()->json($items);
    }

    public function getCaja()
    {
        $items = Caja::sum('monto');
        return response()->json($items);
    }

    public function filterData(Request $request)
    {
        if(!($request->data) && ($request->data2)){
            $items = \App\Caja::where('fecha','<=' ,$request->data2)
            ->with('banco')
            ->orderBy('banco', 'asc')
            ->paginate(15);
        }
        if(($request->data) && !($request->data2)){
            $items = \App\Caja::where('fecha','>=' ,$request->data)
            ->with('banco')
            ->orderBy('banco', 'asc')
            ->paginate(15);
        }
        if(($request->data) && ($request->data2)){
            $items = \App\Caja::whereBetween('fecha',[$request->data, $request->data2])
            ->with('banco')
            ->orderBy('banco', 'asc')
            ->paginate(15);
        }
        if(!($request->data) && !($request->data2)){
            $items = \App\Caja::with('banco')
            ->orderBy('banco', 'asc')
            ->paginate(15);
        }
        
        return response()->json($items);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Caja::find($id);
        $item->delete;

        return response()->json('Success');
    }

     public function __construct()
    {
        $this->middleware('auth');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\paisRequest;
use App\Historico;


class headController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $registros = Historico::select('monto')->orderBy('fecha','desc')->limit(1)->get(1);       
        return json_encode($registros);
    }


}

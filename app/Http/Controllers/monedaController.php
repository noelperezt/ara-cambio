<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\monedaRequest;
use App\Pais;
use App\Moneda;
use App\Historico;

class monedaController extends Controller
{
    public function index()
    {
        $items = Moneda::orderBy('moneda', 'asc')->with('pais')->paginate(15);
        return response()->json($items);
    }

    public function getPais()
    {
    	$items = Pais::where('status',1)->orderBy('descripcion', 'asc')->get();
        return response()->json($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(monedaRequest $request)
    {
       $moneda=Moneda::create([
            'pais'      => $request->pais,
            'moneda'    => $request->moeda,
            'tasa'      => $request->copo
        ]);
        Historico::create([
            'moneda'    => $moneda->id,
            'monto'      => $request->copo,
            'usuario'      => Auth::user()->id,
            'fecha'      => date('Y-m-d'),
            'hora'      => date('H:i:s')
        ]);
        return response()->json('Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Moneda::find($id);
        return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Moneda::find($id);
        $item->fill([
            'pais'      => $request->pais,
            'moneda'    => $request->moeda,
            'tasa'      => $request->copo,
            'status'    => $request->status
        ]);
        $item->save();
        Historico::create([
            'moneda'    => $id,
            'monto'      => $request->copo,
            'usuario'      => Auth::user()->id,
            'fecha'      => date('Y-m-d'),
            'hora'      => date('H:i:s')
        ]);
        

        return response()->json('Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Moneda::find($id);
        $item->destroy;

        return response()->json('Success');
    }
    
     public function __construct()
    {
        $this->middleware('auth');
    }
    
}

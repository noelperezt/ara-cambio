<?php

namespace App\Http\Controllers;
use App\Historico;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{
     public function do()
    {
        
       return view('login');

    }

    public function login()
    {
        $registros = Historico::select('monto')->orderBy('fecha','desc')->limit(1)->get();
        
        $json_response = json_encode($registros);
        
        $value = json_decode($json_response,TRUE);
        $valueGet=  $value[0]['monto'] ;
        
        session(['taxa' => $valueGet]);
        
        
       
       
        // Guardamos en un arreglo los datos del usuario.
        $userdata = array(
            'username' => Input::get('username'),
            'password'=> Input::get('password'),
            'status'=>1
        );
        


        
        // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.
       if(Auth::attempt($userdata, Input::get('remember-me', 0)))
        {
            // De ser datos válidos nos mandara a la bienvenida
            return Redirect::to('/');
        }
        // En caso de que la autenticación haya fallado manda un mensaje al formulario de login y también regresamos los valores enviados con withInput().
        Session::flash('message','red, Os dados inseridos estão incorretos.');
        return Redirect::to('login');

       
        
    }
    
    public function logout()
    {
        Auth::logout();
        Session::flash('message','green, Sua sessão foi fechada.');
        return Redirect::to('login');
    }

}

<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\Solicitud;



class observacionController extends Controller

{

   

   public function index()

    {

        $items = Solicitud::orderBy('created_at', 'desc')->with('cliente')->with('beneficiario')->with('banco')->with(['comentario','comentario.usuario'])->paginate(15);

        return response()->json($items);

    }



    public function filterData(Request $request)

    {

        $items = \App\Solicitud::where('fecha', 'LIKE', '%'.$request->data_de_pedido.'%')

        ->where('prioriad', 'LIKE', '%'.$request->prioridad.'%')

        ->where('cliente', 'LIKE', '%'.$request->cliente.'%')

        ->where('tipo_pago', 'LIKE', '%'.$request->tipo_pagamento.'%')

        ->where('banco', 'LIKE', '%'.$request->banco.'%')

        ->where('status', 'LIKE', '%'.$request->statusf.'%')

        ->with('cliente')->with('beneficiario')->with('banco')

        ->paginate(15);

        return response()->json($items);

    }



    public function __construct()

    {

        $this->middleware('auth');

    }



}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observacion extends Model
{
    protected $table = "observaciones";
    protected $fillable = [
    	'descripcion',
    	'usuario',
    	'solicitud',
        'fecha',
        'hora',
        'status'
    ];
    public function usuario(){
    	return $this->belongsTo('App\User', 'usuario');
    }
    public function solicitud(){
    	return $this->belongsTo('App\Solicitud', 'solicitud');
    }
}

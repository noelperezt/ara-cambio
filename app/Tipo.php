<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $table = "tipo_pago";
    protected $fillable = [
    	'descripcion',
    	'status'
    ];

    public function solicitud()
    {
        return $this->hasMany('App\Solicitud');
    }
}

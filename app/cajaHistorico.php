<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cajaHistorico extends Model
{
    protected $table = "caja_historial";
    protected $fillable = [
    	'caja',
        'monto',
    	'usuario',
    ];
    public function caja(){
    	return $this->belongsTo('App\Caja', 'caja');
    }
    public function usuario(){
    	return $this->belongsTo('App\User', 'usuario');
    }
 }

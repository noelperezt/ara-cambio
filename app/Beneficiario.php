<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class Beneficiario extends Model

{

    protected $table = "beneficiarios";

    protected $fillable = [

    	'codigo',

    	'tipo',

    	'documento',

    	'cpf',

    	'nombre',

    	'apellido',

    	'fecha_nac',

    	'profesion',

    	'pais',

    	'celular',

    	'telefono',

    	'email',

    	'status'

    ];

    public function solicitud()

    {

        return $this->hasMany('App\Solicitud');

    }

    public function cuenta()

    {

        return $this->hasMany('App\Cuenta' , 'beneficiario');

    }

    public function pais()

    {

        return $this->belongsTo('App\Pais', 'pais', 'id');

    }

}


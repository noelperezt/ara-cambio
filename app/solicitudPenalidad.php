<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class solicitudPenalidad extends Model
{
    protected $table = "solicitud_penalidad";
    protected $fillable = [
    	'penalidad',
    	'solicitud',
    ];
    public function solicitud(){
    	return $this->belongsTo('App\Solicitud', 'solicitud', 'id');
    }
    public function penalidad(){
    	return $this->belongsTo('App\Penalidad', 'penalidad', 'id');
    }
}

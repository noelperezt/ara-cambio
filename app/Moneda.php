<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
    protected $table = "monedas";
    protected $fillable = [
    	'pais',
    	'moneda',
    	'tasa',
    	'status',
    ];
    public function pais()
    {
    	return $this->belongsTo('App\Pais', 'pais', 'id');
    }

    public function historico()
    {
        return $this->hasMany('App\Historico', 'moneda');
    }

    public function solicitud()
    {
        return $this->hasMany('App\Solicitud');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    protected $table = "cuentas";
    protected $fillable = [
    	'beneficiario',
    	'banco',
    	'tipo_cuenta',
    	'cuenta',
    	'status',
    ];
    public function banco()
    {
    	return $this->belongsTo('App\Banco', 'banco', 'id');
    }
    public function beneficiario()
    {
        return $this->belongsTo('App\Beneficiario', 'beneficiario', 'id');
    }
    
    public function solicitud()
    {
        return $this->hasMany('App\Solicitud');
    }
}

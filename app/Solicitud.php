<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class Solicitud extends Model

{

    protected $table = "solicitudes";

    protected $fillable = [

    	'codigo',

    	'monto',

    	'taza',

    	'total',

    	'fecha',

    	'estado',

    	'prioriad',

    	'nro_comprobante',

    	'url_img_comprobante',

    	'nro_comprobante_rpt',

    	'url_img_comprobante_rpt',

    	'banco',

    	'banco_rpt',

    	'cliente',

    	'beneficiario',

    	'cuenta',

    	'tipo_pago',

    	'moneda',

    	'usuario',

        'unidad',

        'banco_emisor',

        'nro_comprobante_emisor',

    	'status'

    ];

    public function banco(){

    	return $this->belongsTo('App\Banco', 'banco', 'id');

    }

    public function banco_rpt(){

    	return $this->belongsTo('App\Banco', 'banco_rpt', 'id');

    }

    public function cliente(){

    	return $this->belongsTo('App\Cliente', 'cliente', 'id');

    }

    public function beneficiario(){

    	return $this->belongsTo('App\Beneficiario', 'beneficiario', 'id');

    }

    public function cuenta(){

    	return $this->belongsTo('App\Cuenta', 'cuenta', 'id');

    }

    public function tipo_pago(){

    	return $this->belongsTo('App\Tipo', 'tipo_pago', 'id');

    }

    public function moneda(){

    	return $this->belongsTo('App\Moneda', 'moneda', 'id');

    }

    public function usuario(){

    	return $this->belongsTo('App\User', 'usuario', 'id');

    }



    public function cuentas()

    {

        return $this->hasMany('App\Cuenta');

    }

    

    public function comentario()

    {

        return $this->hasMany('App\Observacion', 'solicitud');

    }

    public function sol_Penalidad()

    {

        return $this->hasMany('App\SolicitudPenalidad');

    }

}


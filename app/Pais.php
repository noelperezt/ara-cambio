<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = "pais";
    protected $fillable = [
    	'descripcion',
    	'status',
    ];

    public function banco ()
    {
    	return $this->hasMany('App\Banco');
    }
    public function monedas()
    {
    	return $this->hasOne('App\Moneda','pais');
    }
}

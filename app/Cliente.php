<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    protected $table = "clientes";
    protected $fillable = [
    	'codigo',
    	'tipo',
    	'nombre',
    	'apellido',
    	'fecha_nac',
    	'pais',
    	'celular',
    	'telefono',
    	'email',
    	'cep',
    	'lugar',
    	'numero',
    	'barrio',
    	'ciudad',
    	'estado',
        'profesion',
    	'observacion',
    	'status',
    ];
    public function pais()
    {
        return $this->belongsTo('App\Pais', 'pais', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    protected $table = "caja";
    protected $fillable = [
    	'monto',
    	'fecha',
    	'banco',
        'codigo',
        'tipo_ope',
    ];

    public function banco()
    {
    	return $this->belongsTo('App\Banco', 'banco', 'id');
    }

    public function historico()
    {
        return $this->hasMany('App\cajaHistorico', 'caja');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    protected $table = "unidades";
    protected $fillable = [
    	'descripcion',
    	'status',
    ];
    public function usuario()
    {
        return $this->hasMany('App\User');
    }
}

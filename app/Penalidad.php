<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penalidad extends Model
{
    protected $table = "penalidad";
    protected $fillable = [
    	'descripcion',
    	'monto',
    	'status',
    ];
    public function sol_Penalidad()
    {
        return $this->hasMany('App\SolicitudPenalidad');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    protected $table = "historicos";
    protected $fillable = [
    	'fecha',
        'hora',
    	'monto',
    	'usuario',
    	'moneda',
    ];
    public function usuario(){
    	return $this->belongsTo('App\User', 'usuario');
    }
    public function moneda(){
    	return $this->belongsTo('App\Moneda', 'moneda');
    }
 }

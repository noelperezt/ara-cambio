<?php



namespace App;



use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;



class User extends Authenticatable

{

    use Notifiable;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'nombre', 'apellido', 'username', 'password', 'rol', 'unidad', 'status'

    ];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'password', 'remember_token',

    ];



    public function unidad(){

        return $this->belongsTo('App\Unidad', 'unidad', 'id');

    }



    public function solicitud()

    {

        return $this->hasMany('App\Solicitud', 'usuario');

    }

    public function historico()

    {

        return $this->hasMany('App\Historico');

    }

    public function observacion()

    {

        return $this->hasMany('App\Observacion');

    }

}


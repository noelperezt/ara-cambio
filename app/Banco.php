<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    protected $table = "banco";
    protected $fillable = [
    	'nombre',
    	'codigo',
        'pais',
        'caja',
        'caja_rpt',
        'status'
    ];
    public function pais()
    {
    	return $this->belongsTo('App\Pais', 'pais', 'id');
    }
    public function cuentas()
    {
    	return $this->hasMany('App\Cuenta');
    }
    public function solicitud()
    {
        return $this->hasMany('App\Solicitud');
    }
    public function solicitud_rpt()
    {
        return $this->hasMany('App\Solicitud');
    }
    public function caixa()
    {
        return $this->hasMany('App\Caja', 'banco');
    }
}

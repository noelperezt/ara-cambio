Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

var app = new Vue({
    el: '#app',
    data: {
    	items: [],
    	paises: [],
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    formErrorsUpdate:{},
	    newItem: {'pais':'','moeda':'','copo':''},
	    fillItem: {'pais':'','moeda':'','copo':'','status':''}
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('/aracambio/moneda/index?page='+page).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
			this.$http.get('/aracambio/moneda/getpais').then((response) => {
				this.$set('paises', response.data);
			});
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.getVueItems(page);
		},
		createItem: function(){
			var input = this.newItem;
			this.$http.post('/aracambio/moneda/store', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newItem = {'pais':'','moeda':'','copo':''};
				this.formErrors = {};
			}, response => {
				this.$set('formErrors', response.body);
			});
		},
		editItem: function(id){
			this.$http.get('/aracambio/moneda/'+id+'/edit/').then((response) => {
				this.$set('fillItem.id', response.data.id);
				this.$set('fillItem.pais', response.data.pais);
				this.$set('fillItem.moeda', response.data.moneda);
				this.$set('fillItem.copo', response.data.tasa);
				this.$set('fillItem.status', response.data.status);
				$('#editItem').modal('show');
			});
		},
		updateItem: function(id){
			var input = this.fillItem;
			this.$http.post('/aracambio/moneda/update/'+id, input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillItem = {'pais':'','moeda':'','copo':''};
				$('#editItem').modal('hide');
			});
		},
		destroyItem: function(id){
			alert('Función no establecida por el momento')
			/*this.$http.get('/item/destroy/'+id).then((response) => {
				this.changePage(this.pagination.current_page);
			});*/
		},
		filterItem: function() {
			var input = this.filter;
			this.$http.post('/articulo/filterdata/', input).then((response) => {
				this.$set('articulos', response.data);
			});
		},
		format: function (amount, decimals) {
			amount += ''; // por si pasan un numero en vez de un string
		    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

		    decimals = decimals || 0; // por si la variable no fue fue pasada

		    // si no es un numero o es igual a cero retorno el mismo cero
		    if (isNaN(amount) || amount === 0) 
		        return parseFloat(0).toFixed(decimals);

		    // si es mayor o menor que cero retorno el valor formateado como numero
		    amount = '' + amount.toFixed(decimals);

		    var amount_parts = amount.split('.'),
		        regexp = /(\d+)(\d{3})/;

		    while (regexp.test(amount_parts[0]))
		        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

		    return amount_parts.join('.');
		},
 	}
});
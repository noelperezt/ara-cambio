Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");
Vue.directive('selecttwo', {
  twoWay: true,
  bind: function () {
    $(this.el).select2()
    .on("select2:select", function(e) {
      this.set($(this.el).val());
    }.bind(this));
  },
  update: function(nv, ov) {
    $(this.el).trigger("change");
  }
});
var app = new Vue({
    el: '#app',
    data: {
    	items: [],
    	bancos: [],
    	beneficiario: [],
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    formErrorsUpdate:{},
	    newItem: {'beneficario':'','banco':'','tipo_da_conta':'', 'numero_da_conta':''},
	    fillItem: {'beneficario':'','banco':'','tipo_da_conta':'', 'numero_da_conta':'','status':''},

	    filter: {
	       	'beneficiario': '',
	    	'banco':'',
	    	'tipo_da_conta': '',
	    }
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('/aracambio/cuenta/index?page='+page).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
			this.$http.get('/aracambio/cuenta/getbanco').then((response) => {
				this.$set('bancos', response.data);
			});
			this.$http.get('/aracambio/cuenta/getbeneficiario').then((response) => {
				this.$set('beneficiario', response.data);
			});
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.getVueItems(page);
		},
		createItem: function(){
			var input = this.newItem;
			this.$http.post('/aracambio/cuenta/store', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newItem = {'beneficario':'','banco':'','tipo_da_conta':'','numero_da_conta':''};
				this.formErrors = {};
			}, response => {
				this.$set('formErrors', response.body);

				this.formErrors = {};
				$('#agregar').modal('hide');
			
			},
			response => {
				this.$set('formErrors', response.body);
			});
		},
		editItem: function(id){
			this.$http.get('/aracambio/cuenta/'+id+'/edit/').then((response) => {
				this.$set('fillItem.id', response.data.id);
				this.$set('fillItem.beneficario', response.data.beneficiario);
				this.$set('fillItem.banco', response.data.banco);
				this.$set('fillItem.tipo_da_conta', response.data.tipo_cuenta);
				this.$set('fillItem.numero_da_conta', response.data.cuenta);
				$('#editItem').modal('show');
			});
		},
		updateItem: function(id){
			var input = this.fillItem;
			this.$http.post('/aracambio/cuenta/update/'+id, input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillItem = {'beneficario':'','banco':'','tipo_da_conta':'','numero_da_conta':''};
				$('#editItem').modal('hide');
			});
		},
		destroyItem: function(id){
			this.$http.get('/aracambio/cuenta/destroy/'+id).then((response) => {
				this.changePage(this.pagination.current_page);
			});
		},
		filterItem: function(page) {
			var input = this.filter;
			this.$http.post('/aracambio/cuenta/filter', input).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
		},
 	}
});
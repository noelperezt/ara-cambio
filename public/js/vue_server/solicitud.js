Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");
Vue.directive('selecttwo', {
  twoWay: true,
  bind: function () {
    $(this.el).select2()
    .on("select2:select", function(e) {
      this.set($(this.el).val());
    }.bind(this));
  },
  update: function(nv, ov) {
    $(this.el).trigger("change");
  }
});
Vue.directive('cuenta', {
  update: function(nv, ov) {
  	console.log("Entro");
  	$(this.getcuenta).trigger("change");
  },
});
var app = new Vue({
    el: '#app',
    data: {
    	items: [],
    	cuentas: [],
    	reports: {},
    	observacion: {
    		'id': '',
    		'value': '',
    	},
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    formErrorsUpdate:{},
	    newItem: {
	    	"codigo": "",
		    "data_de_pedido": "",
			"prioridade": "",
			"quantidade": "",
			"tasa_cambio": "",
			"total_cambio": "",
			"moneda": "",
			"cliente": "",
			"beneficiario": "",
			"banco": "",
			"cuenta": "",
			"tipo_pagamento": "",
			"descripcion": "",
			"usuario": ""
		},
	    fillItem: {
		    "codigo": "",
		    "data_de_pedido": "",
			"prioridade": "",
			"quantidade": "",
			"tasa_cambio": "",
			"total_cambio": "",
			"moneda": "",
			"cliente": "",
			"beneficiario": "",
			"banco": "",
			"cuenta": "",
			"tipo_pagamento": "",
			"usuario": ""
	    },
	    filter: {
	    	'data_de_pedido':'',
	    	'data_de_pedido2':'',
	    	'prioridad': '',
	    	'cliente':'',
	    	'tipo_pagamento': '',
	    	'banco': '',
	    	'status': ''
	    }
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('/aracambio/solicitud/index?page='+page).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);

				var f;

				for (var i in this.items) {
					f=(i.fecha).split('-');
					i.fecha=f[2]+'/'+f[1]+'/'+f[0];
				}
			});
			this.$http.get('/aracambio/solicitud/getpago').then((response) => {
				this.$set('pagos', response.data);
			});
			this.$http.get('/aracambio/solicitud/getuser').then((response) => {
				this.$set('users', response.data);
			});
			this.$http.get('/aracambio/solicitud/getpais').then((response) => {
				this.$set('paises', response.data);
			});
			this.$http.get('/aracambio/solicitud/getbanco').then((response) => {
				this.$set('bancos', response.data);
			});
			this.$http.get('/aracambio/solicitud/getbeneficiario').then((response) => {
				this.$set('beneficiarios', response.data);
			});
			this.$http.get('/aracambio/solicitud/getcliente').then((response) => {
				this.$set('clientes', response.data);
			});
			this.$http.get('/aracambio/solicitud/getmoneda').then((response) => {
				this.$set('monedas', response.data);
			});
		},
		getCodeItem: function(){
			var token = "";
			var possible = "0123456789";
			for (var i = 0; i < 8; i++) {
				token += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			this.newItem.codigo = token;
			$('#agregar').modal('show');
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.filterItem(page);
		},
		createItem: function(){
			var input = this.newItem;
			this.$http.post('/aracambio/solicitud/store', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newItem = {
					"codigo": "",
				    "data_de_pedido": "",
					"prioridade": "",
					"quantidade": "",
					"tasa_cambio": "",
					"total_cambio": "",
					"moneda": "",
					"cliente": "",
					"beneficiario": "",
					"banco": "",
					"cuenta": "",
					"tipo_pagamento": "",
					"usuario": ""
				};
				this.formErrors = {};
				$('#agregar').modal('hide');
				$(".nuevocliente").css( "display", "block" );
      			$(".cliente").css( "display", "none" );
      			$(".nuevobeneficiario").css( "display", "block" );
       			$(".beneficiario").css( "display", "none" );
       			$("#cep_cliente").val("");
       			$('#cep_cliente').prop('readonly', false);
       			$("#cep_beneficiario").val("");
       			$('#cep_beneficiario').prop('readonly', false);
       			$("#total_cambio").val("");
    			$("#tasa_cambio").val("");
			
			},
			response => {
				this.$set('formErrors', response.body);
			});
		},
		editItem: function(id){
			this.$http.get('/aracambio/solicitud/'+id+'/edit').then((response) => {
				this.$set('fillItem.id', response.data.id);
				this.$set('fillItem.fecha', response.data.fecha);
				this.$set('fillItem.nombre_cliente', response.data.cliente.nombre);
				this.$set('fillItem.apellido_cliente', response.data.cliente.apellido);
				this.$set('fillItem.identidad_cliente', response.data.cliente.cep);
				this.$set('fillItem.telefono_cliente', response.data.cliente.telefono);
				this.$set('fillItem.nombre_beneficiario', response.data.beneficiario.nombre);
				this.$set('fillItem.apellido_beneficiario', response.data.beneficiario.apellido);
				this.$set('fillItem.identidad_beneficiario', response.data.beneficiario.cpf);
				this.$set('fillItem.telefono_beneficiario', response.data.beneficiario.telefono);
				this.$set('fillItem.banco', response.data.banco.id);
				this.$set('fillItem.cuenta', response.data.cuenta.cuenta);
				this.$set('fillItem.tipo_cuenta', response.data.cuenta.tipo_cuenta);
				this.$set('fillItem.moneda', response.data.moneda.moneda);
				this.$set('fillItem.monto', response.data.monto);
				this.$set('fillItem.tasa', response.data.taza);
				this.$set('fillItem.referencia', response.data.nro_comprobante);
				this.$set('fillItem.total', response.data.total);
				this.$set('fillItem.nro_comprobante_rpt', response.data.nro_comprobante_rpt);
				this.$set('fillItem.banco_rpt', response.data.banco_rpt);
				$('#editItem').modal('show');
			});
		},
		updateItem: function(id){
			var input = this.fillItem;
			this.$http.post('/aracambio/solicitud/update/'+id, input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillItem = {'nome':'','status':''};
				$('#editItem').modal('hide');
			});
		},
		describir: function (id) {
			this.observacion.id = id;
			$('#observacion').modal('show');
		},
		observacoes: function(){
			var input = this.observacion;
			this.$http.post('/aracambio/solicitud/observaciones', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.observacion = '';
				this.formErrors = {};
				$('#observacion').modal('hide');
			}, response => {
				this.$set('formErrors', response.body);
			});
		},
		destroyItem: function(id, status){
			
			this.$http.get('/aracambio/solicitud/destroy/'+id+'/'+status).then((response) => {
				this.changePage(this.pagination.current_page);
			});
		},
		filterItem: function(page) {
			var input = this.filter;
			this.$http.post('/aracambio/solicitud/filter?page='+page, input).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
		},
		getCuenta: function (){
			this.$http.get('/aracambio/solicitud/getcuenta/'+this.newItem.beneficiario).then((response) => {
				this.newItem.cuenta = '';
				this.$set('cuentas', response.data);
			}, (response) => {
				console.log("Error");
				this.newItem.cuenta = '';
				this.cuentas = [];
			});
		},
		reporte: function (data) {
			this.reports = data;
			$('#reporte').modal('show');
		},
		format: function (amount, decimals) {
			amount += ''; // por si pasan un numero en vez de un string
		    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

		    decimals = decimals || 0; // por si la variable no fue fue pasada

		    // si no es un numero o es igual a cero retorno el mismo cero
		    if (isNaN(amount) || amount === 0) 
		        return parseFloat(0).toFixed(decimals);

		    // si es mayor o menor que cero retorno el valor formateado como numero
		    amount = '' + amount.toFixed(decimals);

		    var amount_parts = amount.split('.'),
		        regexp = /(\d+)(\d{3})/;

		    while (regexp.test(amount_parts[0]))
		        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

		    return amount_parts.join('.');
		},
		dateFormat: function(date) {
			var day = moment(date).format('DD/MM/YYYY');
			return day;
		}
		
 	}
});
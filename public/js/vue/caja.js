Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

var app = new Vue({
    el: '#app',
    data: {
    	items: [],
    	bancos: [],
    	paises: [],

    	deposito: {
    		'id': '',
    		'value': '',
    		'codigo': '',
    	},
    	reports: {},
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    formErrorsUpdate:{},
	    newItem: {  
	    	"banco": '',
  			"pais": '',
  			"montante": ""
  		},
	    filter: {'data':'', 'data2':'' }
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('/caja/index?page='+page).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
			this.$http.get('/caja/getpais').then((response) => {
				this.$set('paises', response.data);
			});
			this.$http.get('/caja/getsolicitudes').then((response) => {
				this.$set('solicitudes', response.data);
			});
			this.$http.get('/caja/getcaja').then((response) => {
				this.$set('cajas', response.data);
			});
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.getVueItems(page);
		},
		createItem: function(){
			var token = "";
			var possible = "0123456789";
			for (var i = 0; i < 8; i++) {
				token += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			this.newItem.codigo = token;
			this.newItem.montante = this.unFormatIn(this.newItem.montante, '.', '');
			this.newItem.montante = this.unFormatIn(this.newItem.montante, ',', '.');
			var input = this.newItem;
			this.$http.post('/caja/store', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newItem = {'nome':''};
				this.formErrors = {};
			}, response => {
				this.$set('formErrors', response.body);
			});
		},
		editItem: function(id){
			this.$http.get('/caja/'+id+'/edit/').then((response) => {
				this.$set('deposito.id', response.data.id);
				this.$set('deposito.value', response.data.monto);
				this.formatIn3(this.deposito.value);
				$('#editar').modal('show');
			});
		},
		updateItem: function(){
			this.deposito.value = this.unFormatIn(this.deposito.value, '.', '');
			this.deposito.value = this.unFormatIn(this.deposito.value, ',', '.');
			var input = this.deposito;
			this.$http.post('/caja/update/'+input.id, input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.deposito = '';
				$('#editar').modal('hide');
			});
		},

		depositar: function (id) {

			var token = "";
			var possible = "0123456789";
			for (var i = 0; i < 8; i++) {
				token += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			this.deposito.codigo=token;
			this.deposito.id = id;
			console.log(this.deposito.value)
			$('#deposito').modal('show');
		},
		deposite: function(){
			this.deposito.value = this.unFormatIn(this.deposito.value, '.', '');
			this.deposito.value = this.unFormatIn(this.deposito.value, ',', '.');
			var input = this.deposito;
			this.$http.post('/caja/deposito', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.deposito = '';
				this.formErrors = {};
				$('#deposito').modal('hide');
			}, response => {
				this.$set('formErrors', response.body);
			});
		},
		transacciones: function (data){
			var value = 0;
			for(var i = 0; i < data.length; i++){
				value++;
			}
			return value;
		},
		destroyItem: function(id){
			this.$http.get('/caja/destroy/'+id).then((response) => {
				this.changePage(this.pagination.current_page);
			});
		},
	filterItem: function(page) {
			var input = this.filter;
			this.$http.post('/caja/filter', input).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
		},
		getBanco: function (){
			this.$http.get('/caja/getbanco/'+this.newItem.pais).then((response) => {
				this.newItem.banco = '';
				this.$set('bancos', response.data);
			}, (response) => {
				this.newItem.banco = '';
				this.bancos = [];
			});
		},
		format: function (amount, decimals) {
			amount += ''; // por si pasan un numero en vez de un string
		    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

		    decimals = decimals || 0; // por si la variable no fue fue pasada

		    // si no es un numero o es igual a cero retorno el mismo cero
		    if (isNaN(amount) || amount === 0) 
		        return parseFloat(0).toFixed(decimals);

		    // si es mayor o menor que cero retorno el valor formateado como numero
		    amount = '' + amount.toFixed(decimals);

		    var amount_parts = amount.split('.'),
		        regexp = /(\d+)(\d{3})/;

		    while (regexp.test(amount_parts[0]))
		        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

		    return amount_parts.join(',');
		},
		reporte: function (data) {
			this.reports = data;
			$('#reporte').modal('show');
		},
		formatIn3: function (nStr){
			 nStr += '';
			  var comma = /,/g;
			  nStr = nStr.replace(comma,'');
			  x = nStr.split('.');
			  x1 = x[0];
			  x2 = x.length > 1 ? '.' + x[1] : '';
			  var rgx = /(\d+)(\d{3})/;
			  while (rgx.test(x1)) {
			    x1 = x1.replace(rgx, '$1' + ',' + '$2');
			  }
			  this.deposito.value = x1 + x2;

		},
		unFormatIn: function (text, busca, reemplaza){
  			while (text.toString().indexOf(busca) != -1)
      		text = text.toString().replace(busca,reemplaza);
  			return text;
		},
		dateFormat: function(date) {
			var day = moment(date).format('DD/MM/YYYY');
			return day;
		},

 	}
});

const number = document.querySelector('.number');

function formatNumber (n) {
	n = String(n).replace(/\D/g, "");
  return n === '' ? n : Number(n).toLocaleString();
}
number.addEventListener('keyup', (e) => {
	const element = e.target;
	const value = element.value;
  element.value = formatNumber(value);

});
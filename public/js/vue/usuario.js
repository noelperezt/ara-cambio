Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

var app = new Vue({
    el: '#app',
    data: {
    	items: [],
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    pass: false,
	    formErrorsUpdate:{},
	    newItem: {
	    	'nome': '',
	    	'nome2': '',
	    	'username':'',
	    	'senha': '',
	    	'senha1': '',
	    	'rol': '',
	    	'unidad':''
	    },
	    fillItem: {
	    'nome': '',
	    	'nome2': '',
	    	'username':'',
	    	'senha': '',
	    	'senha1': '',
	    	'rol': '',
	    	'unidad':''
	    }
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('/usuario/index?page='+page).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
			this.$http.get('/usuario/getUnidad').then((response) => {
				this.$set('unidades', response.data);
			});
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.getVueItems(page);
		},
		createItem: function(){
			var input = this.newItem;
			if (this.pass == false) {
				this.$http.post('/usuario/store', input).then((response) => {
					this.changePage(this.pagination.current_page);
					this.newItem = {'nome':''};
					this.formErrors = {};
				}, response => {
					this.$set('formErrors', response.body);
				});
			}
			
		},
		editItem: function(id){
			this.$http.get('/usuario/'+id+'/edit/').then((response) => {
				this.$set('fillItem.id', response.data.id);
				this.$set('fillItem.nome', response.data.nombre);
				this.$set('fillItem.nome2', response.data.apellido);
				this.$set('fillItem.unidad', response.data.unidad);
				this.$set('fillItem.username', response.data.username);
				this.$set('fillItem.rol', response.data.rol);
				this.$set('fillItem.status', response.data.status);
				$('#editItem').modal('show');
			});
		},
		updateItem: function(id){
			var input = this.fillItem;
			this.$http.post('/usuario/update/'+id, input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillItem = {'nome':'','status':''};
				$('#editItem').modal('hide');
			});
		},
		destroyItem: function(id){
			alert('Función no establecida por el momento')
			this.$http.get('/usuario/destroy/'+id).then((response) => {
				this.changePage(this.pagination.current_page);
			});
		},
		filterItem: function() {
			var input = this.filter;
			this.$http.post('/articulo/filterdata/', input).then((response) => {
				this.$set('articulos', response.data);
			});
		},
		passConfirm: function() {
			if (this.newItem.senha === this.newItem.senha1) {
				this.pass = false;
			}else{
				this.pass = true;
			}
		}
 	}
});
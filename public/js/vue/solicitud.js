Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");
Vue.directive('selecttwo', {
  twoWay: true,
  bind: function () {
    $(this.el).select2()
    .on("select2:select", function(e) {
      this.set($(this.el).val());
    }.bind(this));
  },
  update: function(nv, ov) {
    $(this.el).trigger("change");
  }
});
Vue.directive('cuenta', {
  update: function(nv, ov) {
  	$(this.getcuenta).trigger("change");
  },
});
var app = new Vue({
    el: '#app',
    data: {
    	items: [],
    	cuentas: [],
    	reports: {},
    	observacion: {
    		'id': '',
    		'value': '',
    	},
    	exito:0,
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    formErrorsUpdate:{},
	    newItem: {
	    	"codigo": "",
		    "data_de_pedido": "",
			"prioridade": "",
			"quantidade": "",
			"tasa_cambio": "",
			"total_cambio": "",
			"moneda": "",
			"cliente": "",
			"beneficiario": "",
			"banco": "",
			"cuenta": "",
			"tipo_pagamento": "",
			"descripcion": "",
			"usuario": ""
		},
	    fillItem: {
		    "codigo": "",
		    "data_de_pedido": "",
			"prioridade": "",
			"quantidade": "",
			"tasa_cambio": "",
			"total_cambio": "",
			"moneda": "",
			"cliente": "",
			"beneficiario": "",
			"banco": "",
			"cuenta": "",
			"tipo_pagamento": "",
			"usuario": "",
			"id_banco":"",
			"total2":""
	    },
	    filter: {
	    	'data_de_pedido':'',
	    	'data_de_pedido2':'',
	    	'prioridad': '',
	    	'cliente':'',
	    	'tipo_pagamento': '',
	    	'banco': '',
	    	'status': ''
	    },
	    formErrorsCliente:{},
	    newItemCliente: {
	    	"codigo": "",
		    "nome": "",
			"ultimo_nome": "",
			"cep": "",
			"data_de_nascimento": "",
			"profissao": "",
			"pais": "",
			"estado": "",
			"cidades": "",
			"bairrio": "",
			"endereco_residencial": "",
			"telefone_de_localizacao": "",
			"celular": "",
			"email": ""
		},
		formErrorsBeneficiario:{},
	    newItemBeneficiario: {
	    	"codigo": "",
		    "nome": "",
			"ultimo_nome": "",
			"cpf": "",
			"data_de_nascimento": "",
			"profissao": "",
			"pais": "",
			"estado": "",
			"cidades": "",
			"bairrio": "",
			"endereco_residencial": "",
			"telefone_de_localizacao": "",
			"celular": "",
			"email": ""
		},
		formErrorsCuenta:{},
		newItemCuenta: {'beneficario':'','banco':'','tipo_da_conta':'', 'numero_da_conta':''},
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('solicitud/index?page='+page).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);

				var f;

				for (var i in this.items) {
					f=(i.fecha).split('-');
					i.fecha=f[2]+'/'+f[1]+'/'+f[0];
				}
			});
			this.$http.get('solicitud/getpago').then((response) => {
				this.$set('pagos', response.data);
			});
			this.$http.get('solicitud/getuser').then((response) => {
				this.$set('users', response.data);
			});
			this.$http.get('solicitud/getpais').then((response) => {
				this.$set('paises', response.data);
			});
			this.$http.get('solicitud/getbanco').then((response) => {
				this.$set('bancos', response.data);
			});
			this.$http.get('solicitud/getbeneficiario').then((response) => {
				this.$set('beneficiarios', response.data);
			});
			this.$http.get('solicitud/getcliente').then((response) => {
				this.$set('clientes', response.data);
			});
			this.$http.get('solicitud/getmoneda').then((response) => {
				this.$set('monedas', response.data);
			});
			this.$http.get('solicitud/getusuarios').then((response) => {
				this.$set('usuarios', response.data);
			});
			this.$http.get('solicitud/getunidades').then((response) => {
				this.$set('unidades', response.data);
			});
		},
		getCodeItem: function(){
			var token = "";
			var possible = "0123456789";
			for (var i = 0; i < 8; i++) {
				token += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			this.newItem.codigo = token;
			$(".cliente").css( "display", "none" );
			$(".cliente2").css( "display", "none" );
			$(".beneficiario").css( "display", "none" );
			$(".beneficiario2").css( "display", "none" );
			$(".nuevacuenta").css( "display", "none" );
			$('#agregar').modal('show');
		},
		
		getCodeItemCliente: function(){
			var token = "";
			var possible = "0123456789";
			for (var i = 0; i < 8; i++) {
				token += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			this.newItemCliente.codigo = token;
			$('#agregar').modal('hide');
			$('#agregar_cliente').modal('show');
		},
		getCodeItemBeneficiario: function(){
			var token = "";
			var possible = "0123456789";
			for (var i = 0; i < 8; i++) {
				token += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			this.newItemBeneficiario.codigo = token;
			$('#agregar').modal('hide');
			$('#agregar_beneficiario').modal('show');
		},
		openCuenta: function(){
			$('#agregar').modal('hide');
			this.newItemCuenta.beneficario=this.newItem.beneficiario;
			$('#agregar_cuenta').modal('show');
		},
		cerrar_modal: function(){
			$('#agregar').modal('show');
			
		},
		createItemCLiente: function(){
		    var id;
			var input = this.newItemCliente;
			this.$http.post('cliente/store', input).then((response) => {
				this.newItemCliente = {
					"codigo": "",
				    "nome": "",
					"ultimo_nome": "",
					"cep": "",
					"data_de_nascimento": "",
					"profissao": "",
					"pais": "",
					"estado": "",
					"cidades": "",
					"bairrio": "",
					"endereco_residencial": "",
					"telefone_de_localizacao": "",
					"celular": "",
					"email": ""
				};
				this.formErrorsCliente = {};
				$("#nombre_cliente2").val(input.nome+" "+input.ultimo_nome);
                $("#telefono_cliente2").val(input.celular);
                $("#cep_cliente2").val(input.cep);
                $("#hiddencep").val(input.cep);
				this.clientes.push(response.data);
				this.newItem.cliente=response.data["id"];
    		    
                $('#cep_cliente2').prop('readonly', true);
                $(".nuevocliente").css( "display", "none" );
                $(".cliente").css( "display", "none" );
                $(".cliente2").css( "display", "block" );
                $('#agregar_cliente').modal('hide');
    			$('#agregar').modal('show');
			},
			response => {
				this.$set('formErrorsCliente', response.body);
			});
		},
		createItemBeneficiario: function(){
			var input = this.newItemBeneficiario;
			this.$http.post('beneficiario/store', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newItemBeneficiario = {
					"codigo": "",
				    "nome": "",
					"ultimo_nome": "",
					"cpf": "",
					"data_de_nascimento": "",
					"profissao": "",
					"pais": "",
					"estado": "",
					"cidades": "",
					"bairrio": "",
					"endereco_residencial": "",
					"telefone_de_localizacao": "",
					"celular": "",
					"email": ""
				};
				this.formErrorsBeneficiario = {};
				$("#hiddencpf").val(input.cpf);
				$("#cep_beneficiario2").val(input.cpf);
				$("#nombre_beneficiario2").val(input.nome+" "+input.ultimo_nome);
                $("#telefono_beneficiario2").val(input.celular);
                $('#cep_beneficiario2').prop('readonly', true);
				this.beneficiarios.push(response.data);
				this.newItem.beneficiario=response.data["id"];
    
                var idBeneficiario = response.data["id"];
            
                $.get("solicitud/getcuenta/"+idBeneficiario+"", function(response, state){
                  if(response){
                    $("#cuenta_bancaria2").empty();
                    $("#cuenta_bancaria2").append("<option value=''>Seleccione</option>");
                    if(response.length>0){
                      for (i=0; i < response.length; i++){
                        $("#cuenta_bancaria2").append("<option value='"+response[i].id+"'>"+response[i].banco.nombre+" - "+response[i].cuenta+" </option>");
                      }
                    }else{
                      $("#cuenta_bancaria2").empty();
                      $("#cuenta_bancaria2").append("<option value=''>Sem contas</option>");
                    }
                  }else{
                    $("#cuenta_bancaria2").empty();
                    $("#cuenta_bancaria2").append("<option value=''>Sem contas</option>");
                  }
                });
    		    
                $(".nuevobeneficiario").css( "display", "none" );
                $(".nuevacuenta").css( "display", "block" );
                $(".beneficiario").css( "display", "none" );
                $(".beneficiario2").css( "display", "block" );
                $('#agregar_beneficiario').modal('hide');
    			$('#agregar').modal('show');
			
			},
			response => {
				this.$set('formErrorsBeneficiario', response.body);
			});
		},
		createItemCuenta: function(){
			var input = this.newItemCuenta;
			var nombre_banco="";
			this.$http.post('cuenta/store', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newItemCuenta = {'beneficario':'','banco':'','tipo_da_conta':'','numero_da_conta':''};
				this.formErrorsCuenta = {};
				this.$http.get('banco/'+response.data["banco"]+'/edit/').then((response2) => {
                    $("#cuenta_bancaria").append("<option selected='selected' value='"+response.data["id"]+"'>"+response2.data.nombre+" - "+response.data["cuenta"]+" </option>");
				    $("#cuenta_bancaria2").append("<option selected='selected' value='"+response.data["id"]+"'>"+response2.data.nombre+" - "+response.data["cuenta"]+" </option>");
			    });
				this.newItem.cuenta=response.data["id"];
                $(".nuevacuenta").css( "display", "none" );
                $('#agregar_cuenta').modal('hide');
    			$('#agregar').modal('show');
			},
			response => {
				this.$set('formErrorsCuenta', response.body);
			});
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.filterItem(page);
		},
		createItem: function(){
			var input = this.newItem;
			this.$http.post('solicitud/store', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newItem = {
					"codigo": "",
				    "data_de_pedido": "",
					"prioridade": "",
					"quantidade": "",
					"tasa_cambio": "",
					"total_cambio": "",
					"moneda": "",
					"cliente": "",
					"beneficiario": "",
					"banco": "",
					"cuenta": "",
					"tipo_pagamento": "",
					"usuario": ""
				};
				this.formErrors = {};
				$('#agregar').modal('hide');
				$(".nuevocliente").css( "display", "none" );
      			$(".cliente").css( "display", "none" );
      			$(".cliente_select").val("");
      			$(".nuevobeneficiario").css( "display", "none" );
       			$(".beneficiario").css( "display", "none" );
       			$(".beneficiario_select").val("");
       			$("#cep_cliente").val("");
       			$('#cep_cliente').prop('readonly', false);
       			$("#cep_beneficiario").val("");
       			$('#cep_beneficiario').prop('readonly', false);
       			$("#total_cambio").val("");
    			$("#tasa_cambio").val("");
    			$("#comprobantetransf").val("");
    			$("#bancotransf").val("");
			
			},
			response => {
				this.$set('formErrors', response.body);
			});
		},
		editItem: function(id){
		    var idBeneficiario=0;
			this.$http.get('solicitud/'+id+'/edit').then((response) => {
				idBeneficiario = response.data.beneficiario.id;
				this.$http.get("solicitud/getcuenta/"+idBeneficiario).then((response2)=>{
                this.$set('cuentas', response2.data);
                this.$set('fillItem.id', response.data.id);
				this.$set('fillItem.fecha', response.data.fecha);
				this.$set('fillItem.nombre_cliente', response.data.cliente.nombre);
				this.$set('fillItem.apellido_cliente', response.data.cliente.apellido);
				this.$set('fillItem.identidad_cliente', response.data.cliente.cep);
				this.$set('fillItem.telefono_cliente', response.data.cliente.telefono);
				this.$set('fillItem.nombre_beneficiario', response.data.beneficiario.nombre);
				this.$set('fillItem.apellido_beneficiario', response.data.beneficiario.apellido);
				this.$set('fillItem.identidad_beneficiario', response.data.beneficiario.cpf);
				this.$set('fillItem.telefono_beneficiario', response.data.beneficiario.telefono);
				this.$set('fillItem.banco', response.data.banco.id);
				this.$set('fillItem.cuenta', response.data.cuenta.id);
				this.$set('fillItem.nrocuenta', response.data.cuenta.cuenta);
				this.$set('fillItem.tipo_cuenta', response.data.cuenta.tipo_cuenta);
				this.$set('fillItem.moneda', response.data.moneda.moneda);
				this.$set('fillItem.monto', response.data.monto);
				this.$set('fillItem.tasa', response.data.taza);
				this.$set('fillItem.referencia', response.data.nro_comprobante);
				this.$set('fillItem.total', response.data.total);
				this.$set('fillItem.nro_comprobante_rpt', response.data.nro_comprobante_rpt);
				this.$set('fillItem.banco_rpt', response.data.banco_rpt);
				this.$set('fillItem.statusf', response.data.status);
				this.$set('fillItem.banco_emisor', response.data.banco_emisor);
				this.$set('fillItem.nro_comprobante_emisor', response.data.nro_comprobante_emisor);
				$("#id_banco").val(response.data.banco.id);
				$("#total2").val(response.data.total);
                });
                $('#editItem').modal('show');
			});
			
		},
		updateItem: function(id){
		    this.fillItem.id_banco=$("#id_banco").val();
		    this.fillItem.total2=$("#total2").val();
			var input = this.fillItem;
			console.log(input);
			this.$http.post('solicitud/update/'+id, input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillItem = {'nome':'','status':''};
				$('#editItem').modal('hide');
			});
		},
		describir: function (id) {
			this.observacion.id = id;
			$('#observacion').modal('show');
		},
		observacoes: function(){
			var input = this.observacion;
			this.$http.post('solicitud/observaciones', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.observacion = '';
				this.formErrors = {};
				$('#observacion').modal('hide');
			}, response => {
				this.$set('formErrors', response.body);
			});
		},
		destroyItem: function(id, status){
			
			this.$http.get('solicitud/destroy/'+id+'/'+status).then((response) => {
				this.changePage(this.pagination.current_page);
			});
		},
		filterItem: function(page) {
			var input = this.filter;
			this.$http.post('solicitud/filter?page='+page, input).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
		},
		getCuenta: function (){
			this.$http.get('solicitud/getcuenta/'+this.newItem.beneficiario).then((response) => {
				this.newItem.cuenta = '';
				this.$set('cuentas', response.data);
			}, (response) => {
				console.log("Error");
				this.newItem.cuenta = '';
				this.cuentas = [];
			});
		},
		reporte: function (data) {
			this.reports = data;
			$('#reporte').modal('show');
		},
		format: function (amount, decimals) {
			amount += ''; // por si pasan un numero en vez de un string
		    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

		    decimals = decimals || 0; // por si la variable no fue fue pasada

		    // si no es un numero o es igual a cero retorno el mismo cero
		    if (isNaN(amount) || amount === 0) 
		        return parseFloat(0).toFixed(decimals);

		    // si es mayor o menor que cero retorno el valor formateado como numero
		    amount = '' + amount.toFixed(decimals);

		    var amount_parts = amount.split('.'),
		        regexp = /(\d+)(\d{3})/;

		    while (regexp.test(amount_parts[0]))
		        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

		    return amount_parts.join(',');
		},
		dateFormat: function(date) {
			var day = moment(date).format('DD/MM/YYYY');
			return day;
		}
		
 	}
});
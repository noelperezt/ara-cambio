Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

var app = new Vue({
    el: '#app',
    data: {
    	cambio: 1,
    	articulos: [],
    	categoria: null,
    	usuario:[],
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    formErrorsUpdate:{},
	    newArticle: {'serial':'', 'nombre':'', 'descripcion':'', 'categoria':'', 'cantidad':''},
	    fillArticulo: {'serial':'', 'nombre':'', 'descripcion':'', 'categoria':'', 'cantidad':''},
	    newCategory: {'nombre': '', 'descripcion': ''},
	    fillCategory: {'nombre': '', 'descripcion': ''},
	    newUser: {'cedula':'','nombre':'','apellio':'','email':''},
	    fillUser: {'cedula':'','nombre':'','apellio':'','email':''},
	    filter: {
	    	'dato': '',
	    	'categoria': ''
	    },
	    filterUser: {
	    	'dato': '',
	    	'categoria': ''
	    },
	    errorArticle: {},
	    errorCategory: {},
	    errorUser: {}
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('/articulo/getdata?page='+page).then((response) => {
				this.$set('articulos', response.data);
				//this.$set('pagination', response.data);
			});
			this.$http.get('/categoria/getdata').then((response) => {
				this.$set('categoria', response.data);
			});
			this.$http.get('/usuario/getdata').then((response) => {
				this.$set('usuario', response.data);
			});
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.getVueItems(page);
		},
		createCategory: function (){
			var input = this.newCategory;
			this.$http.post('/categoria/add/', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newCategory = {'nombre': '', 'descripcion': ''};
				this.errorCategory = {};
				$('#addCategories').modal('hide');
				$.toast("Categoria cargada correctamente");
			}, response => {
				this.$set('errorCategory', response.body);
			});
		},
		editCategory: function(id){
			this.$http.get('/categoria/'+id+'/edit/').then((response) => {
				this.$set('fillCategory', response.data);
				$('#editCategory').modal('show');
			});
		},
		updateCategory: function(id){
			var input = this.fillCategory;
			this.$http.post('/categoria/'+id+'/update/', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillCategory = {'nombre': '', 'descripcion': ''};
				this.errorCategory = {};
				$('#editCategory').modal('hide');
				$.toast("Categoria actualizada con éxito");
			}, response => {
				this.$set('errorCategory', response.body);
			});
		},
		destroyCategory: function(id){
			this.$http.get('/categoria/destroy/'+id).then((response) => {
				$.toast("Categoría eliminada con exito");
				this.changePage(this.pagination.current_page);
			});
		},
		createArticle: function(){
			var input = this.newArticle;
			this.$http.post('/articulo/store/', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newArticle = {'serial':'', 'nombre':'', 'descripcion':'', 'categoria':'', 'cantidad':''};
				this.errorArticle = {};
				$('#addArticle').modal('hide');
				$.toast("Articulo cargado correctamente");
			}, response => {
				this.$set('errorArticle', response.body);
			});
		},
		editArticle: function(id){
			this.$http.get('/articulo/'+id+'/edit/').then((response) => {
				this.$set('fillArticle', response.data);
				$('#editArticle').modal('show');
			});
		},
		updateArticle: function(id){
			var input = this.fillArticle;
			this.$http.post('/articulo/'+id+'/update/', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillArticle = {'serial':'', 'nombre':'', 'descripcion':'', 'categoria':'', 'cantidad':''};
				this.errorArticle = {};
				$('#editArticle').modal('hide');
				$.toast("Articulo actualizado con éxito");
			}, response => {
				this.$set('errorArticle', response.body);
			});
		},
		destroyArticle: function(id){
			this.$http.get('/articulo/destroy/'+id).then((response) => {
				$.toast("Articulo eliminado con exito");
				this.changePage(this.pagination.current_page);
			});
		},
		filterArticle: function() {
			var input = this.filter;
			this.$http.post('/articulo/filterdata/', input).then((response) => {
				this.$set('articulos', response.data);
			});
		},
		//Métodos para todas las acciones referentes a los usuarios
		createUser: function(){
			var input = this.newUser;
			this.$http.post('/usuario/store/', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newUser = {'cedula':'','nombre':'','apellio':'','email':''};
				this.errorUser = {};
				$('#addUser').modal('hide');
				$.toast("usuario cargado correctamente");
			}, response => {
				this.$set('errorUser', response.body);
			});
		},
		editUser: function(id){
			this.$http.get('/usuario/'+id+'/edit/').then((response) => {
				this.$set('fillUser', response.data);
				$('#editUser').modal('show');
			});
		},
		updateUser: function(id){
			var input = this.fillUser;
			this.$http.post('/usuario/'+id+'/update/', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillUser = {'cedula':'','nombre':'','apellio':'','email':''};
				this.errorUser = {};
				$('#editUser').modal('hide');
				$.toast("usuario actualizado con éxito");
			}, response => {
				this.$set('errorUser', response.body);
			});
		},
		destroyUser: function(id){
			this.$http.get('/usuario/destroy/'+id).then((response) => {
				$.toast("usuario eliminado con exito");
				this.changePage(this.pagination.current_page);
			});
		},
		filterUsers: function() {
			var input = this.filterUser;
			this.$http.post('/usuario/filterdata/', input).then((response) => {
				this.$set('usuario', response.data);
			});
		},
		printArticle: function(data) {
			this.$http.get('/articulo/pdf').then((response) => {
			});
		},
		blur: function (){
			if ($('#body').hasClass('modal-open')) {
            	return true;
	        }else{
	            return false;
	        }
		}
 	}
 	
});
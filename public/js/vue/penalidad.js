Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

var app = new Vue({
    el: '#app',
    data: {
    	items: [],
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    formErrorsUpdate:{},
	    newItem: {'descricao':'','montante':''},
	    fillItem: {'descricao':'','montante':'', 'status':''}
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('/penalidad/index?page='+page).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.getVueItems(page);
		},
		createItem: function(){
			var input = this.newItem;
			this.$http.post('/penalidad/store', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newItem = {'descricao':'','montante':''};
				this.formErrors = {};
				}, response => {
				this.$set('formErrors', response.body);
			});
		},
		editItem: function(id){
			this.$http.get('/penalidad/edit/'+id).then((response) => {
				this.$set('fillItem', response.data);
				$('#editItem').modal('show');
			});
		},
		updateItem: function(id){
			var input = this.fillItem;
			this.$http.post('/penalidad/update/'+id, input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillItem = {'descricao':'','montante':'','status':''};
				$('#editItem').modal('hide');
			});
		},
		destroyItem: function(id){
			alert('Función no establecida por el momento')
			/*this.$http.get('/item/destroy/'+id).then((response) => {
				this.changePage(this.pagination.current_page);
			});*/
		},
		filterItem: function() {
			var input = this.filter;
			this.$http.post('/articulo/filterdata/', input).then((response) => {
				this.$set('articulos', response.data);
			});
		},
 	}
});
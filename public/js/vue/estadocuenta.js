Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");
Vue.directive('selecttwo', {
  twoWay: true,
  bind: function () {
    $(this.el).select2()
    .on("select2:select", function(e) {
      this.set($(this.el).val());
    }.bind(this));
  },
  update: function(nv, ov) {
    $(this.el).trigger("change");
  }
});
Vue.directive('cuenta', {
  update: function(nv, ov) {
  	console.log("Entro");
  	$(this.getcuenta).trigger("change");
  },
});
var app = new Vue({
    el: '#app',
    data: {
    	items: [],
    	cuentas: [],
    	reports: {},
    	observacion: {
    		'id': '',
    		'value': '',
    	},
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    formErrorsUpdate:{},
	    newItem: {
	    	"codigo": "",
		    "data_de_pedido": "",
			"prioridade": "",
			"quantidade": "",
			"tasa_cambio": "",
			"total_cambio": "",
			"moneda": "",
			"cliente": "",
			"beneficiario": "",
			"banco": "",
			"cuenta": "",
			"tipo_pagamento": "",
			"descripcion": "",
			"usuario": ""
		},
	    fillItem: {
		    "codigo": "",
		    "data_de_pedido": "",
			"prioridade": "",
			"quantidade": "",
			"tasa_cambio": "",
			"total_cambio": "",
			"moneda": "",
			"cliente": "",
			"beneficiario": "",
			"banco": "",
			"cuenta": "",
			"tipo_pagamento": "",
			"usuario": ""
	    },
	    filter: {
	    	'data_de_pedido':'',
	    	'data_de_pedido2':'',
	    	'prioridad': '',
	    	'cliente':'',
	    	'tipo_pagamento': '',
	    	'banco': '',
	    	'status': ''
	    }
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('estado_cuenta/index?page='+page).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);

				var f;

				for (var i in this.items) {
					f=(i.fecha).split('-');
					i.fecha=f[2]+'/'+f[1]+'/'+f[0];
				}
			});
			this.$http.get('estado_cuenta/totalizar?page='+page).then((response) => {
				this.$set('totales', response.data.data);
				this.$set('pagination', response.data);

				var f;

				for (var i in this.items) {
					f=(i.fecha).split('-');
					i.fecha=f[2]+'/'+f[1]+'/'+f[0];
				}
			});
			this.$http.get('estado_cuenta/sumar?page='+page).then((response) => {
				this.$set('subtotales', response.data.data);
				this.$set('pagination', response.data);

				var f;

				for (var i in this.items) {
					f=(i.fecha).split('-');
					i.fecha=f[2]+'/'+f[1]+'/'+f[0];
				}
			});
			this.$http.get('estado_cuenta/getpago').then((response) => {
				this.$set('pagos', response.data);
			});
			this.$http.get('estado_cuenta/getusuarios').then((response) => {
				this.$set('usuarios', response.data);
			});
			this.$http.get('estado_cuenta/getunidades').then((response) => {
				this.$set('unidades', response.data);
			});
		},
		getCodeItem: function(){
			var token = "";
			var possible = "0123456789";
			for (var i = 0; i < 8; i++) {
				token += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			this.newItem.codigo = token;
			$('#agregar').modal('show');
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.filterItem(page);
		},
		filterItem: function(page) {
			var input = this.filter;
			this.$http.post('estado_cuenta/filter?page='+page, input).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
			this.$http.post('estado_cuenta/filterTotalizar?page='+page, input).then((response) => {
				this.$set('totales', response.data.data);
				this.$set('pagination', response.data);
			});
			this.$http.post('estado_cuenta/filterSumar?page='+page, input).then((response) => {
				this.$set('subtotales', response.data.data);
				this.$set('pagination', response.data);
			});
		},
		format: function (amount, decimals) {
			amount += ''; // por si pasan un numero en vez de un string
		    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

		    decimals = decimals || 0; // por si la variable no fue fue pasada

		    // si no es un numero o es igual a cero retorno el mismo cero
		    if (isNaN(amount) || amount === 0) 
		        return parseFloat(0).toFixed(decimals);

		    // si es mayor o menor que cero retorno el valor formateado como numero
		    amount = '' + amount.toFixed(decimals);

		    var amount_parts = amount.split('.'),
		        regexp = /(\d+)(\d{3})/;

		    while (regexp.test(amount_parts[0]))
		        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

		    return amount_parts.join(',');
		},
		dateFormat: function(date) {
			var day = moment(date).format('DD/MM/YYYY');
			return day;
		}
		
 	}
});
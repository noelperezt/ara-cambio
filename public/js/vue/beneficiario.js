Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

var app = new Vue({
    el: '#app',
    data: {
    	items: [],
    	paginate: 15,
	    pagination: {
			total: 0,
			per_page: 2,
			from: 1,
			to: 0,
			current_page: 1
	    },
	    offset: 4,
	    formErrors:{},
	    formErrorsUpdate:{},
	    newItem: {
	    	"codigo": "",
		    "nome": "",
			"ultimo_nome": "",
			"cpf": "",
			"data_de_nascimento": "",
			"profissao": "",
			"pais": "",
			"estado": "",
			"cidades": "",
			"bairrio": "",
			"endereco_residencial": "",
			"telefone_de_localizacao": "",
			"celular": "",
			"email": ""
		},
	    fillItem: {
		    "nome": "",
			"status": "",
			"pais": "",
			"id": "",
			"ultimo_nome": "",
			"data_de_nascimento": "",
			"profissao": "",
			"celular": "",
			"email": "",
			"telefone_de_localizacao": "",
			"cpf": "",
			"endereco_residencial": "",
			"cidades": "",
			"bairrio": "",
			"estado": ""
	    },
	    filter: {
	    	'cpf':'',
	    	'nome_completo': '',
	    	'profissao':'',
	    	'pais': ''
	    }
    },
    computed: {
	    isActived: function() {
	    	return this.pagination.current_page;
	    },
	    pagesNumber: function() {
			if (!this.pagination.to) {
				return [];
			}
			var from = this.pagination.current_page - this.offset;
			if (from < 1) {
				from = 1;
			}
			var to = from + (this.offset * 2);
			if (to >= this.pagination.last_page) {
				to = this.pagination.last_page;
			}
			var pagesArray = [];
			while (from <= to) {
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
	    }
  	},
	ready: function() {
		this.getVueItems(this.pagination.current_page);
	},
	methods: {
		getVueItems: function(page) {
			this.$http.get('beneficiario/index?page='+page).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
			this.$http.get('banco/getpais').then((response) => {
				this.$set('paises', response.data);
			});
		},
		getCodeItem: function(){
			var token = "";
			var possible = "0123456789";
			for (var i = 0; i < 8; i++) {
				token += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			this.newItem.codigo = token;
			$('#agregar').modal('show');
		},
		changePage: function(page){
			this.pagination.current_page = page;
			this.getVueItems(page);
		},
		createItem: function(){
			var input = this.newItem;
			this.$http.post('beneficiario/store', input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.newItem = {
					"codigo": "",
				    "nome": "",
					"ultimo_nome": "",
					"cep": "",
					"data_de_nascimento": "",
					"profissao": "",
					"pais": "",
					"estado": "",
					"cidades": "",
					"bairrio": "",
					"endereco_residencial": "",
					"telefone_de_localizacao": "",
					"celular": "",
					"email": ""
				};
				this.formErrors = {};
				$('#agregar').modal('hide');
			
			},
			response => {
				this.$set('formErrors', response.body);
			});
		},
		editItem: function(id){
			this.$http.get('beneficiario/'+id+'/edit/').then((response) => {
				this.$set('fillItem.id', response.data.id);
				this.$set('fillItem.nome', response.data.nombre);
				this.$set('fillItem.ultimo_nome', response.data.apellido);
				this.$set('fillItem.data_de_nascimento', response.data.fecha_nac);
				this.$set('fillItem.profissao', response.data.profesion);
				this.$set('fillItem.pais', response.data.pais);
				this.$set('fillItem.celular', response.data.celular);
				this.$set('fillItem.email', response.data.email);
				this.$set('fillItem.telefone_de_localizacao', response.data.telefono);
				this.$set('fillItem.cpf', response.data.cpf);
				this.$set('fillItem.endereco_residencial', response.data.lugar);
				this.$set('fillItem.cidades', response.data.ciudad);
				this.$set('fillItem.bairrio', response.data.barrio);
				this.$set('fillItem.estado', response.data.estado);
				this.$set('fillItem.status', response.data.status);
				$('#editItem').modal('show');
			});
		},
		updateItem: function(id){
			var input = this.fillItem;
			this.$http.post('beneficiario/update/'+id, input).then((response) => {
				this.changePage(this.pagination.current_page);
				this.fillItem = {'nome':'','status':''};
				$('#editItem').modal('hide');
			});
		},
		destroyItem: function(id){
			this.$http.get('beneficiario/destroy/'+id).then((response) => {
				this.changePage(this.pagination.current_page);
			});
		},
		filterItem: function() {
			var input = this.filter;
			this.$http.post('beneficiario/filter', input).then((response) => {
				this.$set('items', response.data.data);
				this.$set('pagination', response.data);
			});
		},
 	}
});
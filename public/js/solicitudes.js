$(document).ready(function(){
  $("#moneda" ).change(function() {
    var tasa = $("#moneda").find(':selected').data('tasa');
    var monto = $("#monto").val();
    const noTruncarDecimales = {maximumFractionDigits: 20};
    var total = tasa * monto;
    $("#total_cambio").val(total.toLocaleString('de-DE', noTruncarDecimales));
    $("#tasa_cambio").val(tasa.toLocaleString('de-DE', noTruncarDecimales));
  });
  $("#moneda" ).change(function() {
    var tasa = $("#moneda").find(':selected').data('tasa');
    var monto = $("#monto").val();
    const noTruncarDecimales = {maximumFractionDigits: 20};
    var total = tasa * monto;
    $("#total_cambio").val(total.toLocaleString('de-DE', noTruncarDecimales));
    $("#tasa_cambio").val(tasa.toLocaleString('de-DE', noTruncarDecimales));
  });
  $("#monto").keyup(function() {
    var tasa = $("#moneda").find(':selected').data('tasa');
    var monto = $("#monto").val();
    const noTruncarDecimales = {maximumFractionDigits: 20};
    if(tasa==null){
      tasa=0;
    }
    var total = tasa * monto;
    $("#total_cambio").val(total.toLocaleString('de-DE', noTruncarDecimales));
    $("#tasa_cambio").val(tasa.toLocaleString('de-DE', noTruncarDecimales));
  });
  $("#tasa_cambio").keyup(function() {
    var tasa = $("#tasa_cambio").val();
    var monto = $("#monto").val();
    tasa = parseFloat(tasa.replace('.', ""));

    const noTruncarDecimales = {maximumFractionDigits: 20};
    if(tasa==null){
      tasa=0;
    }
    var total = tasa * monto;
    $("#total_cambio").val(total);
    $("#tasa_cambio").val(tasa);
  });
  $("#monto_e").keyup(function() {
    var tasa = $("#tasa_e").val();
    var monto = $("#monto_e").val();
    const noTruncarDecimales = {maximumFractionDigits: 20};
    if(tasa==null){
      tasa=0;
    }
    var total = tasa * monto;
    $("#total_e").val(total.toLocaleString('de-DE', noTruncarDecimales));
    $("#total2").val(total);
  });
  $("#tasa_e").keyup(function() {
    var tasa = $("#tasa_e").val();
    var monto = $("#monto_e").val();
    tasa = parseFloat(tasa.replace('.', ""));

    const noTruncarDecimales = {maximumFractionDigits: 20};
    if(tasa==null){
      tasa=0;
    }
    var total = tasa * monto;
    $("#total_e").val(total.toLocaleString('de-DE', noTruncarDecimales));
    $("#total2").val(total);
  });
  $("#select_cliente" ).change(function() {
    var nombre = $("#select_cliente").find(':selected').data('nombre');
    var cep = $("#select_cliente").find(':selected').data('cep');
    var celular = $("#select_cliente").find(':selected').data('celular');
    var cliente = $("#select_cliente").find(':selected').val()
    
    $("#nombre_cliente").val(nombre);
    $("#cedula_cliente").val(cep);
    $("#telefono_cliente").val(celular);
    if(cliente==""){
        if($("#hiddencep").val()!=""){
            $(".cliente2").css( "display", "block" );
            $("#hiddencep").val("");
        }
        $(".nuevocliente").css( "display", "block" );
        $(".cliente").css( "display", "none" );
        $("#cep_cliente").val(cep);
        $('#cep_cliente').prop('readonly', false);
    }else{
       $(".cliente").css( "display", "block" );
       $(".cliente2").css( "display", "none" );
       $(".nuevocliente").css( "display", "block" );
       $("#cep_cliente").val(cep);
       $('#cep_cliente').prop('readonly', true);
       if($("#hiddencep").val()!=""){
            $(".cliente").css( "display", "none" );
            $(".cliente2").css( "display", "block" );
            $("#hiddencep").val("");
        }
    }
  });

  $("#select_beneficiario" ).change(function() {
    var nombre = $("#select_beneficiario").find(':selected').data('nombre');
    var cpf = $("#select_beneficiario").find(':selected').data('cpf');
    var celular = $("#select_beneficiario").find(':selected').data('celular');
    var beneficiario = $("#select_beneficiario").find(':selected').val();
    
    $("#nombre_beneficiario").val(nombre);
    $("#cedula_beneficiario").val(cpf);
    $("#telefono_beneficiario").val(celular);
    if(beneficiario==""){
       $(".nuevobeneficiario").css( "display", "block" );
       $(".beneficiario").css( "display", "none" );
       $("#cep_beneficiario").val(cpf);
       $('#cep_beneficiario').prop('readonly', false);
       if($("#hiddencpf").val()!=""){
            $(".beneficiario2").css( "display", "block" );
            $("#hiddencep").val("");
        }
    }else{
       $(".beneficiario").css( "display", "block" );
       $(".nuevobeneficiario").css( "display", "block" );
       $(".nuevacuenta").css( "display", "block" );
       $(".beneficiario2").css( "display", "none" );
       $("#cep_beneficiario").val(cpf);
       $('#cep_beneficiario').prop('readonly', true);
       if($("#hiddencpf").val()!=""){
            $(".beneficiario").css( "display", "none" );
            $(".beneficiario2").css( "display", "block" );
            $("#hiddencep").val("");
        }
    }

    var idBeneficiario = this.value;

    $.get("solicitud/getcuenta/"+idBeneficiario+"", function(response, state){
      if(response){
        $("#cuenta_bancaria").empty();
        $("#cuenta_bancaria").append("<option value=''>Seleccione</option>");
        if(response.length>0){
          for (i=0; i < response.length; i++){
            $("#cuenta_bancaria").append("<option value='"+response[i].id+"'>"+response[i].banco.nombre+" - "+response[i].cuenta+" </option>");
          }
        }else{
          $("#cuenta_bancaria").empty();
          $("#cuenta_bancaria").append("<option value=''>Sem contas</option>");
        }
      }else{
        $("#cuenta_bancaria").empty();
        $("#cuenta_bancaria").append("<option value=''>Sem contas</option>");
      }
    });

  });
    
  $("#cuenta_b" ).change(function() {
    var banco = $("#cuenta_b").find(':selected').data('banco');
    var cuenta = $("#cuenta_b").find(':selected').data('cuenta');
    var tipo = $("#cuenta_b").find(':selected').data('tipocuenta');
    if(banco){
        $("#banco_b").val(banco);
        $("#id_banco").val(banco);
        document.getElementById("cuenta_ba").value=cuenta;
        document.getElementById("tipo_b").value=tipo;
    }
  });

});


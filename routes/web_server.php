<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', [ 'as' => 'login', 'uses' => 'loginController@do']);
Route::post('login/login' ,[
	'uses' => 'loginController@login',
	'as'   => 'login.login'
	]);
Route::get('login/logout' ,[
	'uses' => 'loginController@logout',
	'as'   => 'login.logout'
	]);

Route::get('/registro', function () {
    return view('registro');
});

Route::get('/', 'homeController@index')->middleware('auth');
Route::get('/factura/{id}', 'facturaController@index')->middleware('auth');

Route::get('/paises',function (){
	 return view('paises');
	})->middleware('auth');
Route::get('pais/index', [
	'uses'	=> 'paisController@index',
	'as'	=> 'pais.index'
]);
Route::post('pais/store', [
	'uses'	=> 'paisController@store',
	'as'	=> 'pais.store'
]);
Route::get('pais/{id}/edit', [
	'uses'	=> 'paisController@edit',
	'as'	=> 'pais.edit'
]);
Route::post('pais/update/{id}', [
	'uses'	=> 'paisController@update',
	'as'	=> 'pais.update'
]);
Route::get('pais/destroy/{id}', [
	'uses'	=> 'paisController@destroy',
	'as'	=> 'pais.destroy'
]);

Route::get('/unidades',function (){
	 return view('unidades');
	})->middleware('auth');
Route::get('unidad/index', [
	'uses'	=> 'unidadController@index',
	'as'	=> 'unidad.index'
]);
Route::post('unidad/store', [
	'uses'	=> 'unidadController@store',
	'as'	=> 'unidad.store'
]);
Route::get('unidad/{id}/edit', [
	'uses'	=> 'unidadController@edit',
	'as'	=> 'unidad.edit'
]);
Route::post('unidad/update/{id}', [
	'uses'	=> 'unidadController@update',
	'as'	=> 'unidad.update'
]);
Route::get('unidad/destroy/{id}', [
	'uses'	=> 'unidadController@destroy',
	'as'	=> 'unidad.destroy'
]);

Route::get('/banco',function (){
	 return view('banco');
	})->middleware('auth');
Route::get('banco/index', [
	'uses'	=> 'bancoController@index',
	'as'	=> 'banco.index'
]);
Route::get('banco/getpais', [
	'uses'	=> 'bancoController@getpais',
	'as'	=> 'banco.getpais'
]);
Route::post('banco/store', [
	'uses'	=> 'bancoController@store',
	'as'	=> 'banco.store'
]);
Route::post('banco/deposito', [
	'uses'	=> 'bancoController@deposito',
	'as'	=> 'banco.deposito'
]);
Route::get('banco/{id}/edit', [
	'uses'	=> 'bancoController@edit',
	'as'	=> 'banco.edit'
]);
Route::post('banco/update/{id}', [
	'uses'	=> 'bancoController@update',
	'as'	=> 'banco.update'
]);
Route::get('banco/destroy/{id}', [
	'uses'	=> 'bancoController@destroy',
	'as'	=> 'banco.destroy'
]);

Route::get('/tipo_pago',function (){
	 return view('tipo_pago');
	})->middleware('auth');
Route::get('tipo_pago/index', [
	'uses'	=> 'tipoPagoController@index',
	'as'	=> 'tipo_pago.index'
]);
Route::post('tipo_pago/store', [
	'uses'	=> 'tipoPagoController@store',
	'as'	=> 'tipo_pago.store'
]);
Route::get('tipo_pago/{id}/edit', [
	'uses'	=> 'tipoPagoController@edit',
	'as'	=> 'tipo_pago.edit'
]);
Route::post('tipo_pago/update/{id}', [
	'uses'	=> 'tipoPagoController@update',
	'as'	=> 'tipo_pago.update'
]);
Route::get('tipo_pago/destroy/{id}', [
	'uses'	=> 'tipoPagoController@destroy',
	'as'	=> 'tipo_pago.destroy'
]);

Route::get('/moneda',function (){
	 return view('moneda');
	})->middleware('auth');
Route::get('moneda/index', [
	'uses'	=> 'monedaController@index',
	'as'	=> 'moneda.index'
]);
Route::get('moneda/getpais', [
	'uses'	=> 'monedaController@getpais',
	'as'	=> 'moneda.getpais'
]);
Route::post('moneda/store', [
	'uses'	=> 'monedaController@store',
	'as'	=> 'moneda.store'
]);
Route::get('moneda/{id}/edit', [
	'uses'	=> 'monedaController@edit',
	'as'	=> 'moneda.edit'
]);
Route::post('moneda/update/{id}', [
	'uses'	=> 'monedaController@update',
	'as'	=> 'moneda.update'
]);
Route::get('moneda/destroy/{id}', [
	'uses'	=> 'monedaController@destroy',
	'as'	=> 'moneda.destroy'
]);

Route::get('/penalidades',function (){
	 return view('penalidades');
	})->middleware('auth');
Route::get('penalidad/index', [
	'uses'	=> 'penalidadController@index',
	'as'	=> 'penalidad.index'
]);
Route::post('penalidad/store', [
	'uses'	=> 'penalidadController@store',
	'as'	=> 'penalidad.store'
]);
Route::get('penalidad/{id}/edit', [
	'uses'	=> 'penalidadController@edit',
	'as'	=> 'penalidad.edit'
]);
Route::post('penalidad/update/{id}', [
	'uses'	=> 'penalidadController@update',
	'as'	=> 'penalidad.update'
]);
Route::get('penalidad/destroy/{id}', [
	'uses'	=> 'penalidadController@destroy',
	'as'	=> 'penalidad.destroy'
]);

Route::get('/cuentas',function (){
	 return view('cuentas');
	})->middleware('auth');
Route::get('cuenta/index', [
	'uses'	=> 'cuentaController@index',
	'as'	=> 'cuenta.index'
]);
Route::get('cuenta/getbanco', [
	'uses'	=> 'cuentaController@getBanco',
	'as'	=> 'cuenta.getbanco'
]);
Route::get('cuenta/getbeneficiario', [
	'uses'	=> 'cuentaController@getbeneficiario',
	'as'	=> 'cuenta.getbeneficiario'
]);
Route::post('cuenta/store', [
	'uses'	=> 'cuentaController@store',
	'as'	=> 'cuenta.store'
]);
Route::get('cuenta/{id}/edit', [
	'uses'	=> 'cuentaController@edit',
	'as'	=> 'cuenta.edit'
]);
Route::post('cuenta/update/{id}', [
	'uses'	=> 'cuentaController@update',
	'as'	=> 'cuenta.update'
]);
Route::get('cuenta/destroy/{id}', [
	'uses'	=> 'cuentaController@destroy',
	'as'	=> 'cuenta.destroy'
]);

Route::get('/beneficiario',function (){
	 return view('beneficiario');
	})->middleware('auth');
Route::get('beneficiario/index', [
	'uses'	=> 'beneficiarioController@index',
	'as'	=> 'beneficiario.index'
]);
Route::get('beneficiario/getpais', [
	'uses'	=> 'beneficiarioController@getPais',
	'as'	=> 'beneficiario.getpais'
]);
Route::post('beneficiario/filter', [
	'uses'	=> 'beneficiarioController@filterData',
	'as'	=> 'beneficiario.filter'
]);
Route::post('beneficiario/store', [
	'uses'	=> 'beneficiarioController@store',
	'as'	=> 'beneficiario.store'
]);
Route::get('beneficiario/{id}/edit', [
	'uses'	=> 'beneficiarioController@edit',
	'as'	=> 'beneficiario.edit'
]);
Route::post('beneficiario/update/{id}', [
	'uses'	=> 'beneficiarioController@update',
	'as'	=> 'beneficiario.update'
]);
Route::get('beneficiario/destroy/{id}', [
	'uses'	=> 'beneficiarioController@destroy',
	'as'	=> 'beneficiario.destroy'
]);

Route::get('/usuario',function (){
	 return view('usuario');
	})->middleware('auth');

Route::get('usuario/index' ,[
	'uses' => 'usuarioController@index',
	'as'   => 'usuario.index'
	]);
Route::post('usuario/store',[
	'uses' => 'usuarioController@store',
	'as'   =>  'usuario.store'
	]);
Route::get('usuario/{id}/edit',[
	'uses' => 'usuarioController@edit',
	'as'   =>  'usuario.edit'
	]);
Route::post('usuario/update/{id}',[
	'uses' => 'usuarioController@update',
	'as'   =>  'usuario.update'
	]);

Route::get('usuario/{id}/destroy',[
	'uses' => 'usuarioController@destroy',
	'as'   =>  'usuario.destroy'
	]);
Route::get('usuario/getUnidad', [
	'uses'	=> 'usuarioController@getUnidad',
	'as'	=> 'usuario.getunidad'
]);

Route::get('/cliente',function (){
	 return view('cliente');
	})->middleware('auth');
Route::get('cliente/index', [
	'uses'	=> 'clienteController@index',
	'as'	=> 'cliente.index'
]);
Route::get('cliente/getpais', [
	'uses'	=> 'clienteController@getPais',
	'as'	=> 'cliente.getpais'
]);
Route::post('cliente/filter', [
	'uses'	=> 'clienteController@filterData',
	'as'	=> 'cliente.filter'
]);
Route::post('cliente/store', [
	'uses'	=> 'clienteController@store',
	'as'	=> 'cliente.store'
]);
Route::get('cliente/{id}/edit', [
	'uses'	=> 'clienteController@edit',
	'as'	=> 'cliente.edit'
]);
Route::post('cliente/update/{id}', [
	'uses'	=> 'clienteController@update',
	'as'	=> 'cliente.update'
]);
Route::get('cliente/destroy/{id}', [
	'uses'	=> 'clienteController@destroy',
	'as'	=> 'cliente.destroy'
]);

Route::get('/caxa',function (){
	 return view('caxa');
	})->middleware('auth');
Route::get('caja/index', [
	'uses'	=> 'cajaController@index',
	'as'	=> 'caja.index'
]);
Route::get('caja/getpais', [
	'uses'	=> 'cajaController@getPais',
	'as'	=> 'caja.getpais'
]);
Route::get('caja/getbanco/{id}', [
	'uses'	=> 'cajaController@getBanco',
	'as'	=> 'caja.getbanco'
]);
Route::post('caja/store', [
	'uses'	=> 'cajaController@store',
	'as'	=> 'caja.store'
]);
Route::get('caja/{id}/edit', [
	'uses'	=> 'cajaController@edit',
	'as'	=> 'caja.edit'
]);
Route::post('caja/update/{id}', [
	'uses'	=> 'cajaController@update',
	'as'	=> 'caja.update'
]);
Route::get('caja/destroy/{id}', [
	'uses'	=> 'cajaController@destroy',
	'as'	=> 'caja.destroy'
]);
Route::get('/solicitud',function (){
	 return view('solicitud');
	})->middleware('auth');

Route::get('solicitud/index' ,[
	'uses' => 'solicitudController@index',
	'as'   => 'solicitud.index'
	]);
Route::post('solicitud/store',[
	'uses' => 'solicitudController@store',
	'as'   =>  'solicitud.store'
	]);
Route::get('solicitud/{id}/edit',[
	'uses' => 'solicitudController@edit',
	'as'   =>  'solicitud.edit'
	]);
Route::post('solicitud/update/{id}',[
	'uses' => 'solicitudController@update',
	'as'   =>  'solicitud.update'
	]);

Route::get('solicitud/destroy/{id}/{status}',[
	'uses' => 'solicitudController@destroy',
	'as'   =>  'solicitud.destroy'
	]);
Route::get('solicitud/getpago', [
	'uses'	=> 'solicitudController@getpago',
	'as'	=> 'solicitud.getpago'
]);

Route::get('solicitud/getuser', [
	'uses'	=> 'solicitudController@getuser',
	'as'	=> 'solicitud.getuser'
]);
Route::get('solicitud/getpais', [
	'uses'	=> 'solicitudController@getpais',
	'as'	=> 'solicitud.getpais'
]);
Route::get('solicitud/getbanco', [
	'uses'	=> 'solicitudController@getbanco',
	'as'	=> 'solicitud.getbanco'
]);
Route::get('solicitud/getbeneficiario', [
	'uses'	=> 'solicitudController@getbeneficiario',
	'as'	=> 'solicitud.getbeneficiario'
]);
Route::get('solicitud/getcliente', [
	'uses'	=> 'solicitudController@getcliente',
	'as'	=> 'solicitud.getcliente'
]);
Route::get('solicitud/getmoneda', [
	'uses'	=> 'solicitudController@getmoneda',
	'as'	=> 'solicitud.getmoneda'
]);
Route::get('solicitud/getcuenta/{id}', [
	'uses'	=> 'solicitudController@getcuenta',
	'as'	=> 'solicitud.getcuenta'
]);
Route::post('solicitud/filter', [
	'uses'	=> 'solicitudController@filterData',
	'as'	=> 'solicitudController.filter'
]);
Route::post('solicitud/observaciones', [
	'uses'	=> 'solicitudController@observaciones',
	'as'	=> 'solicitud.observaciones'
]);

Route::get('/observacion',function (){
	 return view('observacion');
	})->middleware('auth');
Route::get('observacion/index' ,[
	'uses' => 'observacionController@index',
	'as'   => 'observacion.index'
	]);
Route::post('observacion/filter', [
	'uses'	=> 'observacionController@filterData',
	'as'	=> 'observacionController.filter'
]);

Route::get('/historico_tasa',function (){
	 return view('historico_tasa');
	})->middleware('auth');
Route::get('historico_tasa/index' ,[
	'uses' => 'historicoTasaController@index',
	'as'   => 'historico_tasa.index'
	]);
Route::post('historico_tasa/filter', [
	'uses'	=> 'historicoTasaController@filterData',
	'as'	=> 'historicoTasaController.filter'
]);
Route::get('solicitud/{id}/edit', [
	'uses'	=> 'solicitudController@edit',
	'as'	=> 'solicitud.edit'
]);
Route::post('solicitud/update/{id}', [
	'uses'	=> 'solicitudController@update',
	'as'	=> 'solicitud.update'
]);
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudPenalidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_penalidad', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->bigInteger('penalidad')->unsigned();
            $table->foreign('penalidad')->references('id')->on('penalidad');
            $table->bigInteger('solicitud')->unsigned();
            $table->foreign('solicitud')->references('id')->on('solicitudes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_penalidads');
    }
}

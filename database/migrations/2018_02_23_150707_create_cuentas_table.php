<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas', function (Blueprint $table) {
            $table->bigIncrements('id',20)->unsigned();
            $table->bigInteger('beneficiario')->unsigned();
            $table->foreign('beneficiario')->references('id')->on('beneficiarios');
            $table->bigInteger('banco')->unsigned();
            $table->foreign('banco')->references('id')->on('banco');
            $table->string('tipo_cuenta',50);
            $table->string('cuenta',70);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas');
    }
}

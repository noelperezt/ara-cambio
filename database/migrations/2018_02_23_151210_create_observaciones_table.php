<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observaciones', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('descripcion', 70);
            $table->date('fecha');
            $table->string('hora',15);
            $table->bigInteger('usuario')->unsigned();
            $table->foreign('usuario')->references('id')->on('users');
            $table->bigInteger('solicitud')->unsigned();
            $table->tinyInteger('status')->default(1);
            $table->foreign('solicitud')->references('id')->on('solicitudes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observacions');
    }
}

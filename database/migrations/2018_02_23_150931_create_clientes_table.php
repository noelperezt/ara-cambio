<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id',20)->unsigned();
            $table->string('codigo',20)->unique();
            $table->string('tipo', 20)->nullable();
            $table->string('documento', 20)->nullable();
            $table->string('nombre', 70);
            $table->string('apellido', 70);
            $table->date('fecha_nac');
            $table->string('profesion',20);
            $table->string('pais', 50);
            $table->string('celular', 20);
            $table->string('telefono', 20);
            $table->string('email',70);
            $table->string('cep', 70);
            $table->string('lugar', 70);
            $table->string('numero', 20);
            $table->string('barrio', 100);
            $table->string('ciudad', 70);
            $table->string('estado', 70);
            $table->string('observacion', 200)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}

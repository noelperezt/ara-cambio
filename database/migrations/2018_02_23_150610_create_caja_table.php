<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caja', function (Blueprint $table) {
            $table->bigIncrements('id',20)->unsigned();
            $table->double('monto');
            $table->date('fecha');
            $table->bigInteger('banco')->unsigned();
            $table->foreign('banco')->references('id')->on('banco');
            $table->string('codigo')->unique();
            $table->timestamps();
            $table->Integer('tipo_ope');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cajas');
    }
}

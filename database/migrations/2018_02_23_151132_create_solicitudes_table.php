<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('codigo', 70)->unique();
            $table->double('monto');
            $table->double('taza');
            $table->double('total');
            $table->date('fecha');
            $table->string('estado', 70)->default('0');
            $table->string('prioriad', 20);
            $table->string('nro_comprobante', 50)->nullable();
            $table->string('url_img_comprobante', 200)->nullable();
            $table->string('nro_comprobante_rpt', 50)->nullable();
            $table->string('url_img_comprobante_rpt', 200)->nullable();
            $table->bigInteger('banco')->unsigned();
            $table->foreign('banco')->references('id')->on('banco');
            $table->bigInteger('banco_rpt')->unsigned()->nullable();
            $table->foreign('banco_rpt')->references('id')->on('banco');
            $table->bigInteger('cliente')->unsigned();
            $table->foreign('cliente')->references('id')->on('clientes');
            $table->bigInteger('beneficiario')->unsigned();
            $table->foreign('beneficiario')->references('id')->on('beneficiarios');
            $table->bigInteger('cuenta')->unsigned();
            $table->foreign('cuenta')->references('id')->on('cuentas');
            $table->bigInteger('tipo_pago')->unsigned();
            $table->foreign('tipo_pago')->references('id')->on('tipo_pago');
            $table->bigInteger('moneda')->unsigned();
            $table->foreign('moneda')->references('id')->on('monedas');
            $table->bigInteger('usuario')->unsigned();
            $table->foreign('usuario')->references('id')->on('users');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicituds');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historicos', function (Blueprint $table) {
            $table->bigIncrements('id',20)->unsigned();
            $table->date('fecha');
            $table->string('hora',15);
            $table->double('monto');
            $table->bigInteger('usuario')->unsigned();
            $table->foreign('usuario')->references('id')->on('users');
            $table->bigInteger('moneda')->unsigned();
            $table->foreign('moneda')->references('id')->on('monedas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historicos');
    }
}

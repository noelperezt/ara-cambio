<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id',20)->unsigned();
            $table->string('nombre',70);
            $table->string('apellido',70);
            $table->string('username',70)->unique();
            $table->string('password',100);
            $table->string('rol',20);
            $table->bigInteger('unidad')->unsigned();
            $table->foreign('unidad')->references('id')->on('unidades');
            $table->tinyInteger('status')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('unidades')->insert([
            'id'            => 1,
            'descripcion'   => 'Caracas',
            'status'        => 1
        ]);
        DB::table('unidades')->insert([
            'id'            => 2,
            'descripcion'   => 'Manaus',
            'status'        => 1
        ]);
        DB::table('users')->insert([
            'id'                => 1,
            'nombre'            => 'ARA',
            'apellido'			=> 'cambio',
            'username'             => 'aracambios',
            'password'          => bcrypt('123456'),
            'rol'             => 1,
            'unidad'    => 1,
            'status'	=> 1
        ]);
    }
}
